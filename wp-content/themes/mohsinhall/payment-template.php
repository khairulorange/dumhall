<?php
/**
 * Template Name: Payment Template
 */

?>

 <?php

            
class UserPayment{


   public function __construct()

   {
       $userId = $_GET['userid'];
       $userMail = $_GET['userid'];
       $this->requestSessionApi($userId, $userMail );
       add_action( 'listenForSslcommerzResponse', [$this,'processSslcommerzResponseForValidationApi'] );
   }

   //send request to ssl payment getway
   public function requestSessionApi($userId){
       global $wpdb;
       $option = get_option('payment_api_opt');


       $usersInfo = get_userdata($userId);


       if($usersInfo->eventJoin ==='1'){
           $total_amount = (int)get_option( 'payment_api_opt' )['registarionFee'] + (int)$usersInfo->event_price;
           $membershipType =  'Life Membership+Reunion';
       }else{
           $total_amount = (int) get_option( 'payment_api_opt' )['registarionFee'] ;
           $membershipType = 'Life Membership only';
       }


       //NEW V4 HOSTED API OF SSLCOMMERZ

       $post_data = array(
               'store_id'      => trim($option['store_id']),
           'store_passwd'  => trim($option['store_password']),
           'total_amount'  => $total_amount,
           'tran_id'       => "SSLCZ_TEST_".uniqid(),
           'success_url'   => get_page_link($option['success_page']),
           'fail_url'      => get_page_link($option['fail_page']),
           'cancel_url'    => get_page_link($option['fail_page']),
           'ipn_url'       => get_page_link($option['ipn_page']),
           'cus_name'      => $usersInfo->display_name,
           'cus_add1'      => $usersInfo->presentAddress,
           'cus_country'   => "Bangladesh",
           'cus_state'     => "Dhaka",
           'cus_city'      => "Dhaka",
           'cus_postcode'  => "1200",
           'cus_phone'     => $usersInfo->mobile,
           'cus_email'     => $usersInfo->email,
           'ship_name'     => "Dhaka",
           'ship_add1'     => $usersInfo->presentAddress,
           'ship_country'  => 'Bangladesh',
           'ship_state'    => "Dhaka",
           'delivery_tel'  => $usersInfo->mobile,
           'ship_city'     => "Dhaka",
           'ship_postcode' =>  "1000",
           'currency'      => 'BDT',
           'product_category'  => 'Membership',
           'shipping_method'   => 'NO',
           'num_of_item'       => '1',
           'cart'=> json_encode(array(
            array("product"=>"Registration Fee","amount"=>$usersInfo->reg_price),
            array("product"=>"Event Fee","amount"=> $usersInfo->event_price),
        )),
           'product_name'      => 'Registration Fee',
           'product_profile'   => 'general'
       );




       $table_name = $wpdb->prefix . 'payment_history';
       $wpdb->insert(
           $table_name,
           array(
               'time' => current_time( 'mysql' ),
               'user_id' => $usersInfo->ID,
               'user_name' => $post_data['cus_name'],
               'user_email' => $post_data['cus_email'],
               'price' => $post_data['total_amount'],
               'price' => $post_data['total_amount'],
               'gateway' => 'sslcommerz',
               'tran_id' => $post_data['tran_id'],
               'status' => "Pending",
               'membership_type' => $membershipType,
           )
       );
           // var_dump($post_data);
           # REQUEST SEND TO SSLCOMMERZ
       $direct_api_url = "https://sandbox.sslcommerz.com/gwprocess/v3/api.php";

       $handle = curl_init();
       curl_setopt($handle, CURLOPT_URL, $direct_api_url );
       curl_setopt($handle, CURLOPT_TIMEOUT, 30);
       curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 30);
       curl_setopt($handle, CURLOPT_POST, 1 );
       curl_setopt($handle, CURLOPT_POSTFIELDS, $post_data);
       curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
       curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, FALSE); # KEEP IT FALSE IF YOU RUN FROM LOCAL PC


       $content = curl_exec($handle );

       $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

       if($code == 200 && !( curl_errno($handle))) {
           curl_close( $handle);
           $sslcommerzResponse = $content;
       } else {
           curl_close( $handle);
           echo "FAILED TO CONNECT WITH SSLCOMMERZ API";
           exit;
       }

       # PARSE THE JSON RESPONSE
       $sslcz = json_decode($sslcommerzResponse, true );


       if(isset($sslcz['GatewayPageURL']) && $sslcz['GatewayPageURL']!="" ) {
           # THERE ARE MANY WAYS TO REDIRECT - Javascript, Meta Tag or Php Header Redirect or Other
           # echo "<script>window.location.href = '". $sslcz['GatewayPageURL'] ."';</script>";
           echo "<meta http-equiv='refresh' content='0;url=".$sslcz['GatewayPageURL']."'>";
           $post_data['tran_id'];
           # header("Location: ". $sslcz['GatewayPageURL']);
           exit;
       } else {
           echo "JSON Data parsing error!";
       }
   }
   public function listenForSslcommerzResponse() {
       if ( (isset($_POST['val_id']) && $_POST['val_id'] !="") && ((isset($_POST['tran_id']) && $_POST['tran_id'] !="")) && $_SERVER['QUERY_STRING'] != 'sslcommerzeddipn')
       {
           do_action('listenForSslcommerzResponse');
       }
   }
   
}

new UserPayment();

 ?>
    