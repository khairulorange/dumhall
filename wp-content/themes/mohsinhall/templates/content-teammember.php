<div class="item">
    <div class="member">
        <?php if(has_post_thumbnail()): ?>
        <div class="member-img">
            <?php the_post_thumbnail('thumbnail',array('class'=>'img-fluid', 'alt'=> 'team-member-img')); ?>
        </div>
        <?php endif; ?>
        <div class="member-content">
            <h4 class="name"><?php the_title(); ?></h4>
            <p class="designation"><?php the_content(); ?></p>
        </div>
    </div>
</div>