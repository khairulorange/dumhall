<?php
    $memberInfo = array(
        'role'    => 'subscriber',
        'order'   => 'ASC',
        'number' => -1,
    );
    $usersInfo = get_users( $memberInfo );
    if(!empty($usersInfo)):
    ?>
    <div class="row" id="search-filter-data">
    <?php foreach ($usersInfo as $memberInfo):
    $fullName = get_user_meta( $memberInfo->ID, 'name', true);
    $department = get_user_meta( $memberInfo->ID, 'department', true);
    $userPhoto = get_user_meta( $memberInfo->ID, 'userPhoto', true);
    ?>
        <div class="col-12 col-md-4 col-lg-3 mb-3">
            <div class="member-item text-center rounded">
                <?php if(!empty($userPhoto)): ?>
                <img src="<?php echo $userPhoto; ?>" class="img-fluid rounded-circle" alt="user Photo" width="96" height="96" style="height: 96px;object-fit: cover;">
                <?php else: ?>
                <?php echo get_avatar( $memberInfo->ID, 'medium', '', 'member-profile', array('class' => array('img-fluid', 'rounded-circle') )); ?>
                <?php endif; ?>
                <?php if(!empty($fullName)): ?>
                    <h4><?php echo $fullName; ?></h4>
                <?php endif; ?>
                <?php if(!empty($department)): ?>
                    <h6>Department: <?php echo $department; ?></h6>
                <?php endif; ?>
            </div>
        </div>
    <?php endforeach; else:?>
        <h4>Member Not Register Yet!</h4>
    <?php endif; ?>