<div class="blog-item featured pax-15">
    <div class="row">
        <div class="col-12 px-0">
            <?php if(has_post_thumbnail()):
                the_post_thumbnail('full',array('class'=>'img-fluid img-cover', 'alt'=> 'blog-featured-img'));
            endif; ?>
            <span class="featured-text">Featured</span>
        </div>
        <div class="col-12 blog-content paa-20">
            <div class="blog-title-wrapper">
                <a href="<?php the_permalink()?>"> <h4 class="blog-title"><?php the_title(); ?></h4></a>
            </div>
            <p class="meta">
                <span><i class="far fa-user"></i> by <?php the_author(); ?></span>
                <span><i class="far fa-clock"></i> at  <?php echo get_the_time('F j, Y' ); ?></span>
            </p>
            <p>
            <?php
                if(get_the_excerpt()):
                    echo wp_trim_words( get_the_excerpt(), 16, '...' );
                else:
                    echo wp_trim_words( get_the_content(), 16, '...' );
                endif;
            ?>
            </p>
            <a href="<?php the_permalink(); ?>">Read more <i class="fas fa-angle-right"></i></a>
        </div>
    </div>
</div>