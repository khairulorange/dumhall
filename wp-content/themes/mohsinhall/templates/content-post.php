<div class="blog-item pax-15 mab-30">
    <div class="row">
        <?php if(has_post_thumbnail()): ?>
        <div class="col-12 col-md-5 p-0">
            <?php the_post_thumbnail('737x449',array('class'=>'img-fluid img-cover', 'alt'=> 'blog-img')); ?>
        </div>
        <div class="col-12 col-md-7 blog-content paa-20">
                <div class="blog-title-wrapper trimmed" data-line-trim="1">
                    <a href="<?php the_permalink()?>"><h4 class="blog-title"><?php the_title(); ?></h4></a>
                </div>
                <p class="meta">
                    <span><i class="far fa-user"></i> by <?php the_author(); ?></span>
                    <span><i class="far fa-clock"></i> at <?php echo get_the_time('F j, Y' ); ?></span>
                </p>
                <p>
                    <?php
                    if(get_the_excerpt()):
                        echo wp_trim_words( get_the_excerpt(), 16, '...' );
                    else:
                        echo wp_trim_words( get_the_content(), 16, '...' );
                    endif;
                    ?>
                </p>
                <a href="<?php the_permalink(); ?>">Read more <i class="fas fa-angle-right"></i></a>
            </div>
        <?php else: ?>
            <div class="col-12 blog-content paa-20">
                <div class="blog-title-wrapper trimmed" data-line-trim="1">
                    <h4 class="blog-title"><?php the_title(); ?></h4>
                </div>
                <p class="meta">
                    <span><i class="far fa-user"></i> by <?php the_author(); ?></span>
                    <span><i class="far fa-clock"></i> at <?php echo get_the_time('F j, Y' ); ?></span>
                </p>
                <p>
                    <?php
                    if(get_the_excerpt()):
                        echo wp_trim_words( get_the_excerpt(), 16, '...' );
                    else:
                        echo wp_trim_words( get_the_content(), 16, '...' );
                    endif;
                    ?>
                </p>
                <a href="<?php the_permalink(); ?>">Read more <i class="fas fa-angle-right"></i></a>
            </div>
        <?php endif; ?>
    </div>
</div>