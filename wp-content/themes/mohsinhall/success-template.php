<?php
/**
 * Template Name: success template
 */

?>

<?php get_header(); ?>

<div class="container">
    <div class="row">
        <div class="col">
       <?php


    $options = get_option( 'payment_api_opt' );

	$val_id=urlencode($_POST['val_id']);
    $store_id= urlencode($options['store_id']);
    $store_passwd= urlencode($options['store_password']);
    $requested_url = ("https://sandbox.sslcommerz.com/validator/api/validationserverAPI.php?val_id=".$val_id."&store_id=".$store_id."&store_passwd=".$store_passwd."&v=1&format=json");

    $handle = curl_init();
    curl_setopt($handle, CURLOPT_URL, $requested_url);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false); # IF YOU RUN FROM LOCAL PC
    curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false); # IF YOU RUN FROM LOCAL PC

    $result = curl_exec($handle);
    
    $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

    if($code == 200 && !( curl_errno($handle)))
    {

        # TO CONVERT AS ARRAY
        # $result = json_decode($result, true);
        # $status = $result['status'];

        # TO CONVERT AS OBJECT
        $result = json_decode($result);


        # TRANSACTION INFO
        $status = $result->status;
        $tran_date = $result->tran_date;
       // $tran_id = 'SSLCZ_TEST_5dc11368e9af3';
        $tran_id = $result->tran_id;
        $val_id = $result->val_id;
        $amount = $result->amount;
        $store_amount = $result->store_amount;
        $bank_tran_id = $result->bank_tran_id;
        $card_type = $result->card_type;


         global $wpdb;
        $table_name = $wpdb->prefix . 'payment_history';

        $tranCheck = $wpdb->get_results("SELECT * FROM $table_name WHERE tran_id = '$tran_id' LIMIT 1");



        if($tranCheck[0] && $tranCheck[0]->status =='processing' ){

            $wpdb->update(
                $table_name ,
                array(
                    'status' => 'Payment Success',	// string
                ),
                array( 'tran_id' => $tran_id ) // where
            );

            $userId='';



            global $wpdb;



            if(!is_user_logged_in()){
                $temp_table = $wpdb->prefix . 'temporary_users';

                $targetEmail = $tranCheck[0]->user_email;


                $temUserData = $wpdb->get_results("SELECT * FROM $temp_table WHERE email = '$targetEmail' ORDER BY id DESC LIMIT 1 ");

                $randompassword = wp_generate_password(5);
                $userId = wp_create_user($temUserData[0]->email, $randompassword, $temUserData[0]->email);
                if ($userId != NULL) {
                    foreach ($temUserData[0] as $userMetaKey => $userMetaValue) {
                        update_user_meta($userId, $userMetaKey, $userMetaValue);
                    }
                }

            }elseif (is_user_logged_in() ){
                $userId = get_current_user_id();
            }


            // Insert into event id
            if (!empty($temUserData[0]->wanttojoin)){

                $wantJoin = explode(",",$temUserData[0]->wanttojoin);


                $args = array(
                    'post_type' => 'events',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                );
                $targetEvents = new WP_Query($args);

                while ( $targetEvents->have_posts() ) : $targetEvents->the_post();

                    $joinedUsers = get_post_meta(get_the_ID(),'joinedUsers',true);
                    if (in_array(get_the_ID(), $wantJoin)){
                        if (!in_array(get_the_ID(), $joinedUsers)){

                            if(is_array($joinedUsers)){
                                array_push($joinedUsers,$userId);
                                update_post_meta( get_the_ID(),  'joinedUsers', $joinedUsers);
                            }else{
                                $joinedUsers = array($userId);
                                update_post_meta( get_the_ID(),  'joinedUsers', $joinedUsers);

                            }
                        }

                    }

                endwhile;
                wp_reset_postdata();
            }else{

                $joinedUsers = get_post_meta($tranCheck[0]->event_id,'joinedUsers',true);

                if(is_array($joinedUsers)){
                    array_push($joinedUsers,$userId);
                    update_post_meta( $tranCheck[0]->event_id,  'joinedUsers', $joinedUsers);
                }else{
                    $joinedUsers = array($userId);
                    update_post_meta( $tranCheck[0]->event_id,  'joinedUsers', $joinedUsers);
                }

            }

            the_content();

        }

    }
    else {

        echo "Failed to connect with SSLCOMMERZ";
    }

	?>


        </div>
    </div>
</div>

<?php get_footer(); ?>
