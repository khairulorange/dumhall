(function ($) {
	'use-strict';
	$(document).ready(function(){
		var OBJ = {
			department: 0,
			session: 0
		}
		$('#SelectDepartment').on('change', function() {
			OBJ.department = $(this).find(":selected").val() ;
			callFromDB();
		});

		$('#SelectSession').on('change', function() {
			OBJ.session = $(this).find(":selected").val() ;
			callFromDB();
		});
		function callFromDB() {
			if(OBJ.department != 0 && OBJ.session != 0){
				$.post(MemberSearchObj.ajax_url,{
					'action':'memberSearchFilter',
					'department': OBJ.department,
					'session': OBJ.session,
					security: MemberSearchObj.nonce,
					beforeSend: function (xhr) {
						$('.du-loader').fadeIn(200);
					},
				},function(data){
					//console.log(data);
					//$('#dumhall-user-list').html(data);
					$('#search-filter-data').html(data);
					$('.du-loader').fadeOut(200);

				})

			}
		}
	});


})(jQuery);