jQuery(function ($) {
    'use strict';
    $(document).ready( function(){
        $("#ibenic_file_upload").on("click", function(){
            $('#ibenic_file_input').click(function(event) {
                event.stopPropagation();
            });
        });
        $('#ibenic_file_input').on('change', prepareCertificateUpload);
        function prepareCertificateUpload(event) {
            var file = event.target.files;
            var parent = $("#" + event.target.id).parent();
            var data = new FormData();
            data.append("action", "ibenic_file_upload");
            $.each(file, function(key, value)
            {
                data.append("ibenic_file_upload", value);
            });

            $.ajax({
                url: uploadCertificateImage.ajax_url,
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function(data, textStatus, jqXHR) {

                    if( data.response == "SUCCESS" ){
                        var preview = "";
                        if( data.type === "image/jpg"
                            || data.type === "image/png"
                            || data.type === "image/gif"
                            || data.type === "image/jpeg"
                        ) {
                            preview = "<img src='" + data.url + "' />";

                        } else {
                            preview = data.filename;
                        }

                        var previewID = parent.attr("id") + "_preview";
                        var previewParent = $("#"+previewID);
                        previewParent.show();
                        previewParent.children(".ibenic_file_preview").empty().append( preview );
                        previewParent.children( "button" ).attr("data-fileurl",data.url );
                        $('#certificate').val(data.url);
                        parent.children("input").val("");
                        parent.hide();

                    } else {
                        alert( data.error );
                    }

                }

            });

        }
    });

    $(".ibenic_file_delete").on("click", function(e){
        e.preventDefault();

        var fileurl = $(this).attr("data-fileurl");
        var data = { fileurl: fileurl, action: 'ibenic_file_delete' };
        $.ajax({
            url: uploadCertificateImage.ajax_url,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {

                if( data.response == "SUCCESS" ){
                    $("#ibenic_file_upload_preview").hide();
                    $("#ibenic_file_upload").show();
                    add_message( "File successfully deleted", "success");
                }

                if( data.response == "ERROR" ){
                    add_message( data.error, "danger");
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                add_message( textStatus, "danger" );
            }
        });

    });

    function add_message($msg, $type){
        var html = "<div class='alert alert-"+$type+"'>" + $msg + "</div>";
        $(".ibenic_upload_message").empty().append(html);
        $(".ibenic_upload_message").fadeIn();
        setTimeout(function() { $(".ibenic_upload_message").fadeOut("slow"); }, 2000);
    }
});

