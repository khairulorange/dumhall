jQuery(function ($) {
    'use strict';
    $(document).ready( function(){
        var photoUploadInput = '#user-photo-upload-input';
        $(photoUploadInput).click(function(event) {
            event.stopPropagation();
        });

        $(photoUploadInput).on('change', prepareUpload);
        function prepareUpload(event) {
            event.preventDefault();
            var file = event.target.files;
            var parent = $("#" + event.target.id).parent();
            var data = new FormData();
            data.append("action", "UploadUserPhoto");

            $.each(file, function(key, value)
            {
                data.append("UploadPhoto", value);
            });

            $.ajax({
                url: uploadImages.ajax_url,
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(data, textStatus, jqXHR) {
                    if( data.response == "SUCCESS" ){
                        var preview = "";
                        if( data.type === "image/jpg"
                            || data.type === "image/png"
                            || data.type === "image/gif"
                            || data.type === "image/jpeg"
                        ) {
                            preview = "<img src='" + data.url + "' />";
                        } else {
                            preview = data.filename;
                        }

                        var previewParent = $("#user_photo_upload_preview");
                        previewParent.show();
                        previewParent.children(".user-photo-preview").empty().append( preview );
                        previewParent.children( "button" ).attr("data-fileurl",data.url );
                        parent.children("input").val("");
                        parent.hide();
                        $('#userPhoto').val(data.url);

                    } else {
                         alert( data.error );
                    }

                }

            });

        }
    });


    $(".uploaded_photo_delete").on("click", function(e){
        e.preventDefault();

        var fileurl = $(this).attr("data-fileurl");
        var data = { fileurl: fileurl, action: 'user_photo_delete' };
        $.ajax({
            url: uploadImages.ajax_url,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {

                if( data.response == "SUCCESS" ){
                    $("#user_photo_upload_preview").hide();
                    $("#photo-upload-wrapper").show();
                    add_message( "File successfully deleted", "success");
                }

                if( data.response == "ERROR" ){
                    add_message( data.error, "danger");
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                add_message( textStatus, "danger" );
            }
        });

    });
    function add_message($msg, $type){
        var deleteWrapper = '.photo_deleted_message';
        var html = "<div class='alert alert-"+$type+"'>" + $msg + "</div>";
        $(deleteWrapper).empty().append(html);
        $(deleteWrapper).fadeIn();
        setTimeout(function() { $(deleteWrapper).fadeOut("slow"); }, 2000);
    }


});


