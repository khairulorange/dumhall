jQuery(function ($) {
    'use strict';

    $('.update-member-account-settings').on('submit', function (e) {
        e.preventDefault();
        var formData = $(this).serializeArray();


        $.ajax({
            method: 'POST',
            url: ProfileUpdateObj.ajax_url,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', ProfileUpdateObj.nonce);
            },
            data: {
                action : 'MemberRegister',
                security: ProfileUpdateObj.nonce,
                formData : formData
            },
            success: function (r) {
                var response = $.parseJSON(r);
                if(response.registerStatus === false){
                    $('.register-error').html('<p>'+response.message+'</p>');
                }else{
                    $('.register-success').fadeIn(100).html('<p>' + response.message + '</p>');
                   // window.location.href = response.redirectUrl;

                }


            },
            error: function (r) {
                console.log($.parseJSON(r));

            },
            complete: function () {
                setTimeout(function () {
                    $('.register-success').fadeOut(100);
                }, 400);
            }
        });


    });
});
