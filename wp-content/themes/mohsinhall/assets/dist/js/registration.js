jQuery(function ($) {
    'use strict';

    $('.member-register').on('submit', function (e) {
        e.preventDefault();
        var formData = $(this).serializeArray();


        $.ajax({
            method: 'POST',
            url: RegistrationObj.ajax_url,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', RegistrationObj.nonce);
            },
            data: {
                action : 'MemberRegister',
                security: RegistrationObj.nonce,
                formData : formData
            },
            success: function (r) {
                var response = $.parseJSON(r);
                if(response.registerStatus === false){
                    $('.register-error').html('<p>'+response.message+'</p>');
                }else{
                    $('.register-success').fadeIn(100).html('<p>' + response.message + '</p>');
                   // window.location.href = response.redirectUrl;

                }


            },
            error: function (r) {
                console.log($.parseJSON(r));

            },
            complete: function () {
                setTimeout(function () {
                    $('.register-success').fadeOut(100);
                }, 400);
            }
        });


    });
});
