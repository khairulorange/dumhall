<?php
get_header();
if ( ! is_front_page() ) :
    get_template_part( 'templates/banner', 'page' );
endif;
?>
<main>
    <section>
        <div class="page-content">
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile;  endif; ?>
        </div>
    </section>
</main>

<?php get_footer(); ?>
