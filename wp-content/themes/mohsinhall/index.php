<?php get_header(); ?>
<main>
	<section class="bg-light pay-70">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-9">
					<div class="blog-list blog-archive">
                        <?php
                        $args = new WP_Query(
                            [
                                'post_type' => 'post',
                                'posts_per_page'=> -1,
                                'order'         => 'DSC',
                            ]
                        );

                        if ($args->have_posts()): while ($args->have_posts()) :
                            $args->the_post();
                        get_template_part('templates/content', 'post-full-width');
                        endwhile;  wp_reset_query(); endif;
                        ?>
					</div>
				</div>
			</div>

		</div>
	</section>
</main>
<?php get_footer(); ?>