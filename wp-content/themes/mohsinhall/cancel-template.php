<?php
/**
 * Template Name: Cancel payment template
 */
?>

<?php
get_header();


?>


<div class="container">
    <div class="row">
        <div class="col">

       <?php

       $payment_info = array(
           'payment_method' => 'sslcommerz',
           'amount' => $_POST['amount'],
           'transaction_id' => $_POST['tran_id'],
           'status' => $_POST['status'],
       );

       $tran_id = $payment_info['transaction_id'];

        global $wpdb;;
        $table_name = $wpdb->prefix . 'payment_history';
        $temporary_table = $wpdb->prefix . 'temporary_users';


        $tranCheck = $wpdb->get_results("SELECT * FROM $table_name WHERE tran_id = '$tran_id' LIMIT 1");


        if($tranCheck[0] && $tranCheck[0]->status =='Pending' ){

            $wpdb->update(
                $table_name ,
                array(
                    'status' => $payment_info['status'],	// string
                ),
                array( 'tran_id' => $tran_id ) // where
            );

            the_content();

        }




	?>


        </div>
    </div>
</div>


<?php get_footer();?>