<?php
/*
  * Orangetoolz key features archive page
  * @package orangetoolz
  *  archive posts
  *
  */

get_header(); ?>
<main>
    <?php  get_template_part( 'templates/content', 'banner' ); ?>
    <section class="pay-70">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="page-content">
                        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                            <?php the_content(); ?>
                        <?php endwhile;  wp_reset_query(); endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php get_footer(); ?>
