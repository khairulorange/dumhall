<?php
Class CustomPostType{

    public function __construct()
    {
        add_action('init', [ $this, 'eventsInit' ]);
        add_action('init', [ $this, 'departmentsInit' ]);
        add_action('init', [ $this, 'departmentsTaxonomy' ]);
        add_action('init', [ $this, 'galleriesInit' ]);
        add_action('init', [ $this, 'galleriesTaxonomy' ]);

        add_action( 'manage_events_posts_custom_column', [$this, 'eventsColumns'], 10, 9 );
        add_filter('manage_edit-events_columns', [ $this, 'myEditEventsColumns' ]);
        add_action( 'manage_team_members_posts_custom_column', [$this, 'teamMembersColumns'], 10, 9 );

        add_action( 'manage_galleries_posts_custom_column', [$this, 'galleriesColumns'], 10, 9 );
        add_filter('manage_edit-galleries_columns', [ $this, 'myEditGalleriesColumns' ]);

    }

    /*
    * custom post type Event
    */
    public function eventsInit()
    {

        register_post_type('events', [
            'labels'            => [
                'name'                      => 'Events',
                'singular_name'     => 'Event Item',
                'all_items'             => 'All Events',
                'add_new'                   => 'Add New Event',
                'add_new_item'      => 'Add New Event'
            ],
            'public'            => true,
            'has_archive'   => true,
            'menu_icon'           => 'dashicons-format-gallery',
            'hierarchical'       => true,
            'rewrite'           => [ 'slug' => 'events' ],
            'supports'      => [ 'title','editor','thumbnail', 'excerpt'],
            'show_ui' => true,
        ]);

    }


    function myEditEventsColumns( $columns )
    {
        $columns = array(
            'cb' => '<input type="checkbox" />',
            'title' => __('Event Name', 'dumhall'),
            'image' => __('Event Images', 'dumhall'),
            'author' => __('Author', 'dumhall'),
            'date' => __('Date', 'dumhall')
        );
        return $columns;
    }

    function eventsColumns($column, $post_id)
    {

        switch ($column)
        {
            case 'image':
                $img = get_post_meta($post_id, '_thumbnail_id', true );
                if(!empty($img)):
                    echo '<img src="'. wp_get_attachment_image_url($img, 'thumbnail' ).'" width="80" height="auto">';
                endif;
                break;

        }
    }




    /*
    * custom post type Department
    */
    public function departmentsInit()
    {

        register_post_type('departments', [
                'labels'             => [
                'name'               => 'Departments',
                'singular_name'      => 'Department Item',
                'all_items'          => 'All Departments',
                'add_new'            => 'Add New Department',
                'add_new_item'       => 'Add New Department'
            ],
                'public'             => true,
                'publicly_queryable' => true,
                'has_archive'        => true,
                'menu_icon'          => 'dashicons-id-alt',
                'hierarchical'       => false,
                'rewrite'            => [ 'slug' => 'departments' ],
                'supports'           => [ 'title','editor','thumbnail'],
                'show_ui'            => true,
        ]);
    }

    public  function departmentsTaxonomy()
    {
        register_taxonomy(
            'department_cat',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
            'departments',        //post type name
            array(
                'hierarchical' => true,
                'label' => 'Categories',  //Display name
                'query_var' => true,
                'show_ui'      => true,
                'rewrite'      => array( 'slug' => 'department_cat' ),

            )
        );
    }

    /*
   * custom post type Gallery
   */
    public function galleriesInit()
    {

        register_post_type('galleries', [
            'labels'            => [
                'name'                      => 'Galleries',
                'singular_name'     => 'Gallery Item',
                'all_items'             => 'All Galleries',
                'add_new'                   => 'Add New Gallery',
                'add_new_item'      => 'Add New Gallery'
            ],
            'public'            => true,
            'has_archive'   => true,
            'menu_icon'           => 'dashicons-images-alt2',
            'hierarchical'       => false,
            'rewrite'           => [ 'slug' => 'galleries' ],
            'supports'      => [ 'title','editor','thumbnail'],
            'show_ui' => true,
        ]);
    }

    public function galleriesTaxonomy()
    {
        register_taxonomy(
            'gallery_cat',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
            'galleries',        //post type name
            array(
                'hierarchical' => true,
                'label' => 'Categories',  //Display name
                'query_var' => true,
                'rewrite' => array(
                    'slug' => 'gallery_cat', // This controls the base slug that will display before each term
                    'with_front' => false // Don't display the category base before
                )
            )
        );
    }

	public function myEditGalleriesColumns( $columns )
    {
        $columns = array(
            'cb' => '<input type="checkbox" />',
            'title' => __('Gallery Name', 'ornage'),
            'image' => __('Gallery  Images', 'ornage'),
            'author' => __('Author', 'ornage'),
            'date' => __('Date', 'ornage')
        );
        return $columns;
    }

	public function galleriesColumns($column, $post_id)
    {
        global $post;

        switch ($column)
        {
            case 'image':
                $img = get_post_meta($post_id, '_thumbnail_id', true );
                if(!empty($img)):
                    echo '<img src="'. wp_get_attachment_image_url($img, 'thumbnail' ).'" width="80" height="auto">';
                endif;
                break;

        }
    }

}

new CustomPostType;