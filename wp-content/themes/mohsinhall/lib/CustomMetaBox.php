<?php
class CustomMetaBox{

    public function __construct()
    {
        add_action('add_meta_boxes', [ $this, 'post_metabox' ], 10, 2);
        add_action('save_post', [ $this, 'post_metabox_save' ], 10, 3);
		add_action( 'wp_ajax_eventJoinMetaBox', [$this, 'userJoinMetaBox']);
		add_action( 'wp_ajax_nopriv_eventJoinMetaBox', array($this, 'userJoinMetaBox' ));
    }

    /*
     *  Post Metabox
     */
    public function post_metabox()
    {
        add_meta_box(
            'post-metabox',
            __( 'Featured Posts', 'orange' ),
            [ $this, 'post_metabox_render' ],
            'post',
            'side',
            'high'
        );
    }
    public function post_metabox_render($post)
    {
        wp_nonce_field( 'orange_post_meta_box_nonce', 'meta_box_nonce' );
        $featured_post_meta = get_post_meta( $post->ID );
        ?>
        <div class="orange-post-feature">
        <p class="post-feature-title"><?php _e( 'Check if this is a featured post: ', 'orangetoolz' )?></p>
        <div class="post-feature-content">
            <label for="featured-checkbox">
                <input type="checkbox" name="featured-checkbox" id="featured-checkbox" value="yes" <?php if ( isset ( $featured_post_meta['featured-checkbox'] ) ) checked( $featured_post_meta['featured-checkbox'][0], 'yes' ); ?> />
                <?php _e( 'Featured Post This Item', 'orangetoolz' )?>
            </label>
        </div>
        </div>
        <?php
    }
    public function post_metabox_save($post_id){


        if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'orange_post_meta_box_nonce' ) ){
            return true;
        }
        if (!current_user_can("edit_post", $post_id)) {
            return $post_id;
        }
        if (defined("DOING_AUTOSAVE") && DOING_AUTOSAVE) {
            return $post_id;
        }

        //save database
        if( isset( $_POST[ 'featured-checkbox' ] ) ) {
            update_post_meta( $post_id, 'featured-checkbox', 'yes' );
        } else {
            update_post_meta( $post_id, 'featured-checkbox', 'no' );
        }

        return $post_id;
    }


    /**
	 * Added UserId into metaBox
	 */
	public function userJoinMetaBox(){

		if(!check_ajax_referer( 'event-join-meta-nonce', 'security', false)){
			echo 'Nonce not varified';
			wp_die();
		}else{
		$userId = get_current_user_id();
		
		if(!empty($_POST['eventPostId']) ){

			$joinedUsers = get_post_meta($_POST['eventPostId'],'joinedUsers',true);
			$eventPayment = get_post_meta($_POST['eventPostId'],'event_payment', true);
			$joinUserInfo = array();
			$joinUserInfo['email'] = wp_get_current_user()->user_email;
			$joinUserInfo['name'] = wp_get_current_user()->display_name;
			$joinUserInfo['eventTitle'] = get_the_title( $_POST['eventPostId'] );
			$joinUserInfo['eventPermalink'] = get_permalink( $_POST['eventPostId'] );
			$joinUserInfo['eventPermalink'] = get_permalink( $_POST['eventPostId'] );
			$joinUserInfo['eventDeadline'] = get_post_meta($_POST['eventPostId'],'event_deadline', true);
			if(!empty($eventPayment) === 'yes'){
				$joinUserInfo['eventPaymentType'] = 'Required Payment';
				$joinUserInfo['eventPayAmount'] = get_post_meta( $_POST['eventPostId'], 'payable_amount', true );
			}else{
				$joinUserInfo['eventPaymentType'] = 'Free';
			}
			if(is_array($joinedUsers)){
				if(in_array($userId,$joinedUsers)){
					echo json_encode(array('Status' => true, 'message' => 'You have already joined in the event'));
					wp_die();
				}else{
					array_push($joinedUsers,$userId);
					update_post_meta( $_POST['eventPostId'],  'joinedUsers', $joinedUsers);
					$this->MailSentEventJoinConform($joinUserInfo);
					echo json_encode(array('Status' => true, 'message' => 'Thank you for Join This Event.'));
					wp_die();
				}
			}else{
				$joinedUsers = array($userId);
				update_post_meta( $_POST['eventPostId'],  'joinedUsers', $joinedUsers);
				$this->MailSentEventJoinConform($joinUserInfo);
				echo json_encode(array('Status' => true, 'message' => 'Thank you for Join This Event.'));
				wp_die();
			}

		}else{
			echo json_encode(array('Status' => true, 'message' => 'Event Join Fail!'));
			wp_die();
			}
		}
	}

	public function MailSentEventJoinConform($joinUserInfo){
        add_filter( 'wp_mail_content_type', function( $content_type ) {
			return 'text/html';
		});
		$subject = __("Event Join Successfully ".get_bloginfo( 'name'));
		$headers = array();
		$message = __('Hi : '.$joinUserInfo['name'].'', 'dumhall') . "\r\n\r\n";
		$message .= "<br>";
		$message .= __('Thank you For Joined Our Event : '.$joinUserInfo['eventTitle'].'', 'dumhall') . "\r\n\r\n";
		$message .= "<br>";
		$message .= __('Event Deadline And Time : '.$joinUserInfo['eventDeadline'].'', 'dumhall') . "\r\n\r\n";
		$message .= "<br>";
		$message .= __('Payment Status: '.$joinUserInfo['eventPaymentType'].'') . "\r\n\r\n";
		$message .= "<br>";
		if(!empty($joinUserInfo['eventPayAmount'])):
		$message .= __('Total Paid Amount: '.$joinUserInfo['eventPayAmount'].'') . "\r\n\r\n";
		$message .= "<br>";
		endif;
		$message  .= __('See Event Details by this link : '.$joinUserInfo['eventPermalink'].'') . "\r\n\r\n";
		$message .= "<br>";
		$message .= "Thank you";
		$headers[] = 'From: '.get_bloginfo( 'name').'<'.get_option('admin_email').'>'. "\r\n";
		wp_mail( $joinUserInfo['email'],$subject,$message, $headers);
		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
	}
}
new CustomMetaBox;