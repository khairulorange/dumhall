<?php

class Shortcodes{
    public function __construct()
    {
     add_shortcode('MemberSearch',[$this,'memberSearchForm']);
     add_action('wp_ajax_memberSearchFilter' , [$this,'memberSearchresultFetch']);
     add_action('wp_ajax_nopriv_memberSearchFilter',[$this,'memberSearchresultFetch']);
     add_action('wp_enqueue_scripts',[$this,'memberScript']);
    }
    public function memberScript(){

        wp_enqueue_script(  'membersearch-script', get_template_directory_uri() . '/assets/dist/js/membersearch-shortcode.js', ['jquery'],'', true);

        wp_localize_script( 'membersearch-script', 'MemberSearchObj',
            array(
                'ajax_url' => admin_url( 'admin-ajax.php' ),
                'nonce'    => wp_create_nonce('member-search-filter-nonce')
            ));
    }
    public function memberSearchresultFetch()
    {
        if(!check_ajax_referer( 'member-search-filter-nonce', 'security', false)){
            echo 'Nonce not varified';
            wp_die();
        }else{
            $department = '';
            $session = '';
            if(isset($_POST['department'])){
                $department = sanitize_text_field($_POST['department']);
            }
            if(isset($_POST['session'])){
                $session = sanitize_text_field($_POST['session']);
            }
            $args = array(
                'meta_query' => array(
                    'relation' => 'AND', // Could be OR, default is AND
                    array(
                        'key'     => 'department',
                        'value'   => $department,
                        'compare' => '=='
                    ),
                    array(
                        'key'     => 'session',
                        'value'   => $session,
                        'compare' => '=='
                    )
                )
            );
            $users = get_users( $args );
            if(!empty($users)){
                foreach ( $users as $user ) {
                    //var_dump($user->user_email);
                    // $session = $user->session;
                    $fullName = get_user_meta( $user->ID, 'name', true);
                    $department = get_user_meta( $user->ID, 'department', true);
                    $userPhoto = get_user_meta( $user->ID, 'userPhoto', true);
                   ?>
                            <div class="col-12 col-md-4 col-lg-3 mb-3">
                                <div class="member-item text-center rounded">
                                    <?php if(!empty($userPhoto)): ?>
                                        <img src="<?php echo esc_url($userPhoto); ?>" class="img-fluid rounded-circle" alt="user Photo" width="96" height="96" style="height: 96px;object-fit: cover;">
                                    <?php else: ?>
                                        <?php echo get_avatar( $user->ID, 'medium', '', 'member-profile', array('class' => array('img-fluid', 'rounded-circle') )); ?>
                                    <?php endif; ?>
                                    <?php if(!empty($fullName)): ?>
                                        <h4><?php echo esc_attr($fullName);?></h4>
                                    <?php endif; ?>
                                    <?php if(!empty($department)): ?>
                                        <h6>Department: <?php echo esc_attr($department); ?></h6>
                                    <?php endif; ?>
                                </div>
                            </div>

               <?php };
                wp_die();
            }
            else{
                echo _e('No member data Found ','dumhall');
            }


        }
    }
    public function memberSearchForm(){
        ob_start();

        ?>
        <div class="member-search-filter-wrapper">
            <div class="search-filter-container">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <form method="POST" id="MemberSearchForm" class="form-inline row">
<!--                                <div class="form-gorup col-4">-->
<!--                                    <label for="UserName" class="d-block">User Name</label>-->
<!--                                    <input class="form-control col" type="text" name="name" id="UserName" placeholder="Type name...">-->
<!--                                </div>-->
                                <div class="form-gorup col-6">
                                    <label for="SelectDepartment" class="d-block">Select Department</label>
                                    <select class="form-control " id="SelectDepartment">
                                        <?php

                                        $users = get_users( array( 'fields' => array( 'ID' ) ) );
                                        foreach($users as $user_id){
                                            $data = get_user_meta ( $user_id->ID);
                                            if(!empty($data['department'][0])){
                                                echo '<option value="' . esc_html( $data['department'][0]) . '">' . esc_html( $data['department'][0] ) . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-gorup col-6">
                                    <label for="SelectSession" class="d-block">Select Session</label>
                                    <select  class="form-control " id="SelectSession">
                                            <?php

                                            $users = get_users( array( 'fields' => array( 'ID' ) ) );
                                            foreach($users as $user_id){
                                                $data = get_user_meta ( $user_id->ID);
                                                if(isset($data['session'][0])){
                                                    echo '<option value="' . esc_html( $data['session'][0]) . '">' . esc_html( $data['session'][0] ) . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                </div>

                            </form>
                        </div>
                        <div class="du-loader">
            				<div class="du-loader-inner">
            					<span class="du-loading-icon"></span>
            					<p class="du-loading-text">Member Searching....</p>
            				</div>
            			</div>
                    </div>
                </div>
            </div>
        </div>
            <?php
     return ob_get_clean();}


}