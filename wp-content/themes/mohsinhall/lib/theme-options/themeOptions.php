<?php

/**
 * Orange settings options
 */
class ThemeOptions
{
	public function __construct()
	{
		$this->init_settings_modules();
	}
	public function init_settings_modules()
	{
		require_once __DIR__. '/general/GeneralSettings.php';
	}
}
new ThemeOptions();