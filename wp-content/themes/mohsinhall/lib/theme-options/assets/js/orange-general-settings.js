jQuery(function ($) {
    'use strict';
    $.ajax({
        method: 'GET',
        url: orange_general_settings.api.url,
        dataType: 'json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-WP-Nonce', orange_general_settings.api.nonce);
        }
    }).then(function (r) {
        for (var key in r) {
            if (r.hasOwnProperty(key)) {
                switch (key) {
                    case 'logo_img':
                        $('.site_logo.logo').attr('src', r[key]);
                        $('#logo_img').val(r[key]);
                        break;
                    case 'alt_logo_img':
                        $('.site_logo.alt_logo').attr('src', r[key]);
                        $('#alt_logo_img').val(r[key]);
                        break;
                    case 'shop_page_bg_img':
                        $('.shop_bg_img_src').attr('src', r[key]);
                        $('#shop_page_bg_img').val(r[key]);
                        break;
                    case 'contact_page_bg_img':
                        $('.contact_bg_img_src').attr('src', r[key]);
                        $('#contact_page_bg_img').val(r[key]);
                        break;
                    case 'payment_icon_img':
                        $('.payment_icon_img_src').attr('src', r[key]);
                        $('#payment_icon_img').val(r[key]);
                        break;
                }
            }
        }

        if ( r.hasOwnProperty('email') ) {
            $('#email').val( r.email );
        }
        if ( r.hasOwnProperty('phone') ) {
            $('#phone').val( r.phone );
        }
        if ( r.hasOwnProperty('info_address') ) {
            $('#info_address').val( r.info_address );
        }
        if ( r.hasOwnProperty('design_developed') ) {
            $('#design_developed').val( r.design_developed );
        }

        if ( r.hasOwnProperty('social_media') ) {
            var fields='';
            $.each(r.social_media,function(index,value) {
                fields += repeatableFieldInput(index,value);
            });
            $('.repeatable-field').replaceWith(fields);

        }



    });



    $(document).on('click','.add-field',function(e){
        e.preventDefault();
        var btn = $(this),
            repeatableFields = $('.repeatable-field');
        if(repeatableFields.length > 0){
            var lastItem = repeatableFields.last();
            lastItem.after(repeatableFieldInput(parseInt(lastItem.data('index') + 1)));
        }else{
            console.log(btn.parents('tr').after(repeatableFieldInput(parseInt(0))));
        }

    });

    $(document).on('click','.remove-field',function(e){
        e.preventDefault();
        var btn = $(this);
        btn.parents('tr').remove();

    });



    $('#orange-general-settings-form').on('submit', function (e) {
        e.preventDefault();
        var data = $(this).serializeArray();
        $.ajax({
            method: 'POST',
            url: orange_general_settings.api.url,
            beforeSend: function (xhr) {
                $('.orange-settings').fadeTo(100, 0.5);
                xhr.setRequestHeader('X-WP-Nonce', orange_general_settings.api.nonce);
            },
            data: data,
            success: function (r) {
                $('#feedback').fadeIn(100).html('<p class="success">' + orange_general_settings.strings.saved + '</p>');
            },
            error: function (r) {
                var message = orange_general_settings.strings.error;
                if (r.hasOwnProperty('message')) {
                    message = r.message;
                }
                $('#feedback').fadeIn(100).html('<p class="failure">' + message + '</p>');
            },
            complete: function () {
                setTimeout(function () {
                    $('#feedback').fadeOut(200);
                }, 2500);
                $('.orange-settings').fadeTo(100, 1);
            }

        });

    });

    $('.upload_logo').on('click', function (e) {
        e.preventDefault();
        var uploadBtn = $(this);
        var media_uploader;
        var imgProperty;
        media_uploader = wp.media({
            frame: "post",
            state: "insert",
            multiple: false
        });
        media_uploader.on("insert", function () {
            imgProperty = media_uploader.state().get("selection").first().toJSON();
            uploadBtn.prev('.site_logo','.shop_bg_img').attr('src', imgProperty.url);
            uploadBtn.next('input').val(imgProperty.url);
        });
        media_uploader.open();
    });



    var repeatableFieldInput = function(index,value){

        index = index || 0;
        var html = '',
            icon='',
            link='';
        if(typeof value !== 'undefined'){
            if(value.hasOwnProperty('icon')){
                icon = value.icon
            }
            if(value.hasOwnProperty('link')){
                link = value.link
            }
        }

        html += ' <tr data-index="'+index+'" class="repeatable-field">';

        /*icon*/
        html += '<td><p><label for="social_media['+index+'][icon]">Icon class</label></p>';
        html += '<input type="text" list="icons" id="social_media['+index+'][icon]" class="regular-text" name="social_media['+index+'][icon]" value="'+icon+'"></td>';

        /*link*/
        html += '<td><p><label for="social_media['+index+'][link]">Icon link</label></p>';
        html += '<input type="text" id="social_media['+index+'][link]" class="regular-text" name="social_media['+index+'][link]" value="'+link+'"></td>';

        /*add & remove button */
        html += '<td>' +
            '<button type="button" class="button-primary add-field">Add</button>' +
            '<button type="button" class="button-secondary remove-field" >Remove</button>' +
            '</td></tr>';

        return html;
    };


});
