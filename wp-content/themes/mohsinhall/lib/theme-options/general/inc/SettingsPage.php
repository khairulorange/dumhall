<?php

namespace General;

class SettingsPage
{
    /**
     * slug for settings page
     * @var string
     */
    protected $slug = 'orange_settings';
    /**
     * theme assets folder url
     * @var string
     */
    protected $assetsUrl;

    /**
     * saved general settings
     * @var array
     */
    private $saved_settings;

    public function __construct($assetsUrl)
    {
        $this->assetsUrl = $assetsUrl;
        $this->saved_settings = \General\Settings::getSettings();
        add_action('admin_menu', [$this, 'addOptionsPage'], 5);
        add_action('admin_menu', [$this, 'addsubmenuOptionsPage'], 5);
        add_action('admin_enqueue_scripts', [$this, 'enqueueAssets']);
    }

    /**
     * add options page in admin dashboard
     */
    public function addOptionsPage()
    {
        add_menu_page(
            'orange Options',
            'Theme Option',
            'manage_options',
            'orange_settings',
            '',
            'dashicons-admin-site'
        );

    }

    /**
     * add options page in admin dashboard
     * @return void
     */

    public function addsubmenuOptionsPage()
    {
        add_submenu_page(
            'orange_settings',
            'General Settings',
            'General',
            'manage_options',
            $this->slug,
            [$this, 'optionsPageContent']
        );

    }

    /**
     * add options page content
     * @return void
     */

    public function optionsPageContent()
    {
        ?>
        <div class="orange-settings">

            <h1><?php _e('General Settings', 'orange') ?></h1>

            <form id="orange-general-settings-form">
                <br>
                <h2><?php _e('Logo', 'orange') ?></h2>
                <p class="description"><?php _e('Upload site logo here.', 'orange') ?></p>
                <table class="form-table">
                    <tbody>
                    <tr class="img_logo">
                        <th><?php _e('Logo image', 'orange') ?></th>
                        <td>
                            <img src="." alt="" class="site_logo logo">
                            <button type="button" name="upload_logo" class="upload_logo button"><?php echo !empty($this->saved_settings['logo_img']) ? 'Change' : 'Upload' ?> logo</button>
                            <input type="hidden" value="" name="logo_img" id="logo_img">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php _e('Alternative Logo Style(<small>Footer Logo</small>)') ?>
                        </th>
                    </tr>
                    <tr class="alt_img_logo">
                        <th><?php _e('Logo image', 'orange') ?></th>
                        <td>
                            <img src="." alt="" class="site_logo alt_logo">
                            <button type="button" name="upload_logo"  class="button upload_logo"><?php echo !empty($this->saved_settings['alt_logo_img']) ? 'Change' : 'Upload' ?> logo</button>
                            <input type="hidden" value="" name="alt_logo_img" id="alt_logo_img">
                        </td>
                    </tr>
                    </tbody>
                </table>


                <hr/>
                <br/>
                <h2><?php _e('Business Info', 'orange') ?></h2>
                <p class="description"><?php _e('The following Business info settings will be applied globally all site', 'orange') ?></p>
                <table class="form-table">
                    <tbody>
                    <tr>
                        <th><?php _e('Email', 'orange') ?></th>
                        <td>
                            <input name="email" id="email" class="regular-text" type="text"/>
                        </td>
                    </tr>
                    <tr>
                        <th><?php _e('phone', 'orange') ?></th>
                        <td>
                            <input name="phone" id="phone" class="regular-text" type="text"/>
                        </td>
                    </tr>
                    <tr>
                        <th><?php _e('Address', 'orange') ?></th>
                        <td>
                            <textarea id="info_address" class="regular-text" name="info_address" rows="4" cols="53"></textarea>
                        </td>
                    </tr>

                    </tbody>
                </table>


                <hr/>
                <br/>
                <h2><?php _e('Social media', 'orange') ?></h2>
                <p class="description"><?php _e('The following social meida  settings will be applied globally', 'orange') ?></p>
                <table class="form-table">
                    <tbody>
                    <tr>
                        <th colspan="3">Socials Media Icons &nbsp;<button type="button" class="button-primary add-field">Add</button></th>
                    </tr>
                    <tr data-index="0" class="repeatable-field">
                        <td>
                            <p><label for="social_media[0][icon]">Icon Class</label></p>
                            <input type="text" list="icons" id="social_media[0][icon]" class="regular-text" name="social_media[0][icon]">
                        </td>
                        <td>
                            <p><label for="social_media[0][link]">Icon Link</label></p>
                            <input type="text" id="social_media[0][link]" class="regular-text" name="social_media[0][link]">
                        </td>
                        <td>
                            <button type="button" class="button-primary add-field">Add</button>
                            <button type="button" class="button-secondary remove-field">Remove</button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <datalist id="icons">
                                <option value="fab fa-facebook">
                                <option value="fab fa-facebook-f">
                                <option value="fab fa-facebook-square">
                                <option value="fab fa-twitter">
                                <option value="fab fa-twitter-square">
                                <option value="fab fa-google">
                                <option value="fab fa-google-plus-g">
                                <option value="fab fa-google-plus">
                                <option value="fab fa-google-plus-square">
                                <option value="fab fa-linkedin">
                                <option value="fab fa-linkedin-in">
                                <option value="fab fa-instagram">
                                <option value="fab fa-youtube">
                                <option value="fab fa-youtube-square">
                                <option value="fab fa-pinterest">
                                <option value="fab fa-pinterest-p">
                                <option value="fab fa-pinterest-square">
                                <option value="fab fa-yalp">
                                <option value="fab fa-vimeo">
                                <option value="fab fa-vimeo-v">
                                <option value="fab fa-vimeo-square">
                                <option value="fab fa-dribbble">
                                <option value="fab fa-dribbble-square">
                            </datalist>
                        </td>
                    </tr>

                    </tbody>
                </table>

                <button type="submit" class="button-primary">Submit</button>
                <span id="feedback"></span>
            </form>
        </div>

        <?php
    }

    /**
     * add assets for admin
     * localize variable for use in js
     * @return void
     */

    public function enqueueAssets()
    {
        wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_script( 'wp-color-picker' );
        wp_enqueue_style($this->slug . '-styles', $this->assetsUrl . 'css/orange-settings.css');
        wp_enqueue_media();
        wp_enqueue_script($this->slug . '-scripts', $this->assetsUrl . 'js/orange-general-settings.js', ['jquery']);
        wp_localize_script($this->slug . '-scripts', 'orange_general_settings', array(
            'strings' => [
                'saved' => __('Settings Saved successfully!!', 'orange'),
                'error' => __('An error encountered. Try again.', 'orange')
            ],
            'api'     => [
                'url' => esc_url_raw(get_bloginfo('url').'/index.php/wp-json/orange-api/v1/settings/general'),
                'nonce' => wp_create_nonce('wp_rest')
            ]
        ));
        wp_enqueue_script($this->slug);
    }

}


