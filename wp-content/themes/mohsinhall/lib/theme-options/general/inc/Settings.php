<?php

namespace General;

class Settings
{
	/**
	 * option key for saving options
	 * @var string
	 */
	protected static $optionKey = '_general_settings';

	/**
	 * get settings from db
	 * @return array
	 */

	public static function getSettings()
	{
		$saved = get_option(self::$optionKey, []);
		return $saved;
	}

	/**
	 * save settings in db
	 * @param array
	 */
	public static function saveSettings(array $settings)
	{
		$option = [];

		foreach ($settings as $key => $value) {
			if (!empty($value)) {

            if ($key == 'info_address') {
                $option[$key]= $value;
                continue;
            }

            if($key == 'social_media'){

                foreach ($value as $media){
                    $option[$key][] = [
                        'icon' => sanitize_text_field($media['icon']),
                        'link' => esc_url_raw($media['link'])
                    ];
                }
                continue;
            }

            $option[$key] = sanitize_text_field($value);
            }
		}
		update_option(self::$optionKey, $option);

		return true;
	}
}
