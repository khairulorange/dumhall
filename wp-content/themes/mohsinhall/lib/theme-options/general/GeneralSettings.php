<?php
require_once __DIR__ . '/inc/Settings.php';
require_once __DIR__ . '/inc/SettingsPage.php';
require_once __DIR__ . '/inc/API.php';

class GeneralSettings
{
    /**
     * assets directory
     * @var string
     */
    protected $assets;
    /**
     * saved general settings
     * @var array
     */
    private $settings;



    /**
     * method __construct()
     * set values in properties
     * @uses add_action(), add_shortcode() wp functions
     */
    public function __construct()
    {
        $this->assets = get_template_directory_uri() . '/lib/theme-options/assets/';
        $this->settings = General\Settings::getSettings();

        add_action('init', [$this, 'admin_setting_init']);
        add_action('rest_api_init', [$this, 'rest_init']);
        add_shortcode('orange_logo', [$this, 'render_logo']);
        add_shortcode('orange_alt_logo', [$this, 'render_alt_logo']);
        add_shortcode('orange_copyright_text', [$this, 'copyright_text_render']);
    }


    /**
     * initialize general settings api
     * @uses General\API class
     */

    public function rest_init()
    {
        $api = new General\API();
        $api->addRoutes();
    }

    /**
     * initialize setting option fileds in admin
     * @uses General\SettingsPage class
     * TroopOption namespace
     */

    public function admin_setting_init()
    {
        new General\SettingsPage($this->assets);
    }


    /**
     * render logo
     * @uses _get_logo() method
     * @return mixed
     */

    public function render_logo()
    {
        ob_start();
        if(!empty($this->settings['logo_img'])): ?>
            <div class="logo">
                <a href="<?php echo get_home_url(); ?>"><img src="<?php echo $this->settings['logo_img']; ?>" class="img-fluid" alt=""></a>
            </div>
        <?php endif;
        return ob_get_clean();
    }

    /**
     * render alternative logo
     * @uses render_alt_logo() method
     * @return mixed
     */

    public function render_alt_logo()
    {
        ob_start();
        if(!empty($this->settings['alt_logo_img'])):
            ?>
            <img src="<?php echo $this->settings['alt_logo_img']; ?>" alt="hmc-logo" class="footer-logo">
        <?php endif;
        return ob_get_clean();
    }

    /**
     * render footer copyright text
     * @uses copyright_text_render() method
     * @return string
     */


    public function copyright_text_render(){
        ob_start();
        if(!empty($this->settings['copyright_text'])):
            echo ' <p>'.$this->settings['copyright_text'].'</p>';
        endif;
        return ob_get_clean();
    }

}

$general_settings = new GeneralSettings();
