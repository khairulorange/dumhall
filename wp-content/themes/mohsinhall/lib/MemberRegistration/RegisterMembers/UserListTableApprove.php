<?php
namespace MemberRegister;


class UserListTableApprove  {

	public function __construct()
	{
		add_filter('user_row_actions', [$this, 'customAction'],10,2);

		add_action( 'wp_ajax_ApproveMemberUser', [$this, 'ApproveMemberUser']);
		add_action( 'wp_ajax_nopriv_ApproveMemberUser', array($this, 'ApproveMemberUser' ));
	}

	public function customAction($actions,$user_object)
	{
		unset( $actions['view'] );

		$status = get_user_meta($user_object->data->ID,'status',true);
		if($status){
			$actions['disable_user'] = '<a href="#" class="user-approve-status" data-id ="'.$user_object->data->ID.'" data-action ="disable">Disable</a>';
		}else{
			$actions['approve_user'] = '<a href="#" class="user-approve-status" 
			 data-action ="approve" data-id ="'.$user_object->data->ID.'">Approve</a>';
		}
		return $actions;
	}

	public function ApproveMemberUser(){

		if(!empty($_POST['dataId']) && !empty($_POST['dataAction'])){

			$userId = get_userdata($_POST['dataId']);

			$userStatusInfo = array(
				'userId' => $userId,
				'dataAction' => $_POST['dataAction'],
				'userEmail' =>  $userId->user_email,
				'userName' =>  $userId->display_name,
			);
			if($userStatusInfo['dataAction'] === 'approve'){
				update_user_meta($_POST['dataId'], 'status', 1);
				$this->MailSentRestPassword($userStatusInfo);

			}else{
				update_user_meta($_POST['dataId'], 'status', 0);
			
				 wp_delete_user($userStatusInfo['userId']->ID );
			}
			echo json_encode(array('Status' => true, 'message' => 'User Approve Success', 'reloadUserPageUrl' > home_url() . '/wp-admin/users.php'));
			wp_die();
		}else{
			echo json_encode(array('Status' => false, 'message' => 'User Approve Status Fails!'));
			wp_die();
		}


	}

	public function MailSentRestPassword($userStatusInfo){

		add_filter( 'wp_mail_content_type', function( $content_type ) {
			return 'text/html';
		});

		$subject = __("Your account is Approved".get_bloginfo( 'name'));
		$headers = array();
		$userData = $userStatusInfo['userId'];
		$userLogin = $userData->user_login;
		$key = get_password_reset_key( $userStatusInfo['userId'] );
		$message = __('Your account has been approved by admin.:') . "\r\n\r\n";
		$message .= network_home_url( '/' ) . "\r\n\r\n";
		$message .= "<br>";
		$message .= sprintf(__('Username: %s'), $userLogin) . "\r\n\r\n";
		$message .= "<br>";
		$message .= __('You can create a password by click the link and set a new password.') . "\r\n\r\n";
		$message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
		//$message .= network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($userLogin), 'login');
		$message .= "<br>";
		$message .=  home_url(). "/member-reset-password?action=rp&key=$key&login=$userLogin";
		$message .= "<br>";
		$message .= "Thank you";
		$headers[] = 'From: '.get_bloginfo( 'name').'<'.get_option('admin_email').'>'. "\r\n";

		wp_mail( $userStatusInfo['userEmail'],$subject,$message, $headers);

		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
	}


}