<?php
namespace MemberRegister;


class userProfileUpdateProcess{


	public function __construct() {
	    add_action( 'init', array($this,'custom_blockusers_init') );
	    add_action( 'wp_ajax_userProfileupdate', array($this, 'updateProfileProcess'));
		add_action( 'wp_ajax_nopriv_userProfileupdate', array($this, 'updateProfileProcess' ));
	}

	/*
	 * updateMemberProfile ajax
	 * @mixed
	 */
	public function updateProfileProcess(){

	    if(isset($_REQUEST['action']) && $_REQUEST['action']=='userProfileupdate'):
        do_action( 'wp_ajax_' . $_REQUEST['action'] );
        do_action( 'wp_ajax_nopriv_' . $_REQUEST['action'] );
        var_dump('called');
endif;
// var_dump('called');
		if(!check_ajax_referer( 'profile-update-form-nonce', 'security', false)){
			echo 'Nonce not varified';
			wp_die();
		}
		else{

			if( isset( $_POST[ 'userUpdateProfile' ] ) ) {

				$data = array();
				$metaUserValue = array();

				foreach ($_POST['formData'] as $key => $value) {
					$data[] = $value['name'];
					$metaUserValue[] = $value['value'];
				}
				$registerData = array_combine($data, $metaUserValue);

                var_dump($registerData);
				if (!empty($registerData)) {

					$metas = array(
						'name' => $registerData['name'],
						'nickName' => $registerData['nickName'],
						'qualification' => $registerData['qualification'],
						'department' => $registerData['department'],
						'session' => $registerData['session'],
						'hostelStayPeriod' => $registerData['hostelStayPeriod'],
						'lastHostelLivingRoom' => $registerData['lastHostelLivingRoom'],
						'dateOfBirth' => $registerData['dateOfBirth'],
						'bloodGroup' => $registerData['bloodGroup'],
						'nationalIdNumber' => $registerData['nationalIdNumber'],
						'occupationDesignation' => $registerData['occupationDesignation'],
						'fatherName' => $registerData['fatherName'],
						'motherName' => $registerData['motherName'],
						'spouseName' => $registerData['spouseName'],
						'childrenNumber' => $registerData['childrenNumber'],
						'son' => $registerData['son'],
						'daughter' => $registerData['daughter'],
						'phoneOfficeHome' => $registerData['phoneOfficeHome'],
						'presentAddress' => $registerData['presentAddress'],
						'permanentAddress' => $registerData['permanentAddress'],
						'mobile' => $registerData['mobile'],
						'email' => $registerData['email'],
						'status' => 0
					);

					$userId = get_current_user_id();

					if ($userId != NULL) {
						foreach ($metas as $key => $value) {
							update_user_meta($userId, $key, $value);
						}

					}
					var_dump('update success');
					echo json_encode(array('Status' => true, 'message' => 'Update Profile sucess'));
					wp_die();
				} else {
					echo json_encode(array('Status' => false, 'message' => 'Update Profile Fails'));

					wp_die();
				}
			}
		}
		var_dump('called');
	}

	public function custom_blockusers_init() {
  if ( is_admin() && !defined('DOING_AJAX') && (
  current_user_can('usercrp') || current_user_can('userpcp') ||
  current_user_can('subscriber') || current_user_can('contributor') ||
  current_user_can('editor'))) {
    session_destroy();
    wp_logout();
    wp_redirect( home_url() );
   exit;
  }
 }

}

