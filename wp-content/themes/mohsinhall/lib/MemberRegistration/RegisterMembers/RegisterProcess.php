<?php
namespace MemberRegister;

class RegisterProcess{

	protected $errors = array();
	public function __construct() {
		add_action( 'wp_ajax_MemberRegister', [$this, 'MemberRegisterProcess']);
		add_action( 'wp_ajax_nopriv_MemberRegister', array($this, 'MemberRegisterProcess' ));
	}

	/*
	 * MemberRegister ajax
	 * @mixed
	 */
	public function MemberRegisterProcess(){


		if(!check_ajax_referer( 'register-member-form-nonce', 'security', false)){
			echo 'Nonce not varified';
			wp_die();
		}else{

			if( isset( $_POST[ 'formData' ] ) ) {



				$fieldsKeyName = array();
				$fieldsValue = array();

				foreach ($_POST['formData'] as $formField) {
					$fieldsKeyName[] = $formField['name'];
					$fieldsValue[] = $formField['value'];
				}

				$registerData = array_combine($fieldsKeyName, $fieldsValue);



				$errors = array();

				if( empty( $registerData['name'] ) )

					$errors['name'] = 'Enter Full Name';

//				if( empty( $registerData['nickName'] ) )
//					$errors['nickName'] = 'Enter Nick Name';
//                elseif (username_exists( $registerData['nickName'] ))
//                    $errors['nickName'] =  'Sorry, that username already exists!';

				if( empty( $registerData['mobile'] ) )
					$errors['mobile'] = 'Enter Mobile Number';

				if( empty( $registerData['email'] ) )
					$errors['email'] = 'Enter Email Address';
				elseif( !filter_var($registerData['email'], FILTER_VALIDATE_EMAIL) )
					$errors['email'] = 'Enter Valid Email';

				elseif (email_exists($registerData['email']))
                    $errors['email'] = 'Email Already in use';

				if( empty( $registerData['qualification'] ) )
					$errors['qualification'] = 'Enter Qualification';

				if( empty( $registerData['department'] ) )
					$errors['department'] = 'Enter Department';

				if( empty( $registerData['session'] ) )
					$errors['session'] = 'Enter Session';

				if( empty( $registerData['presentAddress'] ) )
					$errors['presentAddress'] ='Please enter the present address';
//
//                if( empty( $registerData['hostelStayPeriodTo'] ) )
//                    $errors['hostelStayPeriodTo'] ='Enter Hostel Stay Period';

//				if( empty( $registerData['lastHostelLivingRoom'] ) )
//					$errors['lastHostelLivingRoom'] = 'Enter Last Hostel Living Room No';

				if( empty( $registerData['dateOfBirth'] ) )
					$errors['dateOfBirth'] = 'Enter Date of Birth';


//				if( empty( $registerData['nationalIdNumber'] ) )
//					$errors['nationalIdNumber'] = 'Enter Nation Id Number';

				if( empty( $registerData['occupationDesignation'] ) )
					$errors['occupationDesignation'] = 'Enter Occupation Designation';

                if( empty( $registerData['userPhoto'] ) )
					$errors['userPhoto'] = 'Enter Your Photo';

//				if( empty( $registerData['eventJoin'] ) )
//					$errors['eventJoin'] = 'Select Event';

                if( empty( $registerData['termsAndCondition'] ) )
                    $errors['termsAndCondition'] = 'Accept The agreement';

	            if( empty( $registerData['certificate'] ) )
					$errors['certificate'] = 'Enter Your Certificate';
				if( empty( $errors ) ) {
					if (!email_exists($registerData['email'])) {
						$userMetas = array(
							'name' => $registerData['name'],
							'nickName' => $registerData['email'],
							'qualification' => $registerData['qualification'],
							'department' => $registerData['department'],
							'session' => $registerData['session'],
							'hostelStayPeriodFrom' => $registerData['hostelStayPeriodFrom'],
							'hostelStayPeriodTo' => $registerData['hostelStayPeriodTo'],
							'lastHostelLivingRoom' => $registerData['lastHostelLivingRoom'],
							'dateOfBirth' => $registerData['dateOfBirth'],
							'bloodGroup' => $registerData['bloodGroup'],
							'nationalIdNumber' => $registerData['nationalIdNumber'],
							'occupationDesignation' => $registerData['occupationDesignation'],
							'fatherName' => $registerData['fatherName'],
							'motherName' => $registerData['motherName'],
							'spouseName' => $registerData['spouseName'],
							'childrenNumber' => $registerData['childrenNumber'],
							'son' => $registerData['son'],
							'daughter' => $registerData['daughter'],
							'phoneOfficeHome' => $registerData['phoneOfficeHome'],
							'presentAddress' => $registerData['presentAddress'],
							'permanentAddress' => $registerData['permanentAddress'],
							'mobile' => $registerData['mobile'],
							'email' => $registerData['email'],
							'eventJoin' => $registerData['eventJoin'],
							'userPhoto' => $registerData['userPhoto'],
							'certificate' => $registerData['certificate'],
							'reg_price' => get_option( 'payment_api_opt' )['registarionFee'],
                            'wanttojoin' => $registerData['wantToJoin'],
							'event_price' => 500,
							'status' => 0
						);
                       // $randomNumber = mt_srand(3);
						//$randompassword = wp_generate_password(3);
						//$userId = wp_create_user($userMetas['nickName'], $randompassword, $userMetas['email']);

				// 		if ($userId != NULL) {
				// 			foreach ($userMetas as $userMetaKey => $userMetaValue) {
				// 				update_user_meta($userId, $userMetaKey, $userMetaValue);
				// 			}
				// 		}

                        global $wpdb;



                        $table_name = $wpdb->prefix . 'temporary_users';
                        $insertConfirm = $wpdb->insert(
                            $table_name,
                            array(

                                    'time' => current_time( 'mysql' ),
                                    'name' => $userMetas['name'],
                                    'email' => $userMetas['email'],
                                    'qualification' => $userMetas['qualification'],
                                    'department' =>  $userMetas['department'],
                                    'usersession' => $userMetas['session'],
                                    'hostelStayPeriodFrom' => $userMetas['hostelStayPeriodFrom'],
                                    'hostelStayPeriodTo' => $userMetas['hostelStayPeriodTo'],
                                    'lastHostelLivingRoom' => $userMetas['lastHostelLivingRoom'],
                                    'dateOfBirth' => $userMetas['dateOfBirth'],
                                    'bloodGroup' =>$userMetas['bloodGroup'],
                                    'nationalIdNumber' => $userMetas['nationalIdNumber'],
                                    'occupationDesignation' => $userMetas['occupationDesignation'],
                                    'fatherName' => $userMetas['fatherName'],
                                    'motherName' => $userMetas['motherName'],
                                    'spouseName' => $userMetas['spouseName'],
                                    'childrenNumber' => $userMetas['childrenNumber'],
                                    'son' => $userMetas['son'],
                                    'daughter' =>$userMetas['daughter'],
                                    'phoneOfficeHome' => $userMetas['childrenNumber'],
                                    'presentAddress' => $userMetas['presentAddress'],
                                    'permanentAddress' =>$userMetas['permanentAddress'],
                                    'mobile' => $userMetas['mobile'],
                                    'wanttojoin' => $userMetas['wanttojoin'],
                                    'userPhoto' => $userMetas['userPhoto'],
                                    'certificate' => $userMetas['certificate'],
                                )
                        );


                if($insertConfirm){
                            echo json_encode(array('Status' => true, 'message' => 'Event Registration Success', 'redirectUrl' => home_url() . '/checkout?usermail='.$registerData['email']));
                        }


						//$this->MailSent($registerData);

						//echo json_encode(array('Status' => true, 'message' => 'Event Registration Success', 'redirectUrl' => home_url() . '/Thank-you'));
						//echo json_encode(array('Status' => true, 'message' => 'Event Registration Success', 'redirectUrl' => home_url() . '/checkout?usermail='.$registerData['email']));

						wp_die();
					} else {
						echo json_encode(array('Status' => false, 'message' => 'Email address already exits'));
						wp_die();
					}
				} else{
					echo json_encode(array('Status' => false, 'message' =>  $errors));
					wp_die();
				}
			}
		}
	}

	public function MailSent($registerData){
		add_filter( 'wp_mail_content_type', function( $content_type ) {
			return 'text/html';
		});
		$subject = __("Your account on ".get_bloginfo( 'name'));
		$headers = array();
		$message = "Hi ".$registerData['name']."<br>";
		$message .= "Alumni Association Member account has been created on ".get_bloginfo( 'name' );
		$message .= "<br>";
		$message .= "Please Wait for Alumni Association Admin Approval";
		$message .= "<br>";
		$message .= "Thank you";
		$headers[] = 'From: '.get_bloginfo( 'name').'<'.get_option('admin_email').'>'. "\r\n";
		wp_mail( $registerData['email'],$subject,$message, $headers);
		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
	}


}