<?php
get_header();
get_template_part( 'templates/banner', 'page' );
?>
<?php if(!is_user_logged_in()): ?>
<div  class="user-login-design member-update-password my-5">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="register">

						<div class="row">
							<div class="col-12">
								<div class="register-form mx-auto my-5">
									<form  accept-charset="utf-8" class="user-login">
										<div class="row mb-3">
											<div class="col-12 offset-md-3 col-md-6">

												<div class="status-error text-center"></div>
												<div class="status-success text-center"></div>

												<div class="form-label">Access Your Account.</div>
												<div class="update-pass bg-white p-5">
													<div class="er-field-wrap">
														<label for="name" class="float-left">Username</label>
														<div class="newPassField">
															<input type="text" name="userLogin" class="form-control input-lg userLogin" id="userLogin" />
														</div>

												</div>
												<div class="er-field-wrap pt-3">
													<label for="name" class="float-left">Password</label>
													<div class="newPassField">
														<input type="password" name="userPassword" class="form-control input-lg userPassword" id="userPassword" />
														<i id="showPassword" class="fa fa-eye fa-2x"></i>
														<i id="hiddenPassword" class="fa fa-eye-slash fa-2x"></i>
													</div>
												</div>
												<div class="er-field-wrap  mt-3">
													<div class="row align-items-center">
														<div class="col-12 col-md-8">
															<label for="rememberme">
																<input name="rememberme" id="rememberme" type="checkbox" checked="checked" value="forever" /> Remember me</label>
														</div>
														<div class="col-12 col-md-4">
															<div class="user-login-button text-right">
																<input type="submit"  value="Login" class="btn btn-lg btn-primary signup-btn rounded-0"/>
															</div>
														</div>
													</div>

												</div>

											</div>
                                                <div class="registration-link">
                                                    <p class="mb-0 pt-3 text-white">
                                                        <a class="text-white font-weight-bold pl-2" href="<?php echo home_url().'/sign-up/'; ?>">Register</a>
                                                        <a class="text-white font-weight-bold d-none float-right" href="<?php echo home_url().'/login/'; ?>">Lost your password?</a><br />
                                                    </p>
                                                </div>
										</div>
									</form>
								</div>
							</div>
						</div>


					</div>
					<div class="row mb-5">
						<div class="col-12 offset-md-3 col-md-6">
							<div class="login-page-external-links">
								<p class="mb-0 d-none">
									<a href="<?php echo home_url().'/login/'; ?>">Register</a> |
									<a href="<?php echo home_url().'/login/'; ?>">Lost your password?</a><br />
								</p>
								<p><a href="<?php echo get_option('home'); ?>">Back to <?php echo get_bloginfo(); ?></a></p>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php else: ?>
<div  class="member-registration mt-5 mb-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="already-register text-center">
					<h1 class="py-5">You Already Login Your Account!</h1>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php get_footer(); ?>