<?php get_header(); 
if ( ! is_front_page() ) :
    get_template_part( 'templates/banner', 'page' );
endif;
?>
<?php if(is_user_logged_in()): ?>
<div  class="member-profile py-4">
	<div class="container">
		<div class="row ">
			<div class="col-12">
				<div class="profile-info">
					<div class="row">
						<div class="col-12">
							<div class="profile-info">
								<?php
								$currentId = get_current_user_id();
								$userInfo = get_userdata($currentId);
								?>
								<div class="row">
									<div class="col-12 col-md-3">
										<div class="profile-sidebar pb-0">
											<?php 
											if(!empty( $userInfo->userPhoto)): ?>
											<div class="profile-userpic text-center">
												<img src="<?php echo $userInfo->userPhoto; ?>" class="img-fluid" alt="profile-photo">
											</div>
											<?php endif; ?>
											<div class="profile-usertitle">
												<div class="profile-usertitle-name">
													<?php echo $userInfo->name .' '. $userInfo->user_nicename; ?>
												</div>
												<div class="profile-usertitle-job">
													<?php echo $userInfo->occupationDesignation; ?>
												</div>
											</div>

											<div class="profile-usermenu">
												<ul class="nav">
													<li class="d-block w-100">
														<a class="d-block py-3" href="<?php echo home_url().'/member-profile/' ?>">
															<i class="glyphicon glyphicon-home"></i>
															Overview </a>
													</li>
													<li class="d-block w-100">
														<a class="py-3 d-block" href="<?php echo home_url().'/profile-update' ?>">
															<i class="glyphicon glyphicon-user"></i>
															Account Settings </a>
													</li>
													<li class="d-block w-100">
														<a class="py-3 d-block" href="<?php echo wp_logout_url( home_url().'/login' ); ?>">
															<i class="glyphicon glyphicon-user"></i>
															Logout</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="col-12 col-md-9">
										<div class="profile-content px-5 py-3">
											<table>
												<tbody>
												<tr>
													<td>Full Name:</td>
													<td><?php echo $userInfo->name.' '.$userInfo->user_nicename; ?></td>
												</tr>
												<tr>
													<td>Email Address:</td>
													<td><?php echo $userInfo->user_email; ?></td>
												</tr>
												<tr>
													<td>Qualification:</td>
													<td><?php echo $userInfo->qualification; ?></td>
												</tr>
												<tr>
													<td>Department:</td>
													<td><?php echo $userInfo->department; ?></td>
												</tr>
												<tr>
													<td>Session:</td>
													<td><?php echo $userInfo->session; ?></td>
												</tr>
												<tr>
													<td>Hostel Living Period:</td>
													<td><?php echo $userInfo->hostelStayPeriod; ?></td>
												</tr>
												<tr>
													<td>last Hostel Living Room Number:</td>
													<td><?php echo $userInfo->lastHostelLivingRoom; ?></td>
												</tr>
												<tr>
													<td>Date Of birth:</td>
													<td><?php echo $userInfo->dateOfBirth; ?></td>
												</tr>
												<tr>
													<td>Blood Group:</td>
													<td><?php echo $userInfo->bloodGroup; ?></td>
												</tr>
												<tr>
													<td>National Id Card No:</td>
													<td><?php echo $userInfo->nationalIdNumber; ?></td>
												</tr>
												<tr>
													<td>Current occupation and Designation:</td>
													<td><?php echo $userInfo->occupationDesignation; ?></td>
												</tr>
												<tr>
													<td>Father's Name:</td>
													<td><?php echo $userInfo->fatherName; ?></td>
												</tr>
												<tr>
													<td>Mother's Name:</td>
													<td><?php echo $userInfo->motherName; ?></td>
												</tr>
												<tr>
													<td>Spouse's Name:</td>
													<td><?php echo $userInfo->spouseName ? $userInfo->spouseName : 'N/A'; ?></td>
												</tr>
												<tr>
													<td>Total Children Number:</td>
													<td><?php echo $userInfo->childrenNumber ? $userInfo->childrenNumber : 'N/A'; ?></td>
												</tr>
												<tr>
													<td>Son:</td>
													<td><?php echo $userInfo->son ? $userInfo->son : 'N/A'; ?></td>
												</tr>
												<tr>
													<td>Daughter:</td>
													<td><?php echo $userInfo->daughter ? $userInfo->daughter : 'N/A'; ?></td>
												</tr>
												<tr>
													<td>phone Office/Home:</td>
													<td><?php echo $userInfo->phoneOfficeHome ? $userInfo->phoneOfficeHome : 'N/A'; ?></td>
												</tr>
												<tr>
													<td>Present Address:</td>
													<td><?php echo $userInfo->presentAddress; ?></td>
												</tr>
												<tr>
													<td>Permanent Address:</td>
													<td><?php echo $userInfo->permanentAddress; ?></td>
												</tr>
												<tr>
													<td>Mobile:</td>
													<td><?php echo $userInfo->mobile; ?></td>
												</tr>
												<tr>
													<td>Certificate:</td>
													<td>
														<?php if(!empty($userInfo->certificate)): ?>
															<img src="<?php echo $userInfo->certificate; ?>" alt="certificate" class="img-fluid">
														<?php endif; ?>
													</td>
												</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php else: ?>
<div  class="member-registration mt-5 mb-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="already-register text-center">
					<h1 class="py-5">Please Login Your Account!</h1>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php get_footer(); ?>