<?php get_header(); 
if ( ! is_front_page() ) :
    get_template_part( 'templates/banner', 'page' );
endif;
?>

<?php if(is_user_logged_in()): ?>
<div  class="member-profile py-4">
	<div class="container">
		<div class="row ">
			<div class="col-12">
				<div class="profile-info">
					<div class="row">
						<div class="col-12">
							<div class="profile-info">
								<?php
								$currentId = get_current_user_id();
								$userInfo = get_userdata($currentId);
								?>
								<div class="row">
									<div class="col-12 col-md-3">
										<div class="profile-sidebar pb-0">
											<?php if(!empty( $userInfo->userPhoto)): ?>
											<div class="profile-userpic text-center">
												<img src="<?php echo $userInfo->userPhoto; ?>" class="img-fluid" alt="profile-photo">
											</div>
											<?php endif; ?>
											<div class="profile-usertitle">
												<div class="profile-usertitle-name">
													<?php echo $userInfo->name .' '. $userInfo->user_nicename; ?>
												</div>
												<div class="profile-usertitle-job">
													<?php echo $userInfo->occupationDesignation; ?>
												</div>
											</div>

											<div class="profile-usermenu">
												<ul class="nav">
													<li class="d-block w-100">
														<a class="d-block py-3" href="<?php echo home_url().'/member-profile/' ?>">
															<i class="glyphicon glyphicon-home"></i>
															Overview </a>
													</li>
													<li class="d-block w-100">
														<a class="py-3 d-block" href="<?php echo home_url().'/profile-update' ?>">
															<i class="glyphicon glyphicon-user"></i>
															Account Settings </a>
													</li>
													<li class="d-block w-100">
														<a class="py-3 d-block" href="<?php echo wp_logout_url( home_url().'/login' ); ?>">
															<i class="glyphicon glyphicon-user"></i>
															Logout</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="col-12 col-md-9">
										<div class="profile-content p-5">

											<h4 class="text-center pb-3 ">Account Settings</h4>
											<hr class="pb-4">
											<form  accept-charset="utf-8" class="user-profile-update" >
												<div class="row mb-3">
													<div class="col-12 mb-3">
														<div class="er-field-wrap">
															<label for="name" class="float-left">NAME(<span>BLOCK LETTER</span>)</label>
															<input type="text" name="name" class="form-control input-lg er_name" value="<?php echo $userInfo->name; ?>"/>
														</div>
													</div>
													<div class="col-12">
														<div class="er-field-wrap">
															<label for="nickName" class="float-left">NICK NAME</label>
                                                            <div class="field-desabled" disabled>
                                                                <?php echo esc_attr($userInfo->user_login) ; ?>
                                                            </div>
														</div>
													</div>
												</div>
												<div class="row mb-3">
													<div class="col-xs-12 col-md-4">
														<div class="er-field-wrap">
															<label for="qualification" class="float-left">Qualification</label>
															<input type="text" name="qualification" class="form-control input-lg qualification"  value="<?php echo $userInfo->qualification; ?>" />
														</div>
													</div>
													<div class="col-xs-12 col-md-4">
														<div class="er-field-wrap">
															<label for="department" class="float-left">Department</label>
                                                            <div class="field-desabled" disabled>
                                                                <?php echo esc_attr($userInfo->department) ; ?>
                                                            </div>
														</div>
													</div>
													<div class="col-xs-12 col-md-4">
														<div class="er-field-wrap">
															<label for="session" class="float-left">Session</label>
                                                            <div class="field-desabled" disabled>
                                                                <?php echo esc_attr($userInfo->session) ; ?>
                                                            </div>
														</div>
													</div>
												</div>
												<div class="row mb-3">
													<div class="col-xs-6 col-md-6">
														<div class="er-field-wrap">
															<label for="hostelStayPeriod" class="float-left">Hostel Living Period</label>
                                                            <div class="field-desabled" disabled>
                                                                <?php echo esc_attr($userInfo->hostelStayPeriod) ; ?>
                                                            </div>
														</div>
													</div>
													<div class="col-xs-6 col-md-6">
														<div class="er-field-wrap">
															<label for="lastHostelLivingRoom" class="float-left">Hostel Last Living Room No</label>
                                                            <div class="field-desabled" disabled>
                                                                <?php echo esc_attr($userInfo->lastHostelLivingRoom) ; ?>
                                                            </div>
														</div>
													</div>
												</div>
												<div class="row mb-3">
													<div class="col-xs-12 col-md-4">
														<div class="er-field-wrap">
															<label for="dateOfBirth" class="float-left">Date Of birth</label>
                                                            <div class="field-desabled" disabled>
                                                                <?php echo esc_attr($userInfo->dateOfBirth) ; ?>
                                                            </div>
														</div>
													</div>
													<div class="col-xs-12 col-md-4">
														<div class="er-field-wrap">
															<label for="bloodGroup" class="float-left">Blood Group</label>
                                                            <div class="field-desabled" disabled>
                                                                <?php echo esc_attr($userInfo->bloodGroup) ; ?>
                                                            </div>
														</div>
													</div>
													<div class="col-xs-12 col-md-4">
														<div class="er-field-wrap">
															<label for="nationalIdNumber" class="float-left">National Id Card No</label>
                                                            <div class="field-desabled" disabled>
                                                                <?php echo esc_attr($userInfo->nationalIdNumber) ; ?>
                                                            </div>
														</div>
													</div>
												</div>
												<div class="row mb-3">
													<div class="col-xs-12 col-md-12">
														<div class="er-field-wrap">
															<label for="occupationDesignation" class="float-left">Current occupation and Designation</label>
															<input type="text" name="occupationDesignation" class="form-control input-lg occupationDesignation" value="<?php echo $userInfo->occupationDesignation; ?>"  />
														</div>
													</div>
												</div>
												<div class="row mb-3">
													<div class="col-12">
														<div class="er-field-wrap mb-3">
															<label for="fatherName" class="float-left">Father's Name</label>
                                                            <div class="field-desabled" disabled>
                                                                <?php echo esc_attr($userInfo->fatherName) ; ?>
                                                            </div>
														</div>
														<div class="er-field-wrap mb-3">
															<label for="motherName" class="float-left">Mother's Name</label>
                                                            <div class="field-desabled" disabled>
                                                                <?php echo esc_attr($userInfo->motherName) ; ?>
                                                            </div>
														</div>
														<div class="er-field-wrap mb-3">
															<label for="spouseName" class="float-left">Spouse's Name</label>
															<input type="text" name="spouseName" class="form-control input-lg spouseName" value="<?php echo $userInfo->spouseName; ?>"  />
														</div>
														<div class="er-field-wrap  mb-3">
															<div class="row">
																<div class="col-12 col-md-4">
																	<div class="er-field-wrap">
																		<label for="childrenNumber" class="float-left">Total Children</label>
																		<input type="number" name="childrenNumber" class="form-control input-lg childrenNumber" value="<?php echo $userInfo->childrenNumber; ?>"/>
																	</div>
																</div>
																<div class="col-12 col-md-4">
																	<div class="er-field-wrap">
																		<label for="son" class="float-left">Son</label>
																		<input type="number" name="son" class="form-control input-lg son"  value="<?php echo $userInfo->son; ?>"/>
																	</div>
																</div>
																<div class="col-12 col-md-4">
																	<div class="er-field-wrap">
																		<label for="daughter" class="float-left">Daughter</label>
																		<input type="number" name="daughter" class="form-control input-lg daughter"  value="<?php echo $userInfo->daughter; ?>"/>
																	</div>
																</div>
															</div>
														</div>
														<div class="er-field-wrap mb-3">
															<label for="phoneOfficeHome" class="float-left">Phone: Office/Home </label>
															<input type="text" name="phoneOfficeHome" class="form-control input-lg phoneOfficeHome"  value="<?php echo $userInfo->phoneOfficeHome;?>"/>
														</div>
													</div>
													<div class="col-12">
														<div class="er-field-wrap mb-3">
															<label for="presentAddress" class="float-left">Present Address: </label>
															<textarea name="presentAddress" class="form-control input-lg presentAddress" rows="5" cols="40"><?php echo $userInfo->presentAddress;?></textarea>
														</div>
														<div class="er-field-wrap">
															<label for="permanentAddress" class="float-left">Permanent Address: </label>
                                                            <div class="field-desabled" disabled>
                                                                <?php echo esc_attr($userInfo->permanentAddress) ; ?>
                                                            </div>
														</div>
													</div>
												</div>
												<div class="row mb-3">
													<div class="col-12">
														<div class="er-field-wrap">
															<label for="mobile" class="float-left">Mobile</label>
															<input type="tel" name="mobile" class="form-control input-lg mobile" value="<?php echo $userInfo->mobile; ?>"/>
														</div>
													</div>

												</div>

												<div class="row mb-3">
													<div class="col-12">
														<div class="er-field-wrap">
															<label for="email" class="float-left">Email</label>

															<input type="email" name="email" class="form-control input-lg email"  value="<?php echo $userInfo->user_email; ?>"  />
														</div>
													</div>
												</div>

												<div class="row mt-5 mb-3">
													<div class="col-xs-6 col-md-12">
														<div class="register-btn-wrapper text-left">
															<button class="btn btn-lg btn-primary rounded-0" type="submit">Update Account Settings</button>
														</div>
                                                        <div class="status-info  mt-3">
                                                            <div class="status-success"></div>
                                                            <div class="status-error"></div>
                                                        </div>

													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php else: ?>
<div  class="member-registration mt-5 mb-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="already-register text-center">
					<h1 class="py-5">Please Login Your Account!</h1>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php get_footer(); ?>