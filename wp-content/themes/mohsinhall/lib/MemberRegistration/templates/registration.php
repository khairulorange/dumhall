<?php get_header(); 
if ( ! is_front_page() ) :
    get_template_part( 'templates/banner', 'page' );
endif;
?>
<?php if(!is_user_logged_in()): ?>
<div  class="member-registration mt-5 mb-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="register">
					<div class="status-success"></div>
					<div class="row ">
						<div class="col-12">
							<div class="register-heading text-center">
								<img src="<?php echo get_template_directory_uri();?>/assets/img/banner.jpg" class="img-fluid">
								<h3 class="m-5">Alumni Association Membership Form</h3>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="register-form mx-auto">
								<div class="status-error"></div>

								<form  accept-charset="utf-8" class="member-register" enctype="multipart/form-data" method="post">
									<div class="row mb-4">
										<div class="col-xs-6 col-md-6">
											<div class="er-field-wrap">
												<label for="name" class="float-left">Full NAME <span class="require-field text-danger">*</span></label>
												<input type="text" name="name" class="form-control input-lg name" />
												<span id="emptyName" class="text-danger"></span>
											</div>
										</div>
                                        <div class="col-xs-6 col-md-6">
                                            <div class="er-field-wrap">
                                                <label for="email" class="float-left">Email <span class="require-field text-danger">*</span></label>
                                                <input type="email" name="email" class="form-control input-lg email"  />
                                                <span id="emptyEmail" class="text-danger"></span>
                                            </div>
                                        </div>
<!--										<div class="col-xs-6 col-md-6">-->
<!--											<div class="er-field-wrap">-->
<!--												<label for="nickName" class="float-left">User Name <span class="require-field text-danger">*</span></label>-->
<!--												<input type="text" name="nickName" class="form-control input-lg nickName"  />-->
<!--												<span id="emptyNickName" class="text-danger"></span>-->
<!--											</div>-->
<!--										</div>-->
<!--										<div class="col-xs-4 col-md-4">-->
<!--											<div class="er-field-wrap">-->
<!--												<label for="formNumer" class="float-left">Form Numer <span class="require-field text-danger">*</span></label>-->
<!--												<input type="text" name="formNumer"  class="form-control input-lg formNumer"  />-->
<!--												<span id="emptyFormNumer" class="text-danger"></span>-->
<!--											</div>-->
<!--										</div>-->
									</div>
									<div class="row mb-4">
										<div class="col-xs-12 col-md-4">
											<div class="er-field-wrap">
												<label for="qualification" class="float-left">Qualification <span class="require-field text-danger">*</span></label>
												<input type="text" name="qualification" class="form-control input-lg qualification"  />
												<span id="emptyQualification" class="text-danger"></span>
											</div>
										</div>
										<div class="col-xs-12 col-md-4">
											<div class="er-field-wrap">
												<label for="department" class="float-left">Department <span class="require-field text-danger">*</span></label>
												<input type="text" name="department" class="form-control input-lg department"   />
												<span id="emptyDepartment" class="text-danger"></span>
											</div>
										</div>
										<div class="col-xs-12 col-md-4">
											<div class="er-field-wrap">
												<label for="session" class="float-left">Session <span class="require-field text-danger">*</span></label>
                                                <select name="session" id="session" class="form-control">
                                                    <?php
                                                    $b_year='1930';
                                                    $year = date('Y');
                                                    for ($i=$year;$i>=$b_year;$i--){
                                                        echo ' <option value="'.$i.'-'.($i+1).'">'.$i.'-'.($i+1).'</option>';
                                                    }
                                                    ?>
                                                </select>
												<span id="emptySession" class="text-danger"></span>
											</div>
										</div>
									</div>
									<div class="row mb-4">
										<div class="col-xs-6 col-md-4">
											<div class="er-field-wrap">
												<label for="hostelStayPeriodFrom" class="float-left">Hall Living Period From</label>
												<input type="text" name="hostelStayPeriodFrom" class="form-control input-lg hostelStayPeriodFrom"/>
												<span id="emptyHostelStayPeriodFrom" class="text-danger"></span>
											</div>
										</div>
                                        <div class="col-xs-6 col-md-4">
                                            <div class="er-field-wrap">
                                                <label for="hostelStayPeriodTo" class="float-left">To</label>
                                                <input type="text" name="hostelStayPeriodTo" class="form-control input-lg hostelStayPeriodTo"/>
                                                <span id="emptyHostelStayPeriodTo" class="text-danger"></span>
                                            </div>
                                        </div>
										<div class="col-xs-6 col-md-4">
											<div class="er-field-wrap">
												<label for="lastHostelLivingRoom" class="float-left">Hall Last Living Room No </label>
												<input type="number" name="lastHostelLivingRoom"  class="form-control input-lg lastHostelLivingRoom"  />
												<span id="emptyLastHostelLivingRoom" class="text-danger"></span>
											</div>
										</div>
									</div>
									<div class="row mb-4">
										<div class="col-xs-12 col-md-4">
											<div class="er-field-wrap">
												<label for="dateOfBirth" class="float-left">Date Of birth <span class="require-field text-danger">*</span></label>
												<input type="text" name="dateOfBirth" class="form-control input-lg dateOfBirth"  />
												<span id="emptyDateOfBirth" class="text-danger"></span>
											</div>
										</div>
										<div class="col-xs-12 col-md-4">
											<div class="er-field-wrap">
												<label for="bloodGroup" class="float-left">Blood Group</label>
<!--												<input type="text" name="bloodGroup" class="form-control input-lg bloodGroup" />-->
                                                <select name="bloodGroup" class="form-control">
                                                    <option disabled selected value>Blood Group</option>
                                                    <option  value ="A+" >A+</option>
                                                    <option value ="A-" >A-</option>
                                                    <option value ="B+" >B+</option>
                                                    <option value ="B-" >B-</option>
                                                    <option value ="AB+" >AB+</option>
                                                    <option value ="AB-" >AB-</option>
                                                    <option value ="O+" >O+</option>
                                                    <option value ="O-" >O-</option>
                                                </select>
											</div>
										</div>
										<div class="col-xs-12 col-md-4">
											<div class="er-field-wrap">
												<label for="nationalIdNumber" class="float-left">National Id Card No </label>
												<input type="number" name="nationalIdNumber" class="form-control input-lg nationalIdNumber"  />
												<span id="emptyNationalIdNumber" class="text-danger"></span>
											</div>
										</div>
									</div>
									<div class="row mb-4">
										<div class="col-xs-12 col-md-12">
											<div class="er-field-wrap">
												<label for="occupationDesignation" class="float-left">Current occupation and Designation <span class="require-field text-danger">*</span></label>
												<input type="text" name="occupationDesignation" class="form-control input-lg occupationDesignation"  />
												<span id="emptyOccupationDesignation" class="text-danger"></span>
											</div>
										</div>
									</div>
									<div class="row mb-4">
										<div class="col-xs-12 col-md-6">
											<div class="er-field-wrap">
												<label for="fatherName" class="float-left">Father's Name</label>
												<input type="text" name="fatherName" class="form-control input-lg fatherName"  />
											</div>
											<div class="er-field-wrap">
												<label for="motherName" class="float-left">Mother's Name</label>
												<input type="text" name="motherName" class="form-control input-lg motherName"  />
											</div>
											<div class="er-field-wrap">
												<label for="spouseName" class="float-left">Spouse's Name</label>
												<input type="text" name="spouseName" class="form-control input-lg spouseName"  />
											</div>
											<div class="er-field-wrap">
												<div class="row">
													<div class="col-12 col-md-4">
														<div class="er-field-wrap">
															<label for="childrenNumber" class="float-left">Total Children</label>
															<input type="number" name="childrenNumber" class="form-control input-lg childrenNumber" />
														</div>
													</div>
													<div class="col-12 col-md-4">
														<div class="er-field-wrap">
															<label for="son" class="float-left">Son</label>
															<input type="number" name="son" class="form-control input-lg son"  />
														</div>
													</div>
													<div class="col-12 col-md-4">
														<div class="er-field-wrap">
															<label for="daughter" class="float-left">Daughter</label>
															<input type="number" name="daughter" class="form-control input-lg daughter"  />
														</div>
													</div>
												</div>
											</div>
											<div class="er-field-wrap">
												<label for="phoneOfficeHome" class="float-left">Phone: Office/Home </label>
												<input type="tel" name="phoneOfficeHome" class="form-control input-lg phoneOfficeHome"  />
											</div>
										</div>
										<div class="col-xs-12 col-md-6">
											<div class="er-field-wrap">
												<label for="presentAddress" class="float-left">Present Address : <span class="require-field text-danger">*</span>  </label>
												<textarea name="presentAddress"  class="form-control input-lg presentAddress" rows="5" cols="40"></textarea>
                                                <span id="emptyPresentAddress" class="text-danger"></span>
											</div>
											<div class="er-field-wrap">
												<label for="permanentAddress" class="float-left">Permanent Address: </label>
												<textarea name="permanentAddress" class="form-control input-lg permanentAddress" rows="5" cols="40" ></textarea>
											</div>
										</div>
									</div>
									<div class="row mb-4">
										<div class="col-xs-6 col-md-6">
											<div class="er-field-wrap">
												<label for="mobile" class="float-left">Mobile <span class="require-field text-danger">*</span></label>
												<input type="tel" name="mobile" class="form-control input-lg mobile"/>
												<span id="emptyMobile" class="text-danger"></span>
											</div>
										</div>
<!--										<div class="col-xs-6 col-md-6">-->
<!--											<div class="er-field-wrap">-->
<!--												<label for="email" class="float-left">Email <span class="require-field text-danger">*</span></label>-->
<!--												<input type="email" name="email" class="form-control input-lg email"  />-->
<!--												<span id="emptyEmail" class="text-danger"></span>-->
<!--											</div>-->
<!--										</div>-->

<!--                                        <div class="col-xs-6 col-md-6">-->
<!--                                            <div class="er-field-wrap">-->
<!--                                                <div class=select-even>-->
<!--                                                    <label for="eventJoin" class="float-left">Choose Membership<span class="require-field text-danger">*</span></label>-->
<!--                                                    --><?php
//
//                                                    //                                                    $posts = get_posts(array('post_type'=> 'events', 'post_status'=> 'publish', 'suppress_filters' => false, 'posts_per_page'=>-1));
//                                                    //                                                    echo '<select class="form-control" name="eventJoin" id="'.$select_id.'">';
//                                                    //                                                    echo '<option selected="selected" value = "" >Select Event '.$label.' </option>';
//                                                    //                                                    foreach ($posts as $post) {
//                                                    //                                                        echo '<option value="', $post->ID, '"', $selected == $post->ID ? ' selected="selected"' : '', '>', $post->post_title, '</option>';
//                                                    //                                                    }
//                                                    //                                                    echo '</select>';
//
//                                                    ?>
<!--                                                    <select name="eventJoin" class="form-control">-->
<!--                                                        <option disabled selected value>Select Membership</option>-->
<!--                                                        <option  value ="1" >Life Membership+Reunion</option>-->
<!--                                                        <option value ="2" >Life Membership only</option>-->
<!--                                                    </select>-->
<!--                                                    <span id="emptyEvent" class="text-danger"></span>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->


									</div>


									<div class="row mb-4">
										<div class="col-12 col-md-6">
											<div class="er-field-wrap">
												<div class="user-photo-upload">
													<div class="photo_deleted_message"></div>
													<div class="photo-upload-wrapper" id="photo-upload-wrapper">
														<input type="file" class="user-photo-upload-input" id="user-photo-upload-input" style="opacity:0;" />
														<p class="user-photo-upload-text"><?php _e( 'Upload your Photo', 'dumhall' ); ?> </p>
													</div>
													<div id="user_photo_upload_preview" class="file-upload photo-preview user_photo_upload_preview" style="display:none;">
														<div class="user-photo-preview mb-2"></div>
														<button data-fileurl="" class="uploaded_photo_delete">
															<i class="fa fa-window-close" aria-hidden="true"></i>
														</button>
													</div>
													<div class="photo-upload">
														<label for="name">Upload Profile Photo: <span class="require-field text-danger">*</span></label>
													</div>
													<input type="hidden" name="userPhoto" id="userPhoto" required>
													<span id="emptyUserPhoto" class="text-danger"></span>
												</div>
											</div>
										</div>
										<div class="col-12 col-md-6">
											<div class="er-field-wrap">
												<div class="ibenic_upload_message"></div>

												<div id="ibenic_file_upload" class="file-upload">
													<input type="file" id="ibenic_file_input" style="opacity:0;" />
													<p class="ibenic_file_upload_text"><?php _e( 'Upload your file', 'ibenic_upload' ); ?></p>
												</div>
												<div id="ibenic_file_upload_preview" class="file-upload file-preview" style="display:none;">
													<div class="ibenic_file_preview"></div>
													<button data-fileurl="" class="ibenic_file_delete">
														<i class="fa fa-window-close" aria-hidden="true"></i>
													</button>
												</div>
												<div class="photo-upload">
													<label for="name">Upload Certificate Scan File: <span class="require-field text-danger">*</span></label>
												</div>
												<input type="hidden" name="certificate" id="certificate" value="" required>
												<span id="emptyCertificate" class="text-danger"></span>
											</div>
										</div>
									</div>

                                    <?php
                                        // WP_Query arguments
                                        $args = array(
                                            'post_type' => 'events',
                                            'post_status' => 'publish',
                                            'nopaging' => true,
                                            'order' => 'ASC',
                                            'orderby' => 'menu_order',
                                            'posts_per_page' => 1,
                                        );

                                        // The Query
                                        $events = new WP_Query($args);

                                        if($events->have_posts()){

                                        ?>


                                    <div class="row mt-3 ml-2">
                                        <input type="checkbox" class="agreeToJoin"  id="agreeToJoin" style="height: 22px;width: 16px"> <label class="mb-0" for="agreeToJoin" >&nbsp I'm Interested to join upcomming Events.</label>
                                        <input type="hidden" value="" id="target_events_list" name="wantToJoin">
                                    </div>
                                    <div id="events_lists" class="row ml-2 border">
                                        <div class="col-12 evenlists_innner">

                                            <?php


                                            $datetime1 = new DateTime(current_time('F j, Y g a'));

                                            if ( $events->have_posts() ) {
                                                while ( $events->have_posts() ) : $events->the_post();

                                                    $eventDeadLine = get_field('event_deadline');
                                                    $eventPrice = get_field('payable_amount');
                                                    $datetime2 = new DateTime($eventDeadLine);

                                                    if($datetime2>=$datetime1){  ?>

                                                        <div class="col-md-3 pl-0">
                                                            <input type="checkbox"  data-title ="<?php the_title() ?>" data-price="<?php echo isset($eventPrice)?$eventPrice : 0  ; ?>" class="single_event"  id="<?php the_ID(); ?>" >
                                                            <label class="mb-0" for="<?php the_ID(); ?>" >&nbsp <?php the_title();?> </label>
                                                        </div>

                                                        <?php
                                                    }

                                                endwhile;
                                            }else {
                                                echo "No event availe now";
                                            }

                                            // Restore original Post Data
                                            wp_reset_postdata();

                                            ?>

                                        </div>

                                    </div>

                                    <?php

                                    }

                                    ?>

                                    <div class="row mb-3 col-6 mt-3 ">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Product Name</th>
                                                <th>Price</th>
                                            </tr>
                                            </thead>
                                            <tbody id="event-items">
                                                <tr>
                                                    <td>1</td>
                                                    <td>Registraton Fee <span class="require-field text-danger">*</span></td>
                                                    <td class="registration_fee"> <?php  echo get_option( 'payment_api_opt' )['registarionFee'] . '/- BDT' ?></td>
                                                </tr>

                                            </tbody>
                                            <tfoot style="background: #c4d0da" >
                                                <tr>
                                                    <th colspan="2">Total Ammount</th>
                                                    <th class="total_price"> <span><?php  echo get_option( 'payment_api_opt' )['registarionFee'] ?></span>/- BDT</th>
                                                </tr>
                                            </tfoot>

                                        </table>
<!--                                           <p class="mb-0 p-2 bg-warning"><span class="text-danger">*</span> <b> Registation Fee: --><?php // echo get_option( 'payment_api_opt' )['registarionFee'] . ' BDT' ?><!--</b> <span class=" ml-3 text-danger">*</span> <b>Event Joing Fee: 1000 BDT</b></p>-->
                                    </div>
                                    <div class="row mt-3 ml-2">
                                        <input type="checkbox" name="termsAndCondition" value="agree" class="mr-2" id="termsAndCondition"  style="height: 22px;width: 16px"> <label class="mb-0" for="termsAndCondition"> I have read and agree to the&nbsp</label>
                                        <a class="font-weight-bold" target="_blank" href="<?php echo site_url() ?>/terms-conditions/">Terms and Conditions</a>  <span class="require-field text-danger">*</span>
                                    </div>
                                    <div class="row mb-3  ml-2">
                                        <span id="emptyTermsAndCondition" class="text-danger"></span>
                                    </div>
									<div class="row mb-3 mt-3 ml-2">
										<div class="col-xs-6 col-md-12 p-0">
                                            <div class="error py-2" id="all-errors"></div>
<!--                                            <input type="hidden" name="reg_price" id="reg_price" value="150" >-->
											<div class="register-btn-wrapper text-left">
												<button class="btn btn-lg btn-primary signup-btn rounded-0" type="submit">Register</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php else: ?>
<div  class="member-registration mt-5 mb-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="already-register text-center">
					<h1 class="py-5">You Already Have an Account</h1>
				</div>
			</div>
		</div>
	</div>
</div>




<?php endif; ?>

    <script>
        (function($) {
            $(".agreeToJoin").click(function(){
                 $("#events_lists").slideToggle();
            });
        })( jQuery );

        var evnetList = document.querySelectorAll('#events_lists input');
        var eventCartItems = document.querySelector('#event-items');
        var target_events_Field = document.querySelector('#target_events_list');

        var targetEvent =[];

        evnetList.forEach(function(single_item){
            single_item.addEventListener('click', function(){
                if(single_item.checked){

                    eventCartItems.innerHTML += `<tr class="${single_item.getAttribute("id")}"><td></td><td>${$(this).data("title")}</td><td>${$(this).data("price")+'/- BDT'}</td></tr>`;
                    $('.total_price span').html( Number($('.total_price span').text()) +  Number($(this).data("price")));
                    targetEvent.push(`${single_item.getAttribute("id")}`)
                    target_events_Field.value=targetEvent;

                }else{
                   var tt = single_item.getAttribute("id");
                    $('.'+tt).remove();
                    $('.total_price span').html( Number($('.total_price span').text()) - Number($(this).data("price")));

                    var index = targetEvent.indexOf(`${single_item.getAttribute("id")}`);
                    if (index > -1) {
                        targetEvent.splice(index, 1);
                    }
                    target_events_Field.value=targetEvent;

                }
            })
        })

        evnetList.forEach(function(single_item){
            single_item.addEventListener('click', function(){
                let rowcountcart =0;
                eventCartItems.querySelectorAll('tr').forEach(function(single_node){
                    rowcountcart++
                    single_node.querySelector('td').textContent = rowcountcart;
                })

            })
        })
    </script>

<style>
    #events_lists{
        display:none;
        padding: 10px;
        margin-top: 10px;
    }
</style>
<?php get_footer(); ?>