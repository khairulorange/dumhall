jQuery(function ($) {
    'use strict';
    $('.user-profile-update').on('submit', function (e) {

        e.preventDefault();
        var formData = $(this).serializeArray();

        $.ajax({
            method: 'POST',
            url: userProfileUpdateObj.ajax_url,

             beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', userProfileUpdateObj.nonce);
            },
            data: {
                action : 'userProfileUpdate',
                security: userProfileUpdateObj.nonce,
                formData : formData
            },
            success: function (r) {
                console.log(r);
                var response = $.parseJSON(r);
                console.log(response);
                if(response.Status === false){
                    $('.status-error').fadeIn(100).html('<p class="alert alert-danger">' + response.message + '</p>');
                }else{
                    $('.status-success').fadeIn(100).html('<p class="alert alert-success">' + response.message + '</p>');
                }
            },
            error: function (r) {
                console.log(r);
            },
            complete: function () {
                setTimeout(function () {
                    $('.status-success').fadeOut(1000);
                }, 400);
            }
        });

    });
});

