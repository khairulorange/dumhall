jQuery(function ($) {
    'use strict';



    $('.member-register').on('submit', function (e) {
        e.preventDefault();

        $('span.text-danger').text(' ');

        var formSerializeArray = $(this).serializeArray();

        $.ajax({
            method: 'POST',
            url: RegistrationObj.ajax_url,
            beforeSend: function (xhr) {
                $('.member-registration .register').css('opacity','0.5');
                xhr.setRequestHeader('X-WP-Nonce', RegistrationObj.nonce);
            },
            data: {
                action : 'MemberRegister',
                security: RegistrationObj.nonce,
                formData : formSerializeArray
            },
            success: function (r) {
                var response = $.parseJSON(r);

                if(response.Status === false){

                    $('#all-errors').html('<span class="text-danger">Something Wrong! Please check above Fields</span>');

                    if ( response.message.hasOwnProperty('name') ) {
                        $('#emptyName').html( response.message.name);
                    }
                    // if ( response.message.hasOwnProperty('nickName') ) {
                    //     $('#emptyNickName').html( response.message.nickName);
                    // }

                    if ( response.message.hasOwnProperty('mobile') ) {
                        $('#emptyMobile').html( response.message.mobile);
                    }
                    if ( response.message.hasOwnProperty('email') ) {
                        $('#emptyEmail').html( response.message.email);
                    }
                    if ( response.message.hasOwnProperty('qualification') ) {
                        $('#emptyQualification').html( response.message.qualification);
                    }
                    if ( response.message.hasOwnProperty('session') ) {
                        $('#emptySession').html( response.message.session);
                    }
                    if ( response.message.hasOwnProperty('department') ) {
                        $('#emptyDepartment').html( response.message.department);
                    }
                    if ( response.message.hasOwnProperty('presentAddress') ) {
                        $('#emptyPresentAddress').html( response.message.presentAddress);
                    }
                    // if ( response.message.hasOwnProperty('hostelStayPeriodTo') ) {
                    //     $('#emptyHostelStayPeriodTo').html( response.message.hostelStayPeriodTo);
                    // }
                    // if ( response.message.hasOwnProperty('lastHostelLivingRoom') ) {
                    //     $('#emptyLastHostelLivingRoom').html( response.message.lastHostelLivingRoom);
                    // }
                    if ( response.message.hasOwnProperty('dateOfBirth') ) {
                        $('#emptyDateOfBirth').html( response.message.dateOfBirth);
                    }
                    // if ( response.message.hasOwnProperty('nationalIdNumber') ) {
                    //     $('#emptyNationalIdNumber').html( response.message.nationalIdNumber);
                    // }
                    if ( response.message.hasOwnProperty('occupationDesignation') ) {
                        $('#emptyOccupationDesignation').html( response.message.occupationDesignation);
                    }
                    if ( response.message.hasOwnProperty('userPhoto') ) {
                        $('#emptyUserPhoto').html( response.message.userPhoto);
                    }
                    if ( response.message.hasOwnProperty('certificate') ) {
                          $('#emptyCertificate').html( response.message.certificate);
                    }
                    // if ( response.message.hasOwnProperty('eventJoin') ) {
                    //       $('#emptyEvent').html( response.message.eventJoin);
                    // }
                    if ( response.message.hasOwnProperty('termsAndCondition') ) {
                        $('#emptyTermsAndCondition').html( response.message.termsAndCondition);
                    }



                }else{
                    $('.status-success').fadeIn(100).html('<p class="alert alert-success">' + response.message + '</p>');
                    window.location.href = response.redirectUrl;
                }
            },
            error: function (r) {
                console.log($.parseJSON(r));
            },
            complete: function () {
	            $('.member-registration .register').css('opacity','1');
                setTimeout(function () {
                    $('.status-success').fadeOut(100);
                }, 400);
            }
        });
    });

    $( function() {
        $( ".dateOfBirth" ).datepicker({
			changeMonth: true,
			changeYear: true
		});
		 $( ".hostelStayPeriodFrom, .hostelStayPeriodTo" ).datepicker({
			changeMonth: true,
			changeYear: true,
             yearRange: "c-98:c+0",
		});
        
    } );


});
