jQuery(function ($) {
    'use strict';

    $('.user-approve-status').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            method: 'POST',
            url: UserApproveObj.ajax_url,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', UserApproveObj.nonce);
            },
            data: {
                action : 'ApproveMemberUser',
                security: UserApproveObj.nonce,
                dataId : $(this).data('id'),
                dataAction : $(this).data('action'),
            },
            success: function (r) {
                var response = $.parseJSON(r);
                if(response.Status === false){
                    $('.status-error').html('<p>'+response.message+'</p>');
                }else{
                    $('.status-success').fadeIn(100).html('<p>' + response.message + '</p>');
                    location.reload(response.reloadUserPageUrl);
                }
            },
            error: function (r) {
                console.log($.parseJSON(r));
            },
            complete: function () {
                setTimeout(function () {
                    $('.register-success').fadeOut(100);
                }, 400);
            }
        });


    });
});
