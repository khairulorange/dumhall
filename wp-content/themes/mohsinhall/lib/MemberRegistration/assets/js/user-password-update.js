jQuery(function ($) {
    'use strict';


    $("#showPassword").click(function(){
        $("#showPassword").hide();
        $("#hiddenPassword").show();
        $("#userPassword").attr("type","text");
    });

    $("#hiddenPassword").click(function(){
        $("#hiddenPassword").hide();
        $("#showPassword").show();
        $("#userPassword").attr("type","password");
    });

    function checkPasswordStrength( passValue,$strengthResult,$submitButton,blacklistArray ) {

        var NewUserPass = passValue.val();

        // Reset the form & meter
        $submitButton.attr( 'disabled', 'disabled' );
        $strengthResult.removeClass( 'short bad good strong alert-danger alert-warning alert-success alert-info' );

        // Extend our blacklist array with those from the inputs & site data
        blacklistArray = blacklistArray.concat( wp.passwordStrength.userInputBlacklist() )

        // Get the password strength
        var strength = wp.passwordStrength.meter( NewUserPass,blacklistArray);


        // Add the strength meter results
        switch ( strength ) {

            case 2:
                $strengthResult.addClass( 'bad d-block text-center alert alert-warning mt-2 py-2 rounded-0' ).html( pwsL10n.bad );
                break;

            case 3:
                $strengthResult.addClass( 'good d-block text-center alert alert-info mt-2 py-2 rounded-0' ).html( pwsL10n.good );
                break;

            case 4:
                $strengthResult.addClass( 'strong d-block text-center alert alert-success mt-2 py-2 rounded-0' ).html( pwsL10n.strong );
                break;

            default:
                $strengthResult.addClass( 'short d-block text-center alert alert-danger mt-2 py-2 rounded-0' ).html( pwsL10n.short );

        }

        // The meter function returns a result even if pass2 is empty,
        // enable only the submit button if the password is strong and
        // both passwords are filled up

        if ( 2 > strength   ) {
            if(NewUserPass.length >= 2){
                $('.resetPassweak').css("display","block");
                $('.userSetWeakPass').on('click',function(){
                    if($(this).prop('checked')){
                        $(this).change(function() {
                            $submitButton.removeAttr('disabled');
                        });
                    }else{
                        $(this).change(function() {
                            $submitButton.attr( 'disabled', 'disabled' );
                        });
                    }
                });
            }else{
                $('.resetPassweak').css("display","none");
            }
        }else{
            $('.resetPassweak').css("display","none");
        }
        if ( 2 <= strength  ) {
            $submitButton.removeAttr( 'disabled' );
        }
        return strength;
    }

    $( 'body').on( 'keyup', 'input[name=password]',
        function( event ) {
            checkPasswordStrength(
                $( 'input[name=password]' ),         // First password field
                $( '#password-strength' ),           // Strength meter
                $( '.register-btn-wrapper input[type=submit]' ),           // Submit button
                ['black', 'listed', 'word']          // Blacklisted words
            );
        }
    );

    $('.update-user-password').on('submit', function (e) {
        e.preventDefault();
        
        var updatePasswordUser = $(this).serializeArray();
        $.ajax({
            method: 'POST',
            url: userPasswordUpdateObj.ajax_url,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', userPasswordUpdateObj.nonce);
            },
            data: {
                action : 'userPasswordUpdate',
                security: userPasswordUpdateObj.nonce,
                resetPassInfo : updatePasswordUser
            },
            success: function (r) {
              var response = $.parseJSON(r);
                if(response.Status === false){
                     $('.status-error').fadeIn(100).html('<p class="alert alert-danger">' + response.message + '</p>');
                }else{
                     $('.status-success').fadeIn(100).html('<p class="alert alert-success">' + response.message + '</p>');
                    window.location.href = response.redirectUrl;
                  
                }
            },
            error: function (r) {
                console.log($.parseJSON(r));
            },
            complete: function () {
                setTimeout(function () {
                    $('.status-success').fadeOut(6000);
                }, 400);
            }
        });


    });
});
