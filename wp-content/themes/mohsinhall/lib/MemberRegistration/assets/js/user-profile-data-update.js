jQuery(function ($) {
    'use strict';
    $('.user-profile-update').on('submit', function (e) {

        e.preventDefault();
        var updateProfileInfo = $(this).serializeArray();

        $.ajax({
            method: "POST",
            url: userProfileUpdateObj.ajax_url,
            contentType: "application/json", // tried without it too
            dataType: "json", // tried without it too

            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', userProfileUpdateObj.nonce);
            },
            data: {
                action : "userUpdateProfile",
                security: userProfileUpdateObj.nonce,
                userUpdateProfile : updateProfileInfo
            },
            success: function (r) {
                console.log(r);
                var response = $.parseJSON(r);
                if(response.Status === false){
                    $('.status-error').fadeIn(100).html('<p class="alert alert-danger">' + response.message + '</p>');
                }else{
                    $('.status-success').fadeIn(100).html('<p class="alert alert-success">' + response.message + '</p>');
                }
            },
            error: function (r) {
                console.log(r);

            },
            complete: function () {
                setTimeout(function () {
                    $('.status-success').fadeOut(100);
                }, 400);
            }
        });

    });
});

