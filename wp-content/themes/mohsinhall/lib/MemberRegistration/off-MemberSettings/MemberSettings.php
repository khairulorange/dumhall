<?php

namespace MemberLists;

require_once 'MemberLists/MemberListsTable.php';
/**
 * Member Settings
 */


class MemberSettings
{
    private $memberlistTable;

    /**
     * slug for settings page
     * @var string
     */
    protected $slug = 'MemberLists';
    public function __construct()
    {
        add_action('admin_menu', [$this, 'addOptionsPage'], 5);
        add_action('admin_menu', [$this, 'addsubmenuOptionsPage'], 5);
        add_action('admin_menu', [$this, 'screen_option']);

    }

    /**
     * add options page in admin dashboard
     */
    public function addOptionsPage()
    {
        add_menu_page(
            'Member Lists',
            'Member Lists',
            'manage_options',
            'MemberLists',
            '',
            'dashicons-admin-users',
            '25'
        );

    }

    /**
     * add options page in admin dashboard
     * @return void
     */

    public function addsubmenuOptionsPage()
    {
        add_submenu_page(
            'MemberLists',
            'Member Lists',
            'MemberLists',
            'manage_options',
            $this->slug,
            [$this, 'optionsPageContent']
        );

    }

    /**
     * add options page content
     * @return void
     */

    public function optionsPageContent()
    {
        ?>
        <div class="orange-settings">

            <h1><?php _e('Member Lists', 'orange') ?></h1>

            <div class="wrap">
                <div class="page-outer">
                    <div class="cf7-dbt-content">
                        <?php

                        $this->memberlistTable->prepare_items();
                        $this->memberlistTable->display();

                        ?>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }
    public function screen_option() {

        $option = 'per_page';
        $args   = [
            'label'   => 'users',
            'default' => 5,
            'option'  => 'users_per_page'
        ];

        add_screen_option( $option, $args );

        $this->memberlistTable = new MemberListsTable();
    }

}
new MemberSettings();