<?php get_header(); ?>
<main>
    <?php  get_template_part( 'templates/content', 'banner' ); ?>
    <section class="pay-70">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="error-404 text-center">
                        <h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'orange' ); ?></h1>
                        <div class="page-content">
                            <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'orange' ); ?></p>
                            <a class="btn btn-danger" href="<?php echo get_home_url(); ?>">Back to Homepage</a>
                        </div><!-- .page-content -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php get_footer(); ?>

