<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php wp_head(); ?>
</head>
<body>
<div class="single-post">
	<header>
		<div class="container">
			<div class="row align-items-center">
				<div class="col-auto">
					<img src="assets/img/logo-white.png" alt="">
				</div>
				<div class="col">
					<nav>
						<ul class="menu">
							<li>
								<a href="<?php echo $base_url;?>about.php">About</a>
							</li>
							<li>
								<a href="<?php echo $base_url;?>services.php">Services</a>
							</li>
							<li>
								<a href="<?php echo $base_url;?>portfolios.php">Portfolio</a>
							</li>
							<li>
								<a href="#">Testimonials</a>
							</li>
							<li>
								<a href="<?php echo $base_url;?>blog.php">Blog</a>
							</li>
							<li class="menu-btn">
								<a href="#">Contact</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>
</div>