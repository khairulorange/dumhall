<?php
/**
 * Template Name: Members Page
 */
get_header();
if ( ! is_front_page() ) :
	get_template_part( 'templates/banner', 'page' );
endif;
?>
<main>
    <?php  get_template_part( 'templates/content', 'banner' ); ?>
    <div class="searchFilter">
        <?php echo do_shortcode('[MemberSearch]'); ?>
    </div>
	<section class="register-member my-5">
		<div class="container">
           <?php
             get_template_part('templates/content', 'memberlist');
           ?>
        </div>
		</div>
	</section>
</main>
<?php get_footer(); ?>
