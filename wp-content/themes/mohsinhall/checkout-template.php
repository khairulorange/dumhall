<?php
/**
 * Template Name: Checkout Template
 */

?>

 <?php

            
class UserPayment{


   public function __construct()

   {
       $userMail = $_GET['usermail'];
       $eventId = $_GET['eid'];

       if(isset($userMail) && isset($eventId)){
           $this->requestSessionApi( $userMail, $eventId);
       }else{
           $this->requestSessionApi($userMail);
       }

   }

   //send request to ssl payment getway
   public function requestSessionApi($userMail, $eventId=''){

       global $wpdb;
       $userLogin = false;
       $option = get_option('payment_api_opt');
       $total_amount = 0;

       if(is_user_logged_in() && get_current_user_id() == $userMail){
           $userLogin = true;
       }

       // User check
       if($userLogin){
           $usersInfo = get_userdata(get_current_user_id());

       }else{
           $total_amount = (int)get_option( 'payment_api_opt' )['registarionFee'];
       }



       if(!$userLogin){
           $table_name = $wpdb->prefix . 'temporary_users';
           $target_user = $wpdb->get_results( "SELECT * FROM $table_name WHERE email = '$userMail' ORDER BY id DESC LIMIT 1" );
       }





       $registrationType = '';

       if(!$userLogin && !empty($target_user[0]->wanttojoin) ){
           $registrationType = "Membership & Reunion";
           $wantJoin = explode(",",$target_user[0]->wanttojoin);
           $tranPrefix = 'Both';
       }elseif (!$userLogin && empty($target_user[0]->wanttojoin)){
           $registrationType = "Membership Only";
           $tranPrefix = 'Membership';
       }else{
           $registrationType = "Reunion Only";
           $tranPrefix = 'Reunion';
       }

       if(!empty($wantJoin)){

           $args = array(
               'post_type' => 'events',
               'post_status' => 'publish',
               'posts_per_page' => -1,
           );
           // The Query
           $targetEvents = new WP_Query($args);

           $targetEventData =[];

           if ( $targetEvents->have_posts() ) {
               while ( $targetEvents->have_posts() ) : $targetEvents->the_post();

                   if (in_array(get_the_ID(), $wantJoin)){

                       $eventPrice = get_field('payable_amount');
                       $eventPrice = ($eventPrice == null) ?  0 : $eventPrice;
                       $targetEventData[get_the_ID()] = $eventPrice;
                   }
               endwhile;
           }

           wp_reset_postdata();

           foreach ($targetEventData as $key=>$value){
              $total_amount +=$value;
           }


       }elseif ($eventId){

           $args = array(
               'post_type' => 'events',
               'post_status' => 'publish',
               'p' => $eventId,
           );
           // The Query
           $targetEvents = new WP_Query($args);

           if ( $targetEvents->have_posts() ) {

               $targetEventData =[];

               while ( $targetEvents->have_posts() ) : $targetEvents->the_post();

                       $eventPrice = get_field('payable_amount');
                       $targetEventData[get_the_ID()] = $eventPrice;

               endwhile;

           }
           wp_reset_postdata();

           foreach ($targetEventData as $key=>$value){
               $total_amount +=$value;
           }

       }


       //echo '<pre>';
       //var_dump($registrationType);
       //echo '</pre>';




       //NEW V4 HOSTED API OF SSLCOMMERZ

       $post_data = array(
           'store_id'      => trim($option['store_id']),
           'store_passwd'  => trim($option['store_password']),
           'total_amount'  => $total_amount,
           'tran_id'       => $tranPrefix.uniqid(),
           'success_url'   => get_page_link($option['success_page']),
           'fail_url'      => get_page_link($option['fail_page']),
           'cancel_url'    => get_page_link($option['fail_page']),
           'ipn_url'       => get_page_link($option['ipn_page']),
           'cus_name'      => $target_user[0]->name ? $target_user[0]->name : $usersInfo->name,
           'cus_add1'      => $target_user[0]->presentAddress ? $target_user[0]->presentAddress : $usersInfo->presentAddress,
           'cus_country'   => "Bangladesh",
           'cus_state'     => "Dhaka",
           'cus_city'      => "Dhaka",
           'cus_postcode'  => "1200",
           'cus_phone'     => $target_user[0]->mobile ? $target_user[0]->mobile : $usersInfo->mobile,
           'cus_email'     => $target_user[0]->email ? $target_user[0]->email : $usersInfo->email,
           'ship_name'     => "Dhaka",
           'ship_add1'     => $target_user[0]->presentAddress ? $target_user[0]->presentAddress : $usersInfo->presentAddress,
           'ship_country'  => 'Bangladesh',
           'ship_state'    => "Dhaka",
           'delivery_tel'  => $target_user[0]->mobile ? $target_user[0]->mobile : $usersInfo->mobile,
           'ship_city'     => "Dhaka",
           'ship_postcode' =>  "1000",
           'currency'      => 'BDT',
           'product_category'  => 'Membership',
           'shipping_method'   => 'NO',
           'num_of_item'       => '1',
           'product_name'      => $registrationType,
           'product_profile'   => 'general'
       );


       $table_name = $wpdb->prefix . 'payment_history';
       $wpdb->insert(
           $table_name,
                array(
                    'time' => current_time( 'mysql' ),
                    'user_id' =>  $post_data['cus_email'],
                    'user_name' => $post_data['cus_name'],
                    'user_email' => $post_data['cus_email'],
                    'price' => $post_data['total_amount'],
                    'gateway' => 'sslcommerz',
                    'tran_id' => $post_data['tran_id'],
                    'event_id' => $eventId,
                    'status' => "Pending",
                    'mobile' => $post_data['cus_phone'],
                    'membership_type' => $registrationType,
                )
       );



       # REQUEST SEND TO SSLCOMMERZ
       $direct_api_url = "https://sandbox.sslcommerz.com/gwprocess/v3/api.php";

       $handle = curl_init();
       curl_setopt($handle, CURLOPT_URL, $direct_api_url );
       curl_setopt($handle, CURLOPT_TIMEOUT, 30);
       curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 30);
       curl_setopt($handle, CURLOPT_POST, 1 );
       curl_setopt($handle, CURLOPT_POSTFIELDS, $post_data);
       curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
       curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, FALSE); # KEEP IT FALSE IF YOU RUN FROM LOCAL PC


       $content = curl_exec($handle );

       $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

       if($code == 200 && !( curl_errno($handle))) {
           curl_close( $handle);
           $sslcommerzResponse = $content;
       } else {
           curl_close( $handle);
           echo "FAILED TO CONNECT WITH SSLCOMMERZ API";
           exit;
       }

       # PARSE THE JSON RESPONSE
       $sslcz = json_decode($sslcommerzResponse, true );


       if(isset($sslcz['GatewayPageURL']) && $sslcz['GatewayPageURL']!="" ) {
           # THERE ARE MANY WAYS TO REDIRECT - Javascript, Meta Tag or Php Header Redirect or Other
           # echo "<script>window.location.href = '". $sslcz['GatewayPageURL'] ."';</script>";
           echo "<meta http-equiv='refresh' content='0;url=".$sslcz['GatewayPageURL']."'>";
           $post_data['tran_id'];
           # header("Location: ". $sslcz['GatewayPageURL']);
           exit;
       } else {
           echo "JSON Data parsing error!";
       }
   }
   public function listenForSslcommerzResponse() {
       if ( (isset($_POST['val_id']) && $_POST['val_id'] !="") && ((isset($_POST['tran_id']) && $_POST['tran_id'] !="")) && $_SERVER['QUERY_STRING'] != 'sslcommerzeddipn')
       {
           do_action('listenForSslcommerzResponse');
       }
   }

    public function checkoutHtmlRender(){
        ob_start();
        ?>
        <div id="checkout-wrapper">

            <div class="container">
                <div class="row">
                    <input type="checkbox">
                    <div class="col-8">
                        <div class="row mt-3 ml-2">
                            <input type="checkbox" name="agreeTojoinEvent" value="agree" class="mr-2" id="agreeTojoinEvent"  style="height: 22px;width: 16px"> <label class="mb-0" for="termsAndCondition"> Upcomming Event Lists</label>
                        </div>
                        <div class="row d-none">
                            <div class="col-12">

                            </div>
                        </div>
                    </div>
                    <div class="col-4">

                        <h3>Payment History</h3>
                    </div>
                </div>
            </div>

        </div>



        <?php return ob_get_clean(); }


   
}



new UserPayment();




 ?>
    