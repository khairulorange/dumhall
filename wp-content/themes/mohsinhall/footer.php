
<footer>
	<div class="top-footer">
		<div class="container pab-20">
			<div class="row">
                <?php if ( is_active_sidebar( 'footer' ) ):
                    dynamic_sidebar( 'footer' );
                endif; ?>
			</div>
		</div>
	</div>
	<div class="copyright pay-20">
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<p>Copyright &copy; <?php echo date('Y');?>, Mohsinhall Alumni | All rights reserved</p>
				</div>
			</div>
		</div>
	</div>
</footer>

<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>-->
<?php wp_footer(); ?>
</body>
</html>