<?php 
/*
 Template Name: ajax test 
*/
get_header(); ?>
<main>
   <div class="container">
       <div class="row">
           <div class="col-3 m-auto">
               <form class="form-group" action="<?php echo home_url("/");?>" method="post">
                   <label for="info"></label>
                   <input class="form-control" type="text" id="info">
                   <br>
				  <div class="select_user_wrapper">
                                            <select  class="form-control" id="dumhall-user-list">
                                                <option value="">-- <?php _e( 'Select User', 'dumhall' ); ?> --</option>
                                                <?php
                                                $args = array(
                                                    'role' => 'subscriber',
                                                    'orderby' => 'user_email',
                                                    'order' => 'ASC'
                                                );
                                                $users = get_users( $args );

                                                foreach ( $users as $user ) {
                                                    if ( $user->first_name && $user->last_name ) {
                                                        $user_fullname = ' (' . $user->first_name . ' ' . $user->last_name . ')';
                                                    } else {
                                                        $user_fullname = '';
                                                    }
                                                    echo '<option value="' . esc_html( $user->user_email ) . '">' . esc_html( $user->user_email ) . esc_html( $user_fullname) . '</option>';
                                                };
                                                ?>
                                            </select>
                                            <div class="select-all">
                                                <input onclick="displayResult();" type="button" id="select_all" name="select_all" value="Select All user">

                                            </div>
                                            <div class="select-department">
                                                <select   class="form-control" id="select-department_list">
                                                    <option value="">-- <?php _e( 'Select Department', 'dumhall' ); ?> --</option>
                                                    <?php

                                                    $users = get_users( array( 'fields' => array( 'ID' ) ) );
                                                    foreach($users as $user_id){
                                                        $data = get_user_meta ( $user_id->ID);
                                                        if(!empty($data)){
                                                            echo '<option value="' . esc_html( $data['department'][0]) . '">' . esc_html( $data['department'][0] ) . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="select-session">
                                                <select    class="form-control" id="select-session_list">
                                                    <option value="">-- <?php _e( 'Select Session', 'dumhall' ); ?> --</option>
                                                    <?php

                                                    $users = get_users( array( 'fields' => array( 'ID' ) ) );
                                                    foreach($users as $user_id){
                                                        $data = get_user_meta ( $user_id->ID);
                                                        if(isset($data)){
                                                            echo '<option value="' . esc_html( $data['session'][0]) . '">' . esc_html( $data['session'][0] ) . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="search-selected-data">
                                                <input type="submit" value="Search" id="searchMember">
                                            </div>
                                        </div>

                   <input class="btn btn-primary" type="submit" id="submit" value="submit">
               </form>
           </div>
       </div>
   </div>
	<section class="bg-light pay-70">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-9">
					<div class="blog-list blog-archive">
                        <?php
                        $args = new WP_Query(
                            [
                                'post_type' => 'post',
                                'posts_per_page'=> -1,
                                'order'         => 'DSC',
                            ]
                        );

                        if ($args->have_posts()): while ($args->have_posts()) :
                            $args->the_post();
                        get_template_part('templates/content', 'post-full-width');
                        endwhile;  wp_reset_query(); endif;
                        ?>
					</div>
				</div>
			</div>

		</div>
	</section>
</main>
<?php get_footer(); ?>