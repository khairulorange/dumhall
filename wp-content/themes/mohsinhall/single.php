<?php get_header(); ?>
<main>
	<section class="bg-light pay-70">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-8">
                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>
					<div class="blog-single">
						<div class="blog-header">
							<div class="blog-title">
								<h2><?php the_title(); ?></h2>
							</div>
							<div class="blog-meta">
								<p class="meta">
								<span><i class="far fa-user"></i> by <?php the_author(); ?></span>
								<span><i class="far fa-clock"></i> at <?php echo get_the_time('F j, Y' ); ?></span>
								</p>
							</div>
						</div>
                        <?php if(has_post_thumbnail()): ?>
						<div class="blog-thumbnail">
                            <?php the_post_thumbnail('737x449',array('alt'=> 'blog-img')); ?>
						</div>
                        <?php endif; ?>
						<div class="blog-body">
                            <?php the_content(); ?>
						</div>

						<div class="blog-tags">
							<ul>
                                <?php
                                if(get_the_tag_list()) {
                                    echo get_the_tag_list('<ul><li>','</li><li>','</li></ul>');
                                }
                                ?>
							</ul>
						</div>
					</div>
                    <?php endwhile;  wp_reset_query(); endif; ?>
					
						<?php

							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						?>
	
					<div class="blog-back-link pat-50 text-center d-none">
						<a href="<?php echo $base_url; ?>blog.php" class="primary-btn">Back to blog</a>
					</div>
				</div>
				<div class="col-4">
					<div class="blog-sidebar">
                        <?php get_sidebar(); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>

