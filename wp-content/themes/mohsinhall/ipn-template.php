<?php
/**
 * Template Name: ipn
 */



?>
       <?php


    $options = get_option( 'payment_api_opt' );


	$val_id=urlencode($_POST['val_id']);
    $store_id= urlencode( $options['store_id']);
    $store_passwd= urlencode($options['store_password']);
    $requested_url = ("https://sandbox.sslcommerz.com/validator/api/validationserverAPI.php?val_id=".$val_id."&store_id=".$store_id."&store_passwd=".$store_passwd."&v=1&format=json");

    $handle = curl_init();
    curl_setopt($handle, CURLOPT_URL, $requested_url);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false); # IF YOU RUN FROM LOCAL PC
    curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false); # IF YOU RUN FROM LOCAL PC

    $result = curl_exec($handle);

    $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

    if($code == 200 && !( curl_errno($handle)))
    {

        # TO CONVERT AS ARRAY
        # $result = json_decode($result, true);
        # $status = $result['status'];

        # TO CONVERT AS OBJECT
        $result = json_decode($result);


        # TRANSACTION INFO
        $status = $result->status;
        $tran_date = $result->tran_date;
        $tran_id = $result->tran_id;
        $val_id = $result->val_id;
        $amount = $result->amount;
        $store_amount = $result->store_amount;
        $bank_tran_id = $result->bank_tran_id;
        $card_type = $result->card_type;


        global $wpdb;
        $table_name = $wpdb->prefix . 'payment_history';

        $tranCheck = $wpdb->get_results("SELECT * FROM $table_name WHERE tran_id = '$tran_id' LIMIT 1 ");

        if($tranCheck[0] && $tranCheck[0]->status =='Pending' ){

            $wpdb->update(
                $table_name ,
                array(
                    'status' => 'processing',	// string
                ),
                array( 'tran_id' => $tran_id ) // where
            );

        }

    }
    else {
        echo "Failed to connect with SSLCOMMERZ";
    }


	?>
