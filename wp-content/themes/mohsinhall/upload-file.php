<?php
/**
 * Template Name: File Upload
 */
get_header();
if ( ! is_front_page() ) :
	get_template_part( 'templates/banner', 'page' );
endif;
?>
<main>
	<section class="register-member my-5">
		<div class="container">
			<div class="row">
				<div class="col-12 my-5">
					<div class="photo-upload">
						<form class="ibenic_upload_form" enctype="multipart/form-data">

							<div class="user-photo-upload">
								<div class="user-photo-upload-message"></div>
								<div class="photo-upload-wrapper">
									<input type="file" class="user-photo-upload-input" style="opacity:0;" />
									<p class="user-photo-upload-text"><?php _e( 'Upload your Photo', 'dumhall' ); ?></p>
								</div>
								<div id="user_photo_upload_preview" class="file-upload photo-preview user_photo_upload_preview" style="display:none;">
									<div class="user-photo-preview mb-2"></div>
									<button data-fileurl="" class="user-photo-delete">
										<i class="fa fa-window-close" aria-hidden="true"></i>
									</button>
								</div>
							</form>
						</div>


						<style>
							/*.user-photo-upload{*/
								/*position: relative;*/
							/*}*/
							/*.photo-upload-wrapper {*/
								/*display:block;*/
								/*width:150px;*/
								/*height:150px;*/
								/*border-radius: 3px;*/
								/*background-color:rgba(0,0,0,0.3);*/
								/*font-size: 14px;*/
								/*color:white;*/
								/*text-align: center;*/
								/*line-height: 150px;*/
							/*}*/
							/*.photo-upload-wrapper input {*/
								/*opacity: 0;*/
								/*position: absolute;*/
								/*width: 100%;*/
								/*height: 100%;*/
								/*display: block;*/
								/*cursor: pointer;*/
							/*}*/

							/*.photo-preview img{*/
								/*width: 100%;*/
								/*height: 100%;*/
								/*object-fit: cover;*/
							/*}*/

							/*.photo-preview .user-photo-preview {*/
								/*max-width: 150px;*/
								/*max-height: 150px;*/
								/*position: absolute;*/
								/*width: 100%;*/
								/*height: 100%;*/
								/*overflow: hidden;*/
								/*top: 0;*/
								/*left: 0;*/
							/*}*/

							/*.user-photo-delete {*/
								/*position: absolute;*/
								/*width: 40px;*/
								/*left: 113px;*/
								/*padding: 0.5em;*/
								/*text-align: center;*/
								/*color: white;*/
								/*background-color: #4242429e;*/
								/*border-radius: 50% !important;*/
								/*border: none;*/
								/*top: -16px;*/
							/*}*/
							/*.user-photo-delete:focus{*/
								/*outline: none;*/
							/*}*/
						</style>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>
