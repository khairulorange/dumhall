<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 3/10/2019
 * Time: 11:26 AM
 */
get_header();

if ( is_front_page() ) :
	get_template_part( 'templates/banner', 'page' );
endif;

$depart = new WP_Query([
    'post_type' => 'departments',
    'post_status'=> 'publish',
    'posts_per_page' => -1
]);
?>
<div class="departments-archive">
    <?php if ($depart->have_posts()): while ($depart->have_posts()) : $depart->the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; else: __( 'No Department found '); endif; ?>
</div>
<?php get_footer();?>