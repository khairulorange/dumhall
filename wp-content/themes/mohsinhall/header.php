<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php

	global $opt, $settingOptions;
	$settingOptions = $opt->ThemeOptionData();
?>
<header>
	<div class="header-top">
		<div class="container">
			<div class="row align-items-center">
				<div class="col">
					<div class="header_top_info">
						<?php
						if(!empty($settingOptions->email)):
						 ?>
						<span class="info_email"><strong>Email:</strong> <?php echo esc_attr($settingOptions->email);?></span>
						<?php endif;?>
						<?php
						if(!empty($settingOptions->phone)):
						?>
						<span class="info_hotline"><strong>Hotline:</strong> <?php echo esc_attr($settingOptions->phone);?></span>
						<?php endif;?>
					</div>
				</div>
				<div class="col-auto ml-auto">
					<div class="user-nav">
						<ul id="menu-top" class="menu">
							<?php if(is_user_logged_in()): ?>
							<li><a href="<?php echo home_url().'/member-profile'; ?>">Profile Page</a></li>
							<li><a href="<?php echo wp_logout_url( home_url().'/login' ); ?>">Logout</a></li>
							<?php else: ?>
							<li><a href="<?php echo home_url().'/login'; ?>">Login</a></li>
							<?php endif; ?>
							<?php if(!is_user_logged_in()): ?>
							<li><a href="<?php echo home_url().'/sign-up'; ?>">Signup</a></li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="main-header">
		<div class="container">
			<div class="row align-items-center">
				<?php if(!empty($settingOptions->logo_img)): ?>
					<div class="col-auto">
						<a href="<?php bloginfo('url'); ?>">
							<img src="<?php echo $settingOptions->logo_img; ?>" alt="orangetoolz">
						</a>
					</div>
				<?php endif; ?>
				<div class="col">
					<span class="mobile-nav-toggle float-right d-block d-lg-none">
                        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#mobile_nav">
                        <span class="menu-toggle-bar">
                          <span></span>
                          <span></span>
                          <span></span>
                          <span></span>
                        </span>
                        </button>
                    </span>
					<div class="main-menu d-none d-lg-block">
						<nav>
							<?php
							if ( has_nav_menu( 'primary_menu' ) ) :
								wp_nav_menu( array(
									'menu'              => __( 'Primary Nav', 'orange'),
									'container'       => false,
									'theme_location'    => 'primary_menu',
									'depth'             => 4,
									'menu_class'        => 'menu'
								));
							endif;
							?>
						</nav>
					</div>

				</div>
			</div>
		</div>
	</div>
</header>

<div class="header-mobile collapse body-bg d-lg-none"  id="mobile_nav">
	<div class="container">
		<div class="row">
			<div class="col-12 p-0">
				<div class="mobile-content">
				<?php
					if ( has_nav_menu( 'primary_menu' ) ) :
						wp_nav_menu( array(
							'menu'              => __( 'Primary Menu', 'orange'),
							'container'       => false,
							'theme_location'    => 'primary_menu',
							'depth'             => 4,
							'menu_class' => 'list-unstyled pt-3 pb-3 mobile-nav'
						));
					endif;
				?>
				</div>

			</div>

		</div>

	</div>
</div>



