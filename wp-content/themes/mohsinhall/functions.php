<?php

//$usersInfo = get_userdata(346);
//
//echo '<pre>';
//var_dump($usersInfo);
//echo '</pre>';

require_once(get_template_directory() . '/lib/inc/class-paginations.php');
require_once(get_template_directory() . '/lib/inc/photoUploadprocess.php');
require_once(get_template_directory() . '/lib/inc/certificateUploadprocess.php');
require_once(get_template_directory() . '/lib/shortcodes.php');
require_once(get_template_directory() . '/includes/aqua_resizer.php');

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page();

}
if (!defined('DUMHALL_THEME_VERSION')) define('DUMHALL_THEME_VERSION', '1.0');
if (!class_exists('DumhallThemeSetup'))
{
    class DumhallThemeSetup {

        public $pagination;
        public $ThemeOptionInfo;
        public $memberSearchShortcode;

        public function __construct() {
            add_action('after_setup_theme', array($this, 'DUMhallAfterThemeSetup'));
            add_action('wp_enqueue_scripts', array( $this, 'DUMhalleEnqueueScripts' ));
            add_action('widgets_init', array($this, 'DUMhallWidgetsInitSetup'));
			add_action('after_setup_theme', array($this,'removeAdminBar'));
			//create payment history table callback
			add_action('after_setup_theme', array($this,'createPaymentTable'));
			add_filter('user_contactmethods', array($this, 'updateContactMethods'),10,1);
            $this->pagination = new Pagination();
            $this->memberSearchShortcode = new Shortcodes();
            new photoUploadprocess();
            new certificateUploadprocess();
			add_action( 'show_user_profile', array($this,'extraUserProfileFields' ));
			add_action( 'edit_user_profile', array($this,'extraUserProfileFields' ));
			add_action( "user_new_form", array($this, "extraUserProfileFields" ));


			//add_action('wp_ajax_UploadUserPhotos', array($this, 'UploadUserPhotos' ));
			///add_action('wp_ajax_nopriv_UploadUserPhotos', array($this, 'UploadUserPhotos' ));

			add_action('init', [ $this, 'init']);
		}

		public function init()
		{
			$this->joinedMemberPage();
			$this->redirectFromDashboard();
			$this->registerFieldConnections();
		}

        public function DUMhalleEnqueueScripts() {




            /*
            * Enqueue css Files
            */
            wp_enqueue_style('fontawesome-all', get_template_directory_uri() . '/assets/vendor/fontawesome/css/all.min.css', array() , DUMHALL_THEME_VERSION);
            wp_enqueue_style('bootstrap-min', get_template_directory_uri() . '/assets/vendor/bootstrap/css/bootstrap.min.css', array() , DUMHALL_THEME_VERSION);
            wp_enqueue_style('slick', get_template_directory_uri() . '/assets/vendor/slick/slick.css', array() , DUMHALL_THEME_VERSION);
            wp_enqueue_style('slick-theme', get_template_directory_uri() . '/assets/vendor/slick/slick-theme.css', array() , DUMHALL_THEME_VERSION);
            wp_enqueue_style('main', get_template_directory_uri() . '/assets/dist/css/main.css', array() , DUMHALL_THEME_VERSION);
            wp_enqueue_style('ui-css', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', array() , DUMHALL_THEME_VERSION);
            wp_enqueue_style( 'stylesheet', get_stylesheet_uri() );

            /*
             * Enqueue jQuery Files
             */

            wp_enqueue_script('jQuery');
            wp_enqueue_script( 'popper-min','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array( 'jquery' ),'',true );
            wp_enqueue_script('bootstrap-min', get_template_directory_uri() . '/assets/vendor/bootstrap/js/bootstrap.min.js', array(
                'jquery'
            ) , DUMHALL_THEME_VERSION, true);
            wp_enqueue_script('particles', get_template_directory_uri() . '/assets/vendor/particles/particles.js', array(
                'jquery'
            ) , DUMHALL_THEME_VERSION, true);

            wp_enqueue_script('slick-min', get_template_directory_uri() . '/assets/vendor/slick/slick.min.js', array(
                'jquery'
            ) , DUMHALL_THEME_VERSION, true);

            wp_enqueue_script('dotdotdot', get_template_directory_uri() . '/assets/vendor/dotdotdot/dist/jquery.dotdotdot.js', array(
                'jquery'
            ) , DUMHALL_THEME_VERSION, true);
            wp_enqueue_script('site', get_template_directory_uri() . '/assets/dist/js/site.js', array(
                'jquery'
            ) , DUMHALL_THEME_VERSION, true);
            wp_enqueue_script('jquery-ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', array(
                'jquery'
            ) , DUMHALL_THEME_VERSION, true);

            wp_enqueue_script('photo-upload', get_template_directory_uri() . '/assets/dist/js/photo-upload.js', array(
                'jquery'
            ) , DUMHALL_THEME_VERSION, true);


			wp_localize_script( 'photo-upload', 'uploadImages',
				array(
						'ajax_url' => admin_url( 'admin-ajax.php' )

				)
			);

            wp_enqueue_script('certificate-upload', get_template_directory_uri() . '/assets/dist/js/certificate-upload.js', array(
                'jquery'
            ) , DUMHALL_THEME_VERSION, true);

			wp_localize_script( 'certificate-upload', 'uploadCertificateImage',
				array(
						'ajax_url' => admin_url( 'admin-ajax.php' )
				)
			);

			wp_enqueue_script('eventJoinMetaBox', get_template_directory_uri() . '/assets/build/scripts/event-join-metabox.js', array(
				'jquery'
			) , DUMHALL_THEME_VERSION, true);

			wp_localize_script('eventJoinMetaBox', 'eventJoinMetaBoxObj', array(
				'ajax_url' => admin_url( 'admin-ajax.php' ),
				'nonce'    => wp_create_nonce('event-join-meta-nonce')
			));

        }

        public function DUMhallAfterThemeSetup() {
            require_once(get_template_directory() . '/lib/CustomPostType.php');
            require_once(get_template_directory() . '/lib/CustomMetaBox.php');
            require_once(get_template_directory() . '/lib/theme-options/themeOptions.php');
			require_once(get_template_directory() . '/lib/MemberRegistration/MemberRegistration.php');
			require_once(get_template_directory() . '/lib/EventTables/ListEvents.php');
			require_once(get_template_directory() . '/lib/EventTables/EventDetails.php');

            /*
             * load text domain
             */
            load_theme_textdomain('dumhall', get_template_directory() . '/languages');

            /*
             * theme supports
             */
            add_theme_support( 'post-thumbnails', array( 'post','page', 'members','galleries', 'events') );
            add_theme_support('title-tag');
            add_image_size( 'testimonial-thumb',  99, 99, true );
            add_image_size( '737x449',  737, 449, true );
            add_image_size( '118x98',  118, 98, true );
            add_image_size( '270x170',  270, 170, true );

            /**
            * Beaver builder support
            **/
            add_theme_support('fl-theme-builder-headers');
            add_theme_support('fl-theme-builder-footers');
            add_theme_support('fl-theme-builder-parts');

            /*
            * register nav
            */
            register_nav_menus(array(
                'primary_menu' => __('Primary Menu', 'dumhall'),
                'top_menu' => __('Top menu', 'dumhall'),
                'company_links' => __('Company Link Menu', 'dumhall'),
                'useful_links' => __('Useful Link Menu', 'dumhall')
            ));
        }

        /*
        * DumhallWidgetsInitSetup
        * @sideber Register
        * @return mixed
        */

        public function DUMhallWidgetsInitSetup() {
            if ( function_exists('register_sidebar') )
                register_sidebar( array(
                    'name' => __( 'Page Sidebar', 'dumhall' ),
                    'id' => 'page-sidebar',
                    'description' => __( 'Widgets in this area will be shown on page sidebar area.', 'dumhall' ),
                    'before_widget' => '<div class="page-item-sidebar widget mb-3 %2$s">',
                    'after_widget'  => '</div>',
                    'before_title'  => '<h4 class="page-sidebar-title widget-title">',
                    'after_title'   => '</h4>',
                ) );
                register_sidebar( array(
					'name' => __( 'Footer', 'dumhall' ),
					'id' => 'footer',
					'description' => __( 'Widgets in this area will be shown on footer sidebar area.', 'dumhall' ),
					'before_widget' => '<div class="col-md-3"><div class="page-item-sidebar widget mb-3 %2$s">',
					'after_widget'  => '</div></div>',
					'before_title'  => '<h4 class="page-sidebar-title widget-title">',
					'after_title'   => '</h4>',
                    ) );

        }

		/*
		* ThemeOptionData
		* @Theme Option Settings
		* @return Object
		*/

		public function ThemeOptionData(){

            if( class_exists('General\Settings')) {

                $this->orangeGen = General\Settings::getSettings();
                $this->ThemeOptionInfo = (object) array_merge((array) $this->orangeGen);
            }
            return $this->ThemeOptionInfo;
        }

        /*
         * personal Information users
         */

		function updateContactMethods( $contactmethods ) {
			$contactmethods['name'] = 'Full Name';
			$contactmethods['nickName'] = 'Nick Name';
			//$contactmethods['formNumer'] = 'Form Number';
			$contactmethods['qualification'] = 'Qualification';
			$contactmethods['department'] = 'Department';
			$contactmethods['session'] = 'session';
			$contactmethods['hostelStayPeriod'] = 'Hostel Stay Period';
			$contactmethods['lastHostelLivingRoom'] = 'Last Hostel Living Room';
			$contactmethods['dateOfBirth'] = 'Date of Birth';
			$contactmethods['bloodGroup'] = 'Blood Group';
			$contactmethods['nationalIdNumber'] = 'National Id Number';
			$contactmethods['occupationDesignation'] = 'Occupation Designation';
			$contactmethods['fatherName'] = 'Father\'s Name';
			$contactmethods['motherName'] = 'Mother\'s Name';
			$contactmethods['spouseName'] = 'Spouse\'s Name';
			$contactmethods['childrenNumber'] = 'Children Number';
			$contactmethods['son'] = 'Son';
			$contactmethods['daughter'] = 'Daughter';
			$contactmethods['phoneOfficeHome'] = 'Phone Office / Home';
			$contactmethods['presentAddress'] = 'Present Address';
			$contactmethods['permanentAddress'] = 'Permanent Address';
			$contactmethods['mobile'] = 'Mobile Number';
			$contactmethods['email'] = 'Email Address';
			return $contactmethods;
		}
        
        /*
         * Removed admin-bar
         * exclude administrator user
         * @return bools
         */
         
		public function removeAdminBar()
		{
			if (!current_user_can('administrator') && !is_admin())
			{
				show_admin_bar(false);
			}
		}

        /*
         * Extra fields add 
         * Users profile page
         * @return mixed 
         */
         
		public function extraUserProfileFields($user)
		{

			if (in_array('subscriber', (array)$user->roles)) { ?>
			<table class="form-table">
			    <tbody>
			        <tr class="user-profile-photo-wrap">
                    	<th>
                    	    <label>Member Photo </label>
                    	</th>
                    	<td>
                    	    <?php $userPhoto = get_user_meta($user->ID, 'userPhoto', true); ?>
                    	    <?php if(!empty($userPhoto)): ?>
                    	    <img src="<?php echo $userPhoto; ?>" width="300" height="350">
                    	    <?php endif; ?>
                    	 </td>
                    </tr>
                    <tr class="user-certificate-wrap">
                    	<th>
                    	    <label>Certificate Photo </label>
                    	</th>
                    	<td>
                    	    <?php $certificate = get_user_meta($user->ID, 'certificate', true); ?>
                    	    <?php if(!empty($certificate)): ?>
                    	    <img src="<?php echo $certificate; ?>" width="500" height="450">
                    	    <?php endif; ?>
                    	 </td>
                    </tr>
			    </tbody>
			</table>
			
            
			
			    
			    <?php
				$status = get_user_meta($user->ID, 'status', true);

				$approval = '';
				$approval .= '<div class="approve-button">';
				if ($status === '0') {

					$approval .= '<a href="#" class="user-approve-status button button-primary" 
			 data-action ="approve" data-id ="' . $user->ID . '">Approve Member</a>';
				}
				$approval .= '</div>';
				echo $approval;

			}
		}
        
        /*
         * joinedMemberPage Option page 
         * add join member menu
         * @return mixed 
         */
         
		public function joinedMemberPage()
		{
			add_submenu_page(
				'edit.php?post_type=events',
				'Joined Members',
				'Joined Members',
				'manage_options',
				'joined_members',
				[$this,'joinedMemberPageContent']
			);
		}
		
		/*
         * joinedMemberPageContent Option page 
         * Joined Member page content
         * @return mixed 
         */
         
		public function joinedMemberPageContent()
		{
			if (isset($_GET['event_id'])) {
				$this->eventDetailsTable($_GET['event_id']);
				return;
			}
			$this->allEventsTable();


		}
		
		/*
         * allEventsTable list
         * Event list and joined member count
         * @return mixed 
         */
         
		public function allEventsTable()
		{
			$listEvents = new \DUMHALL\LIB\EventTables\ListEvents();
			$listEvents->prepare_items();
			?>
			<div class="wrap">
				<div class="page-outer">
					<h2>Joined Members in Event</h2>
					<div class="event-list-content">
						<?php $listEvents->display();?>
					</div>
				</div>
			</div>
			<?php
		}
		
		/*
         * eventDetailsTable list
         * join member details
         * @return mixed 
         */
         
		public function eventDetailsTable($id)
		{
			$event = get_post($id);
			$listEventDetails = new \DUMHALL\LIB\EventTables\EventDetails($event);
			$listEventDetails->prepare_items();
			?>
			<div class="wrap">
				<div class="page-outer">
					<h2>Joined Members in <?php echo $event->post_title;?></h2>
					<div class="cf7-dbt-content">
						<?php $listEventDetails->display();?>
					</div>
				</div>
			</div>
			<?php

		}

		/*
		 * redirectFromDashboard
		 */
		public function redirectFromDashboard()
		{
			$user = wp_get_current_user();
			if(is_admin()){
				if($user->roles[0] == 'subscriber'){
					wp_redirect(home_url().'/member-profile');
				}
			}
		}

		public function registerFieldConnections()
		{
			if(class_exists('FLPageData')){
				\FLPageData::add_post_property('custom_gallery_images', array(
					'label'   => 'Custom Gallery Images',
					'group'   => 'general',
					'type'    => 'multiple-photos',
					'getter'  => [ $this, 'getGalleryImages' ]
				));
			}
		}

		public function getGalleryImages()
		{
			$images = get_posts([
				'post_type' => 'galleries',
				'posts_per_page' => 8
			]);
			$galleryItems = [];
			foreach ($images as $image){
				$galleryItems[] = get_post_thumbnail_id($image);
			}
			return $galleryItems;
		}
        public function createPaymentTable(){
            global $wpdb;
            $charset_collate = $wpdb->get_charset_collate();
            $sql = "CREATE TABLE " . $wpdb->prefix . "payment_history (
			id mediumint(20) NOT NULL AUTO_INCREMENT,
			time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
			user_id varchar(30)  NULL,
			user_name varchar(30)  NULL,
			user_email varchar(30)  NULL,
			price int(11)  NULL,
			gateway varchar(30)  NULL,
			tran_id varchar(50)  NULL,
			event_id varchar(50)  NULL,
			status varchar(20)  NULL,
			mobile varchar(25)  NULL,
			membership_type varchar(30)  NULL,
			UNIQUE KEY id (id)
		) $charset_collate;";

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);


            $charset_collate = $wpdb->get_charset_collate();
            $sql = "CREATE TABLE " . $wpdb->prefix . "temporary_users (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
			name varchar(35) NOT NULL,
			email varchar(35) NOT NULL,
			qualification varchar(25) NOT NULL,
			department varchar(30) NOT NULL,
			usersession varchar(25) NOT NULL,
			hostelStayPeriodFrom varchar(20) NULL,
			hostelStayPeriodTo varchar(20) NULL,
			lastHostelLivingRoom varchar(20) NULL,
			dateOfBirth varchar(20) NULL,
			bloodGroup varchar(15) NULL,
			nationalIdNumber varchar(20) NULL,
			occupationDesignation varchar(20) NOT NULL,
			fatherName varchar(25) NULL,
			motherName varchar(25) NULL,
			spouseName varchar(25) NULL,
			childrenNumber varchar(5) NULL,
			son varchar(5) NULL,
			daughter varchar(5) NULL,
			phoneOfficeHome varchar(20) NULL,
			presentAddress varchar(80) NOT NULL,
			permanentAddress varchar(80) NULL,
			mobile varchar(20) NOT NULL,
			wanttojoin varchar(30) NULL,
			userPhoto varchar(120)  NULL,
			certificate varchar(120)  NULL,
			UNIQUE KEY id (id)
		) $charset_collate;";

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
        }

    }

}
if (class_exists('DumhallThemeSetup'))
{
    $opt = new DumhallThemeSetup();
}
