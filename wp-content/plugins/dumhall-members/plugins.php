<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Plugin Name: Mohsin Hall Plugin toolkit
 * Plugin URI: https://orangetoolz.com/
 * Author: OrangeToolz
 * Text Domain: dumhall-toolkit
 * Version: 1.0.0
 */

define('DUMHALL_VERSION', '2.0.0');
define('DUMHALL_PATH', __DIR__);
define('DUMHALL_URL', plugins_url(basename(DUMHALL_PATH)));
/**
 * init plugin
 */
function dum_hall_init()
{
	require_once DUMHALL_PATH . '/member-settings/members-settings.php';
	$plugin = new DUMHALL\DUMembers();
}

add_action('plugins_loaded', 'dum_hall_init');