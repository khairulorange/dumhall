(function ($) {
	'use-strict';

	$(document).ready(function(){
		var OBJ = {
			department : 0,
			session    : 0
		}
		$('#select-department_list').on('change', function() {
			OBJ.department = $(this).find(":selected").val() ;
			callFromDB();
		});

		$('#select-session_list').on('change', function() {
			OBJ.session = $(this).find(":selected").val() ;
			callFromDB();
		});
		function callFromDB() {
			if(OBJ.department != 0 && OBJ.session != 0){
				$.post(ajax_object.ajax_url,{
					'action':'member_search_action',
					'department': OBJ.department,
					'session': OBJ.session,
				},function(data){
					if(data == 0){
						alert('No Data Found');
					}else{
						//console.log(data);
						$('#dumhall-user-list').html(data);
					}



				})

			}
		}
	});
})(jQuery);
// arguments: reference to select list, callback function (optional)
function getSelectedOptions(sel, fn) {
	var opts = [], opt;

	// loop through options in select list
	for (var i=0, len=sel.options.length; i<len; i++) {
		opt = sel.options[i];

		// check if selected
		if ( opt.selected ) {
			// add to array of option elements to return from this function
			opts.push(opt);

			// invoke optional callback function if provided
			if (fn) {
				fn(opt);
			}
		}
	}

	// return array containing references to selected option elements
	return opts;
}
// example callback function (selected options passed one by one)
function callback(opt) {
	// display in textarea for this example
	var display = document.getElementById('dumhall-recipient-emails');
	display.innerHTML += opt.value + ', ';
}
// anonymous function onchange for select list with id demoSel
document.getElementById('dumhall-user-list').onchange = function(e) {
	// get reference to display textarea
	var display = document.getElementById('dumhall-recipient-emails');
	display.innerHTML = ''; // reset

	// callback fn handles selected options
	getSelectedOptions(this, callback);

	// remove ', ' at end of string
	var str = display.innerHTML.slice(0, -2);
	display.innerHTML = str;
};

document.getElementById('select_all').onclick = function(e) {

	var display = document.getElementById('dumhall-recipient-emails');
	var x = document.getElementById("dumhall-user-list");
	var txt = "";
	var i;
	for (i = 0; i < x.length; i++) {
		txt = txt + "\n" + x.options[i].value + ',';
	}
	txt = txt.slice(0, txt.length-1);
	display.value = txt;
	return false; // don't return online form
};