<?php
namespace PAYMENTS;

class PaymentOptions{
    public function __construct()
    {
        $this->settingsInit();
    }
    public function settingsInit(){
        $this->optionPaymentPageContent();
    }
    public function optionPaymentPageContent()
    {

        //must check that the user has the required capability
        if (!current_user_can('manage_options'))
        {
            wp_die( __('You do not have sufficient permissions to access this page.') );
        }

//        $optionCombine = [];
//
//         $optionData =  array(
//                 'du_sslcommerz_testmode'=> $optionCombine['du_sslcommerz_testmode'],
//                 'du_sslcommerz_store_id'=> $optionCombine['du_sslcommerz_store_id'],
//         );

//        foreach ($_POST['formData'] as $formField) {
//            $fieldsKeyName[] = $formField['name'];
//            $fieldsValue[] = $formField['value'];
//        }

       // $data_field_name = 'mt_favorite_color';

        // Read in existing option value from database
        //$opt_val = get_option( $opt_name );

        // See if the user has posted us some information
        // If they did, this hidden field will be set to 'Y'
//        if( !$opt_val) {
//            // Read their posted value
//            $opt_val = $opt_name;
//
//            // Save the posted value in the database
//            update_option( $opt_name, $opt_val );
//
//            // Put a "settings saved" message on the screen
//
//            ?>
<!--            <div class="updated"><p><strong>--><?php //_e('settings saved.', 'dumhall' ); ?><!--</strong></p></div>-->
<!--            --><?php
//
//        }


        // Now display the settings editing screen



        if( isset( $_POST[ 'submit_payment_form' ] ) ) {

            $fieldData=[];

            $fieldData[$_POST['testmode']] = $_POST['du_sslcommerz_testmode'];
            $fieldData[$_POST['storeID']] = $_POST['du_sslcommerz_store_id'];
            $fieldData[$_POST['StorePass']] = $_POST['du_sslcommerz_store_password'];
            $fieldData[$_POST['']] = $_POST['du_sslcommerz_redirect_page_id'];
            $fieldData[$_POST['du_sslcommerz_fail_page_id']] = $_POST['du_sslcommerz_fail_page_id'];
            $fieldData[$_POST['du_sslcommerz_ipnurl']] = $_POST['du_sslcommerz_ipnurl'];

           // update_option('payment_info', $fieldData);

            foreach ($fieldData as $key=> $value){
                echo  $key .' '. $value .'<br>';
            }

        }


       //$payment_data =  get_option('payment_info');

        if($payment_data){
//            //var_dump($payment_data);
//            foreach ($payment_data as $key=>$value){
//                echo $key .' '. $value .'<br>';
//            }
        }


        echo '<div class="wrap">';

        // header

        echo "<h2>" . __( 'Payment Settings', 'dumhall' ) . "</h2>";

        // settings form

        ?>

        <form name="payment-form" method="POST" action="">
            <table class="form-table">
                <tbody>

                <tr valign="top">
                    <th scope="row" class="titledesc">
                        <label for="du_sslcommerz_testmode">Testmode </label>
                    </th>
                    <td class="forminp">
                        <fieldset>
                            <legend class="screen-reader-text"><span>Testmode</span></legend>
                            <label for="du_sslcommerz_testmode">
                                <input class="" type="checkbox" name="du_sslcommerz_testmode" id="du_sslcommerz_testmode" style="" value="1"> Enable Testmode</label><br>
                            <p class="description">Use Sandbox (testmode) API for development purposes. Don't forget to uncheck before going live.</p>
                        </fieldset>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row" class="titledesc">
                        <label for="du_sslcommerz_store_id">Store ID </label>
                    </th>
                    <td class="forminp">
                        <fieldset>
                            <legend class="screen-reader-text"><span>Store ID</span></legend>
                            <input class="input-text regular-input " type="text" name="du_sslcommerz_store_id" id="du_sslcommerz_store_id" style="" value="" placeholder="">
                            <p class="description">API store id <span style="color: red">(NOT the merchant panel id)</span>. You should obtain this info from SSLCommerz.</p>
                        </fieldset>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row" class="titledesc">
                        <label for="du_sslcommerz_store_password">Store Password </label>
                    </th>
                    <td class="forminp">
                        <fieldset>
                            <legend class="screen-reader-text"><span>Store Password</span></legend>
                            <input class="input-text regular-input " type="text" name="du_sslcommerz_store_password" id="du_sslcommerz_store_password" style="" value="" placeholder="">
                            <p class="description">API store password <span style="color: red">(NOT the merchant panel password)</span>. You should obtain this info from SSLCommerz.</p>
                        </fieldset>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row" class="titledesc">
                        <label for="du_sslcommerz_redirect_page_id">Select Success Page </label>
                    </th>
                    <td class="forminp">
                        <fieldset>
                            <legend class="screen-reader-text"><span>Select Success Page</span></legend>
                            <select class="select " name="du_sslcommerz_redirect_page_id" id="du_sslcommerz_redirect_page_id" style="">
                                <option value="">Select Success Page</option>
                                <?php $this->get_pages();?>
                            </select>
                            <p class="description">User will be redirected here after a successful payment.</p>
                        </fieldset>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row" class="titledesc">
                        <label for="du_sslcommerz_fail_page_id">Fail / Cancel Page </label>
                    </th>
                    <td class="forminp">
                        <fieldset>
                            <legend class="screen-reader-text"><span>Fail / Cancel Page</span></legend>
                            <select class="select " name="du_sslcommerz_fail_page_id" id="du_sslcommerz_fail_page_id" style="">
                                <option value="">Select Fail / Cancel Page</option>
                                <?php $this->get_pages();?>
                            </select>
                            <p class="description">User will be redirected here if transaction fails or get canceled.</p>
                        </fieldset>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row" class="titledesc">
                        <label for="du_sslcommerz_ipnurl">IPN (Instant Payment Notification) URL </label>
                    </th>
                    <td class="forminp">
                        <fieldset>
                            <legend class="screen-reader-text"><span>IPN (Instant Payment Notification) URL</span></legend>
                            <input class="input-text regular-input " type="text" name="du_sslcommerz_ipnurl" id="du_sslcommerz_ipnurl" style="" value="<?php echo get_site_url()?>/index.php?sslcommerzipn" placeholder="">
                            <p class="description">Copy this URL and set as "IPN URL" in your Merchant Panel.</p>
                        </fieldset>
                    </td>
                </tr>
                </tbody>
            </table>

            <p class="submit">
                <input type="submit" name="submit_payment_form" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
            </p>

        </form>
        </div>

        <?php
    }
    // get all pages
    function get_pages()
    {
        if ( $pages = get_pages( $args ) ){
        foreach ( $pages as $page ) {
            echo "<option value='{$page->ID}'>{$page->post_title}</option>";
        }
    }
    }
}

