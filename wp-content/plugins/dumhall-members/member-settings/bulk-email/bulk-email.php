<?php
/**
 *
 *Bulk email admin page template
 */

class BULKMAIL{

    public function __construct(){

        // add_action('wp_ajax_member_search_action',[$this,'member_search_sction']);
        //add_action('wp_ajax_nopriv_member_search_action',[$this,'member_search_sction']);
        // add_action('admin_footer',[$this,'member_search_script']);
        $this->dumhallBulkMail();
    }

    /**
     *
     */
    public function dumhallBulkMail() {
        // get site info to construct 'FROM' for email
        $from_name = wp_specialchars_decode( get_option('blogname'), ENT_QUOTES );
        $from_email = get_bloginfo('admin_email');

        // initialize
        $send_mail_message = false;

        if ( !empty( $_POST ) && check_admin_referer( 'dumhall_send_email', 'dumhall-form-nonce' ) ) {
            // handle attachment
            $attachment_path = '';
            if ( $_FILES ) {
                if ( !function_exists( 'wp_handle_upload' ) ) {
                    require_once( ABSPATH . 'wp-admin/includes/file.php' );
                }
                $uploaded_file = $_FILES['attachment'];
                $upload_overrides = array( 'test_form' => false );
                $attachment = wp_handle_upload( $uploaded_file, $upload_overrides );
                if ( $attachment && !isset( $attachment['error'] ) ) {
                    // file was successfully uploaded
                    $attachment_path = $attachment['file'];
                } else {
                    // echo $attachment['error'];
                }
            }

            // get the posted form values
            $dumhall_recipient_emails = isset( $_POST['dumhall_recipient_emails'] ) ? trim($_POST['dumhall_recipient_emails']) : '';
            $dumhall_subject = isset( $_POST['dumhall_subject'] ) ? stripslashes(trim($_POST['dumhall_subject'])) : '';
            $dumhall_body = isset( $_POST['dumhall_body'] ) ? stripslashes(nl2br($_POST['dumhall_body']))  : '';
            $dumhall_group_email = isset( $_POST['dumhall_group_email'] ) ? trim($_POST['dumhall_group_email']) : 'no';
            $recipients = explode( ',',$dumhall_recipient_emails );

            // initialize some vars
            $errors = array();
            $valid_email = true;

            // simple form validation
            if ( empty( $dumhall_recipient_emails ) ) {
                $errors[] = __( "Please enter an email recipient in the To: field.", 'dumhall' );
            } else {
                // Loop through each email and validate it
                foreach( $recipients as $recipient ) {
                    if ( !filter_var( trim($recipient), FILTER_VALIDATE_EMAIL ) ) {
                        $valid_email = false;
                        break;
                    }
                }
                // create appropriate error msg
                if ( !$valid_email ) {
                    $errors[] = _n( "The To: email address appears to be invalid.", "One of the To: email addresses appears to be invalid.", count($recipients), 'dumhall-toolkit' );
                }
            }
            if ( empty($dumhall_subject) ) $errors[] = __( "Please enter a Subject.", 'dumhall-toolkit' );
            if ( empty($dumhall_body) ) $errors[] = __( "Please enter a Message.", 'dumhall-toolkit' );

            // send the email if no errors were found
            if ( empty($errors) ) {
                $headers[] = "Content-Type: text/html; charset=\"" . get_option('blog_charset') . "\"\n";
                $headers[] = 'From: ' . $from_name . ' <' . $from_email . ">\r\n";
                $attachments = $attachment_path;

                if ( $dumhall_group_email === 'yes' ) {
                    if ( wp_mail( $dumhall_recipient_emails, $dumhall_subject, $dumhall_body, $headers, $attachments ) ) {
                        $send_mail_message = '<div class="updated">' . __( 'Your email has been successfully sent!', 'dumhall-toolkit' ) . '</div>';
                    } else {
                        $send_mail_message = '<div class="error">' . __( 'There was an error sending the email.', 'dumhall-toolkit' ) . '</div>';
                    }
                } else {
                    foreach( $recipients as $recipient ) {
                        if ( wp_mail( $recipient, $dumhall_subject, $dumhall_body, $headers, $attachments ) ) {
                            $send_mail_message .= '<div class="updated">' . __( 'Your email has been successfully sent to ', 'dumhall-toolkit' ) . esc_html($recipient) . '!</div>';
                        } else {
                            $send_mail_message .= '<div class="error">' . __( 'There was an error sending the email to ', 'dumhall-toolkit' ) . esc_html($recipient) . '</div>';
                        }
                    }
                }

                // delete the uploaded file (attachment) from the server
                if ( $attachment_path ) {
                    unlink($attachment_path);
                }
            }
        }
        ?>
        <div class="wrap" id="dumhall-bulk-wrapper">
            <h1><?php _e( 'Send Email From Admin', 'dumhall-toolkit' ); ?></h1>
            <?php
            if ( !empty($errors) ) {
                echo '<div class="error"><ul>';
                foreach ($errors as $error) {
                    echo "<li>$error</li>";
                }
                echo "</ul></div>\n";
            }
            if ( $send_mail_message ) {
                echo $send_mail_message;
            }
            ?>
            <div id="poststuff">
                <div id="post-body" class="metabox-holder columns-2">
                    <div id="post-body-content">
                        <form method="POST" id="dumhall-form" enctype="multipart/form-data">
                            <?php wp_nonce_field( 'dumhall_send_email', 'dumhall-form-nonce' ); ?>
                            <table cellpadding="0" border="0" class="form-table">
                                <tr>
                                    <th scope=”row”>From:</th>
                                    <td><input type="text" disabled value="<?php echo "$from_name &lt;$from_email&gt;"; ?>" required><div class="note"><?php _e( 'These can be changed in Settings->General.', 'dumhall-toolkit' ); ?></div></td>
                                </tr>
                                <tr>
                                    <th scope=”row”><label for="dumhall-recipient-emails">To:</label></th>

                                    <td><input type="email" multiple id="dumhall-recipient-emails" name="dumhall_recipient_emails" value="<?php echo esc_attr($this->dumhallPluginInstector($dumhall_recipient_emails) ); ?>" required>
                                        <div class="note">
                                            <?php _e( 'To send to multiple recipients, enter each email address separated by a comma or choose from the user list below.', 'dumhall-toolkit' ); ?>
                                        </div>
                                        <div class="select_user_wrapper">
                                            <select  multiple="multiple" id="dumhall-user-list" name="dumhall-user-list[]">
                                                <?php
                                                $args = array(
                                                    'role' => 'subscriber',
                                                    'orderby' => 'user_email',
                                                    'order' => 'ASC'
                                                );
                                                $users = get_users( $args );

                                                foreach ( $users as $user ) {
                                                    if ( $user->first_name && $user->last_name ) {
                                                        $user_fullname = ' (' . $user->first_name . ' ' . $user->last_name . ')';
                                                    } else {
                                                        $user_fullname = '';
                                                    }
                                                    echo '<option value="' . esc_html( $user->user_email ) . '">' . esc_html( $user->user_email ) . esc_html( $user_fullname) . '</option>';
                                                };
                                                ?>
                                            </select>
                                            <div class="select-all">
                                                <input  type="button" id="select_all" name="select_all" value="Select All user">

                                            </div>
                                            <div class="select-department">
                                                <select  id="select-department_list">
                                                    <option value="">-- <?php _e( 'Select Department', 'dumhall' ); ?> --</option>
                                                    <?php

                                                    $users = get_users( array( 'fields' => array( 'ID' ) ) );
                                                    foreach($users as $user_id){
                                                        $data = get_user_meta ( $user_id->ID);
                                                        if(!empty($data['department'][0])){
                                                            echo '<option value="' . esc_html( $data['department'][0]) . '">' . esc_html( $data['department'][0] ) . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="select-session">
                                                <select   id="select-session_list">
                                                    <option value="">-- <?php _e( 'Select Session', 'dumhall' ); ?> --</option>
                                                    <?php

                                                    $users = get_users( array( 'fields' => array( 'ID' ) ) );
                                                    foreach($users as $user_id){
                                                        $data = get_user_meta ( $user_id->ID);
                                                        if(isset($data['session'][0])){
                                                            echo '<option value="' . esc_html( $data['session'][0]) . '">' . esc_html( $data['session'][0] ) . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope=”row”></th>
                                    <td>
                                        <div class="dumhall-radio-wrap">
                                            <input type="radio" class="radio" name="dumhall_group_email" value="no" id="no"<?php if ( isset($dumhall_group_email) && $dumhall_group_email === 'no' ) echo ' checked'; ?> required>
                                            <label for="no"><?php _e( 'Send each recipient an individual email', 'dumhall-toolkit' ); ?></label>
                                        </div>
                                        &nbsp;&nbsp;
                                        <div class="dumhall-radio-wrap">
                                            <input type="radio" class="radio" name="dumhall_group_email" value="yes" id="yes"<?php if ( isset($dumhall_group_email) && $dumhall_group_email === 'yes' ) echo ' checked'; ?> required>
                                            <label for="yes"><?php _e( 'Send a group email to all recipients', 'dumhall-toolkit' ); ?></label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope=”row”><label for="dumhall-subject">Subject:</label></th>
                                    <td><input type="text" id="dumhall-subject" name="dumhall_subject" value="<?php echo esc_attr(  $this->dumhallPluginInstector($dumhall_subject) );?>" required></td>
                                </tr>
                                <tr>
                                    <th scope=”row”><label for="dumhall_body">Message:</label></th>
                                    <td align="left">
                                        <?php
                                        $settings = array( "editor_height" => "200" );
                                        wp_editor( $this->dumhallPluginInstector($dumhall_body), "dumhall_body", $settings );
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope=”row”><label for="attachment">Attachment:</label></th>
                                    <td><input type="file" id="attachment" name="attachment"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="right">
                                        <input  type="submit" value="<?php _e( 'Send Email', 'dumhall-toolkit' ); ?>" name="submit" class="button button-primary">
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>

                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <?php
    }
    /**
     * Helper function for form values
     *
     * @since 0.9
     *
     * @param string $var Var name to test isset
     *
     * @return string $var value if isset or ''
     */
    public function dumhallPluginInstector(&$var) {
        return isset($var) ? $var : '';
    }


    public function member_search_script(){
        ?>
        <script type="text/javascript" >
            jQuery(document).ready(function($) {
                var data = {
                    action: "member_search_sction",
                    whatever: 1234
                };

                // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
                jQuery.post(ajaxurl, data, function(response) {
                    alert('Got this from the server: ' + response);
                });
            });
        </script>
        <?php
    }
}



$bulkMailObj = new BULKMAIL();