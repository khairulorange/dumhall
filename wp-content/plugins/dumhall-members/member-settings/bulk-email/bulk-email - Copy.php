<?php
/**
*
 *Bulk email admin page template
 */
namespace DUMHALL\BULK;
class BULKMAIL{

    public function __construct(){
          $this->bulkMailProcess();
          $this->bulkMailRender();
          $this->dumhallPluginIssetor();
    }
    public function bulkMailProcess(){
// get site info to construct 'FROM' for email
        $from_name = wp_specialchars_decode( get_option('blogname'), ENT_QUOTES );
        $from_email = get_bloginfo('admin_email');

        // initialize
        $send_mail_message = false;

        if ( !empty( $_POST ) && check_admin_referer( 'dumhall_send_email', 'dumhall-form-nonce' ) ) {
            // handle attachment
            $attachment_path = '';
            if ( $_FILES ) {
                if ( !function_exists( 'wp_handle_upload' ) ) {
                    require_once( ABSPATH . 'wp-admin/includes/file.php' );
                }
                $uploaded_file = $_FILES['attachment'];
                $upload_overrides = array( 'test_form' => false );
                $attachment = wp_handle_upload( $uploaded_file, $upload_overrides );
                if ( $attachment && !isset( $attachment['error'] ) ) {
                    // file was successfully uploaded
                    $attachment_path = $attachment['file'];
                } else {
                    // echo $attachment['error'];
                }
            }

            // get the posted form values
            $dumhall_recipient_emails = isset( $_POST['dumhall_recipient_emails'] ) ? trim($_POST['dumhall_recipient_emails']) : '';
            $dumhall_subject = isset( $_POST['dumhall_subject'] ) ? stripslashes(trim($_POST['dumhall_subject'])) : '';
            $dumhall_body = isset( $_POST['dumhall_body'] ) ? stripslashes(nl2br($_POST['dumhall_body']))  : '';
            $dumhall_group_email = isset( $_POST['dumhall_group_email'] ) ? trim($_POST['dumhall_group_email']) : 'no';
            $recipients = explode( ',',$dumhall_recipient_emails );

            // initialize some vars
            $errors = array();
            $valid_email = true;

            // simple form validation
            if ( empty( $dumhall_recipient_emails ) ) {
                $errors[] = __( "Please enter an email recipient in the To: field.", 'dumhall' );
            } else {
                // Loop through each email and validate it
                foreach( $recipients as $recipient ) {
                    if ( !filter_var( trim($recipient), FILTER_VALIDATE_EMAIL ) ) {
                        $valid_email = false;
                        break;
                    }
                }
                // create appropriate error msg
                if ( !$valid_email ) {
                    $errors[] = _n( "The To: email address appears to be invalid.", "One of the To: email addresses appears to be invalid.", count($recipients), 'dumhall-toolkit' );
                }
            }
            if ( empty($dumhall_subject) ) $errors[] = __( "Please enter a Subject.", 'dumhall-toolkit' );
            if ( empty($dumhall_body) ) $errors[] = __( "Please enter a Message.", 'dumhall-toolkit' );

            // send the email if no errors were found
            if ( empty($errors) ) {
                $headers[] = "Content-Type: text/html; charset=\"" . get_option('blog_charset') . "\"\n";
                $headers[] = 'From: ' . $from_name . ' <' . $from_email . ">\r\n";
                $attachments = $attachment_path;

                if ( $dumhall_group_email === 'yes' ) {
                    if ( wp_mail( $dumhall_recipient_emails, $dumhall_subject, $dumhall_body, $headers, $attachments ) ) {
                        $send_mail_message = '<div class="updated">' . __( 'Your email has been successfully sent!', 'dumhall-toolkit' ) . '</div>';
                    } else {
                        $send_mail_message = '<div class="error">' . __( 'There was an error sending the email.', 'dumhall-toolkit' ) . '</div>';
                    }
                } else {
                    foreach( $recipients as $recipient ) {
                        if ( wp_mail( $recipient, $dumhall_subject, $dumhall_body, $headers, $attachments ) ) {
                            $send_mail_message .= '<div class="updated">' . __( 'Your email has been successfully sent to ', 'dumhall-toolkit' ) . esc_html($recipient) . '!</div>';
                        } else {
                            $send_mail_message .= '<div class="error">' . __( 'There was an error sending the email to ', 'dumhall-toolkit' ) . esc_html($recipient) . '</div>';
                        }
                    }
                }

                // delete the uploaded file (attachment) from the server
                if ( $attachment_path ) {
                    unlink($attachment_path);
                }
            }
        }
    }
    public function bulkMailRender(){
     ob_start()?>

        <div class="wrap" id="dum-bulk-wrapper">
            <h1><?php _e( 'Send Email From Admin', 'dumhall-toolkit' ); ?></h1>
            <?php
            if ( !empty($errors) ) {
                echo '<div class="error"><ul>';
                foreach ($errors as $error) {
                    echo "<li>$error</li>";
                }
                echo "</ul></div>\n";
            }
            if ( $this->$send_mail_message ) {
                echo $this->$send_mail_message;
            }
            ?>
            <div id="bulkMailWrapper">
                <div  class="metabox-holder columns-2">
                    <div class="post-body-content">
                        <form method="POST" id="dumhall-form" enctype="multipart/form-data">
                            <?php wp_nonce_field( 'dumhall_send_email', 'dumhall-form-nonce' ); ?>
                            <table cellpadding="0" border="0" class="form-table">
                                <tr>
                                    <th scope="row">From:</th>
                                    <td><input type="text" disabled value="<?php echo "$this->$from_name &lt;$this->$from_email&gt;"; ?>" required><div class="note"><?php _e( 'These can be changed in Settings->General.', 'dumhall-toolkit' ); ?></div></td>
                                </tr>
                                <tr>
                                    <th scope="row"><label for="dumhall-recipient-emails">To:</label></th>
                                    <td><input type="email" multiple id="dumhall-recipient-emails" name="dumhall_recipient_emails" value="<?php echo esc_attr( $this['dumhallPluginIssetor']($this->$dumhall_recipient_emails) ); ?>" required><div class="note"><?php _e( 'To send to multiple recipients, enter each email address separated by a comma or choose from the user list below.', 'dumhall-toolkit' ); ?></div>
                                        <select id="dumhall-user-list">
                                            <option value="">-- <?php _e( 'user list', 'dumhall-toolkit' ); ?> --</option>
                                            <?php
                                            $users = get_users( 'orderby=user_email' );
                                            foreach ( $users as $user ) {
                                                if ( $user->first_name && $user->last_name ) {
                                                    $user_fullname = ' (' . $user->first_name . ' ' . $user->last_name . ')';
                                                } else {
                                                    $user_fullname = '';
                                                }
                                                echo '<option value="' . esc_html( $user->user_email ) . '">' . esc_html( $user->user_email ) . esc_html( $user_fullname) . '</option>';
                                            };
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row"></th>
                                    <td>
                                        <div class="dumhall-radio-wrap">
                                            <input type="radio" class="radio" name="dumhall_group_email" value="no" id="no"<?php if ( isset($dumhall_group_email) && $dumhall_group_email === 'no' ) echo ' checked'; ?> required>
                                            <label for="no"><?php _e( 'Send each recipient an individual email', 'dumhall-toolkit' ); ?></label>
                                        </div>
                                        &nbsp;&nbsp;
                                        <div class="dumhall-radio-wrap">
                                            <input type="radio" class="radio" name="dumhall_group_email" value="yes" id="yes"<?php if ( isset($dumhall_group_email) && $dumhall_group_email === 'yes' ) echo ' checked'; ?> required>
                                            <label for="yes"><?php _e( 'Send a group email to all recipients', 'dumhall-toolkit' ); ?></label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row"><label for="dumhall-subject">Subject:</label></th>
                                    <td><input type="text" id="dumhall-subject" name="dumhall_subject" value="<?php echo esc_attr( $this['dumhallPluginIssetor']($dumhall_subject) );?>" required></td>
                                </tr>
                                <tr>
                                    <th scope="row"><label for="dumhall_body">Message:</label></th>
                                    <td align="left">
                                        <?php
                                        $settings = array( "editor_height" => "200" );
                                        wp_editor( $this['dumhallPluginIssetor']($dumhall_body), "dumhall_body", $settings );
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope=”row”><label for="attachment">Attachment:</label></th>
                                    <td><input type="file" id="attachment" name="attachment"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="right">
                                        <input type="submit" value="<?php _e( 'Send Email', 'dumhall-toolkit' ); ?>" name="submit" class="button button-primary">
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <div id="postbox-container-1" class="postbox-container">
                        <div class="postbox">
                            <h3><span>Like this plugin?</span></h3>
                            <div class="inside">
                                <ul>
                                    <li><a href="https://wordpress.org/support/view/plugin-reviews/send-email-from-admin?filter=5" target="_blank">Rate it on WordPress.org</a></li>
                                    <li><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=8HHLL6WRX9Z68" target="_blank">Donate to the developer</a></li>
                                </ul>
                            </div> <!-- .inside -->
                        </div>
                    </div>

                </div>                    <div class="clear"></div>
                </div>
        </div>
    <?php
      return ob_get_flush();
    }
    public function dumhallPluginIssetor(&$var) {
    return isset($var) ? $var : '';
    }
}
$bulkmail = new BULKMAIL();