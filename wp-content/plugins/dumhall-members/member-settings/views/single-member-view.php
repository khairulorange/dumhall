
<div class="single-member-details">
    <?php

    /**
     * The plugin area to view the usermeta
     */

//    if( current_user_can('edit_users' ) ) { ?>
<!--        <h2> --><?php //echo __('Displaying Usermeta for ' . $user->display_name . ' (' . $user->user_login . ')', 'dumhall-toolkit'); ?><!-- </h2>-->
<!--        --><?php
//
//        $usermeta = get_user_meta( $user_id );
//        var_dump( $user->user_email);
//        echo '<div class="card">';
//        foreach( $usermeta as $key => $value ) {
//            $v = (is_array($value)) ? implode(', ', $value) : $value;
//            echo '<p">'. $key . ': ' . $v . '</p>';
//        }
//        echo '</div><br>';
//        ?>
<!--        <a href="--><?php //echo esc_url( add_query_arg( array( 'page' => wp_unslash( $_REQUEST['page'] ) ) , admin_url( 'users.php' ) ) ); ?><!--">--><?php //_e( 'Back', 'dumhall-toolkit' ) ?><!--</a>-->
<!--        --><?php
//    }
//    else {
//        ?>
<!--        <p> --><?php //echo __( 'You are not authorized to perform this operation.', 'dumhall-toolkit' ) ?><!-- </p>-->
<!--        --><?php
//    }?>

    <?php if( current_user_can('edit_users' ) ) {
         ob_start();
        ?>
    <div class="dumhall-user-area">

        <div class="dumhall-user-info">
            <div class="dumhall-image">
                <?php echo get_avatar($data->user->ID, 250); ?>
            </div>
            <div class="dumhall-fields">

                <div class="dumhall-field-row">

                    <div class="dumhall-field-label"><?php _e('First Name','dumhall-toolkit')?></div>
                    <div class="dumhall-field-value"><?php echo $user->first_name; ?></div>
                </div>

                <div class="dumhall-field-row">

                    <div class="dumhall-field-label"><?php _e('Last Name','dumhall-toolkit')?></div>
                    <div class="dumhall-field-value"><?php echo $user->last_name; ?></div>
                </div>
                <div class="dumhall-field-row">

                    <div class="dumhall-field-label"><?php _e('Name','dumhall-toolkit')?></div>
                    <div class="dumhall-field-value"><?php echo $user->name; ?></div>
                </div>
                <div class="dumhall-field-row">

                    <div class="dumhall-field-label"><?php _e('Email','dumhall-toolkit')?></div>
                    <div class="dumhall-field-value"><?php echo $user->user_email; ?></div>
                </div>
                <div class="dumhall-field-row">

                    <div class="dumhall-field-label"><?php _e('Mobile','dumhall-toolkit')?></div>
                    <div class="dumhall-field-value"><?php echo $user->mobile; ?></div>
                </div>
                <div class="dumhall-field-row">

                    <div class="dumhall-field-label"><?php _e('Phone Office Home','dumhall-toolkit')?></div>
                    <div class="dumhall-field-value"><?php echo $user->phoneOfficeHome; ?></div>
                </div>
                <div class="dumhall-field-row">

                    <div class="dumhall-field-label"><?php _e('Permanent Address','dumhall-toolkit')?></div>
                    <div class="dumhall-field-value"><?php echo $user->permanentAddress; ?></div>
                </div>
                <div class="dumhall-field-row">

                    <div class="dumhall-field-label"><?php _e('Present Address','dumhall-toolkit')?></div>
                    <div class="dumhall-field-value"><?php echo $user->presentAddress; ?></div>
                </div>

                <div class="dumhall-field-row">

                    <div class="dumhall-field-label"><?php _e('Department','dumhall-toolkit')?></div>
                    <div class="dumhall-field-value"><?php echo $user->department; ?> </div>
                </div>
                <div class="dumhall-field-row">

                    <div class="dumhall-field-label"><?php _e('Qualification','dumhall-toolkit')?></div>
                    <div class="dumhall-field-value"><?php echo $user->qualification; ?> </div>
                </div>
                <div class="dumhall-field-row">

                    <div class="dumhall-field-label"><?php _e('Designation','dumhall-toolkit')?></div>
                    <div class="dumhall-field-value"><?php echo $user->occupationDesignation; ?> </div>
                </div>
                <div class="dumhall-field-row">

                    <div class="dumhall-field-label"><?php _e('Blood Group','dumhall-toolkit')?></div>
                    <div class="dumhall-field-value"><?php echo $user->bloodGroup; ?> </div>
                </div>
                <div class="back-btn-wrap">
                    <a class="btn btn-admin-back" href="<?php echo esc_url( add_query_arg( array( 'page' => wp_unslash( $_REQUEST['page'] ) ) , admin_url( 'admin.php' ) ) ); ?>"><?php _e( 'Back', 'dumhall-toolkit' ) ?></a>

                </div>

            </div>

        </div>
        <div class="dumhall_tabbing_container ui-tabs ui-widget ui-widget-content ui-corner-all">



            <div class="dumhall-user-content">


                <table class="user-content ui-tabs-panel ui-widget-content ui-corner-bottom" id="rmsecondtabcontent" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">

                    <tbody><tr> <td class="rmnotice"> </td></tr>
                    </tbody></table>

                <table class="user-content ui-tabs-panel ui-widget-content ui-corner-bottom" id="rmthirdtabcontent" aria-labelledby="ui-id-3" role="tabpanel" aria-hidden="true" style="display: none;">
                    <tbody><tr> <td class="rmnotice">No payment records exist for this user.</td></tr>                </tbody></table>

                <table class="user-content ui-tabs-panel ui-widget-content ui-corner-bottom" id="rmfourthtabcontent" aria-labelledby="ui-id-4" role="tabpanel" aria-hidden="true" style="display: none;">

                    <tbody><tr> <td class="rmnotice">No email sent yet</td></tr>
                    </tbody></table>

                <!-- Login-timeline Start -->
                <div class="rm-login-timeline rm-login-user-view">

                    <table class="user-content rm-login-analytics ui-tabs-panel ui-widget-content ui-corner-bottom" id="rmfifthtabcontent" aria-labelledby="ui-id-5" role="tabpanel" aria-hidden="true" style="display: none;">
                        <tbody>
                        <tr><td class="rmnotice"><span class="rm-line-break">No login records available for this user.<br></span>This area displays user specific login records with details. <a target="_blank" href="https://registrationmagic.com/wordpress-user-login-plugin-guide/#user-manager">More Info</a></td></tr>                        </tbody>
                    </table>

                </div>

                <!-- Login-timeline End -->

            </div>
        </div>

    </div>
        <?php
    }
    else {
        ?>
        <p> <?php echo __( 'You are not authorized to perform this operation.', 'dumhall-toolkit' ) ?> </p>
        <?php
    }
    return ob_get_flush();
    ?>
</div>