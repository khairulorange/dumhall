<?php

/**
 * The plugin area to process the table bulk actions.
 */
if( current_user_can('edit_users' ) ) { ?>
<div class="member-bulk-mail">
    <h2> <?php echo __('Member Bulk Mail Settings: <br>','dumhall-toolkit' ); ?> </h2>
    <div class="member-bulk-email-inner">
        <form method="post"  name="bulk-user-mail">
            <?php wp_nonce_field( 'bulk_send_email', 'bulk-email-form-nonce' ); ?>
            <div class="form-group">
                <label for="selected-user-emails">Emails</label>
                <textarea type="text" name="users_email" class="form-control users_email">
                    <?php
                    $count = count($bulk_user_ids);
                    foreach( $bulk_user_ids as $key => $user_id ) {
                        $user = get_user_by( 'id', $user_id );
                        // $userData = explode(',',$user);
                        echo $user->user_email;
                        if($count - 1 > $key){
                            echo ', ';
                        }
                    }
                    ?>
                </textarea>
            </div>
            <div class="form-group">
                <label for="selected-user-subject">Subject</label>
                <input type="text" name="users_subject" class="form-control" value="<?php echo __('Mohsin Hall Meeting query','dumhall-toolkit' );?>">
            </div>
            <div class="form-group">
                <label for="selected-user-subject">From Mail</label>
                <input type="text" name="users_from_mail" class="form-control" value="<?php echo get_option('admin_email');?>">
            </div>
            <div class="form-group">
                <label for="selected-user-emails">Message Body</label>
                <textarea rows="10"  type="text" name="users_message" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <input type="submit" value="<?php _e( 'Send Email', 'dumhall-toolkit' ); ?>" name="submit" class=" user-bulk-submit button button-primary">
            </div>
        </form>
    </div>
</div>
    <?php
    if($_POST['bulk_send_email']){

        $userMails = $_POST['users_email'];
        $userSubject = $_POST['users_subject'];
        $userMessage = $_POST['users_message'];

        add_filter( 'wp_mail_content_type', function( $content_type ) {
            return 'text/html';
        });
        $subject = $userSubject;
        $headers = array();
        $message = "Hi<br>";
        $message .= "Alumni Association Member account has been created on ".get_bloginfo( 'name' );
        $message .= "<br>";
        $message .= "Please Wait for Alumni Association Admin Approval";
        $message .= "<br>";
        $message .= "Thank you";
        $headers[] = 'From: '.get_bloginfo( 'name').'<'.get_option('admin_email').'>'. "\r\n";
        foreach($userMails as $totalMail){
            wp_mail( $totalMail,$subject,$message, $headers);
        }

        remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
        echo "Your Mail send Successfully";
//        wp_safe_redirect('?page=du_members');
    }
}
else {
    ?>
    <p> <?php echo __( 'You are not authorized to perform this operation.', 'dumhall-toolkit') ?> </p>
    <?php
}
?><?php


