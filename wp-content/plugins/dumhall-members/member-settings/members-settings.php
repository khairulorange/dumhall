<?php
namespace DUMHALL;
use PAYMENTS\PaymentOptions;

require_once 'member-list-table.php';


class DUMembers
{   /**
	 * Member class instance
	 * @var object
	 */
	private $memberlistTable;

	/**
	 * method __construct()
	 */
	public function __construct()
	{
		/**
		 * call initialize plugin
		 */
		$this->init();
	}

	/**
	 * initialize plugin
	 * @return void
	 */
	public function init()
	{
		require_once 'member-list-table.php';
		add_action('admin_menu', [$this, 'addOptionsPage'], 5);
        add_action('admin_menu', [$this, 'du_member_screen_option']);
        add_action('admin_enqueue_scripts', [$this, 'enqueueAdminAssets']);
        //$this->paymentSettingsApi();
        add_action('admin_init',[$this,'paymentSettingsApi']);
        add_action('wp_ajax_member_search_action',[$this,'member_search_sction']);
	
	}
    /**
     * enqueue assets in admin
     * @return void
     */
    public function enqueueAdminAssets()
    {
        wp_enqueue_style('dumember-admin-style', DUMHALL_URL . '/assets/css/member-details.css' );
        wp_enqueue_script('my-custom-script', DUMHALL_URL . '/assets/js/member-settings.js');
        wp_enqueue_script('memberSearchFilter', DUMHALL_URL . '/assets/js/user-search-filter.js',array('jquery'),time(),true);
        wp_localize_script(
                'memberSearchFilter',
                'ajax_object',array(
                'ajax_url' => admin_url( 'admin-ajax.php' ),
                'nonce'    => wp_create_nonce('search-filter-nonce'))
            );

    }
	/**
	 * add plugin option page in admin menu
	 * @return void
	 */
	public function addOptionsPage()
	{
		add_menu_page(
			'Alumni Members',
			'Alumni Members',
			'manage_options',
			'du_members',
			[$this, 'optionsPageContent'],
			'dashicons-admin-users',
			20
		);
        add_submenu_page( 'du_members', 'Member Settings', 'Member Settings', 'manage_options', 'member-settings', [$this,'alumniMemberSettings']);
        add_submenu_page( 'du_members', 'Bulk Mail', 'Bulk Mail', 'manage_options', 'du-bulk-mail', [$this,'bulkEmailSettings']);
        add_submenu_page( 'du_members', 'Payment Settings', 'Payment Settings', 'manage_options', 'payment-settings', [$this,'paymentSettings']);
	}
	public function alumniMemberSettings(){
      echo "<h1>Member setting page</h1>";
    }
    public function bulkEmailSettings(){
        ob_start();
        ?>
        <div class="user-bulk-information">
            <?php
            require_once(DUMHALL_PATH . '/member-settings/bulk-email/bulk-email.php');

            ?>
        </div>

        <?php  return ob_get_flush();
        }
	/**
	 * callback for option page content
	 * @return void
	 */
	public function optionsPageContent()
	{
		ob_start();
	?>
	<div class="du-member-settings">

            <h1><?php _e('Mohsin Hall Member Lists', 'dumhall') ?></h1>

            <div class="wrap">
                <div class="page-outer">
                    <div class="du-member-content">
                        <?php
                        $this->memberlistTable->prepare_items();
                        ?>
                        <form method="post" class="search-member-list" action="<?php echo $_SERVER['PHP_SELF'] .'?page=du_members' ?>">
                        <?php $this->memberlistTable->search_box('Search Member','search_member_id');
                                 $this->memberlistTable->display();
                           ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
	<?php
	return ob_get_flush();
	}
   public function paymentSettingsApi(){
       register_setting( 'payment-settings-group', 'payment_api_opt' );
       add_settings_section(
           'payment_api_section',
           __( 'Payment Settings', 'dumhall' ),
           [$this,'PaymentApiSettingSsectionCallback'],
           'payment-settings'
       );

       add_settings_field(
           'test_mode',
           __( 'Test Mode', 'dumhall' ),
           [$this,'testModeFieldRender'],
           'payment-settings',
           'payment_api_section'
       );
       add_settings_field(
           'store_id',
           __( 'Store Id', 'dumhall' ),
           [$this,'storeIdFieldRender'],
           'payment-settings',
           'payment_api_section'
       );
       add_settings_field(
           'store_password',
           __( 'Store Password', 'dumhall' ),
           [$this,'storePasswordFieldRender'],
           'payment-settings',
           'payment_api_section'
       );
       add_settings_field(
           'success_page',
           __( 'Select Success Page', 'dumhall' ),
           [$this,'successPageFieldRender'],
           'payment-settings',
           'payment_api_section'
       );
       add_settings_field(
           'fail_page',
           __( 'Select Fail / Cancel Page', 'dumhall' ),
           [$this,'failPageFieldRender'],
           'payment-settings',
           'payment_api_section'
       );
       add_settings_field(
           'ipn_page',
           __( 'Select IPN Page', 'dumhall' ),
           [$this,'IpnPageFieldRender'],
           'payment-settings',
           'payment_api_section'
       );

       add_settings_field(
           'registration_fee',
           __( 'Registration Fee', 'dumhall' ),
           [$this,'registration_fee_cb'],
           'payment-settings',
           'payment_api_section'
       );

   }
    public function PaymentApiSettingSsectionCallback(){
        //echo "<h2>" . __( 'Payment Settings', 'dumhall' ) . "</h2>";
    }
    public function testModeFieldRender(){
        $options = get_option( 'payment_api_opt' );
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Testmode</span></legend>
            <?php
            $checked = ($options['test_mode'] == 'true' ? ' checked="checked"' : '');
            ?>
            <label for="payment_api_opt[test_mode]">
                <input class="" type="checkbox" name='payment_api_opt[test_mode]' id="payment_api_opt[test_mode]"  value="true" <?php echo $checked;?> > Enable Testmode
            </label>
            <p class="description">Use Sandbox (testmode) API for development purposes. Don't forget to uncheck before going live.</p>
        </fieldset>
        <?php
    }
    public function storeIdFieldRender(){
        $options = get_option( 'payment_api_opt' );
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Store ID</span></legend>
            <input class="input-text regular-input " type="text" name="payment_api_opt[store_id]" id="payment_api_opt[store_id]" style="" value="<?php echo $options['store_id']; ?>">
            <p class="description">API store id <span style="color: red">(NOT the merchant panel id)</span>. You should obtain this info from SSLCommerz.</p>
        </fieldset>
        <?php
    }
    public function storePasswordFieldRender(){
        $options = get_option( 'payment_api_opt' );
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Store Password</span></legend>
            <input class="input-text regular-input " type="text" name="payment_api_opt[store_password]" id="payment_api_opt[store_password]" style="" value="<?php echo $options['store_password']; ?>" placeholder="">
            <p class="description">API store password <span style="color: red">(NOT the merchant panel password)</span>. You should obtain this info from SSLCommerz.</p>
        </fieldset>
        <?php
    }
    public function registration_fee_cb(){
        $options = get_option( 'payment_api_opt' );
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Store Password</span></legend>
            <input class="input-text regular-input" placeholder="500" type="text" name="payment_api_opt[registarionFee]" id="payment_api_opt[store_password]" style="" value="<?php echo $options['registarionFee']; ?>" placeholder="">
            <p  class="description">Please enter the registration fee</p>
        </fieldset>
        <?php
    }

    public function successPageFieldRender(){
        $options = get_option( 'payment_api_opt' );
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Select Success Page</span></legend>
            <select class="select " name="payment_api_opt[success_page]" id="payment_api_opt[success_page]" style="">
                <option selected="selected" disabled="disabled" value="">Select Success Page</option>
                <?php
                $selected_page =  $options['success_page'];
                $pages = get_pages();
                foreach ( $pages as $page ) {
                    $option = '<option value="' . $page->ID . '" ';
                    $option .= ( $page->ID == $selected_page ) ? 'selected="selected"' : '';
                    $option .= '>';
                    $option .= $page->post_title;
                    $option .= '</option>';
                    echo $option;
                }
                ?>
            </select>
            <p class="description">User will be redirected here after a successful payment.</p>
        </fieldset>
        <?php
    }
    public function failPageFieldRender(){
        $options = get_option( 'payment_api_opt' );
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Select Fail / Cancel Page</span></legend>
            <select class="select " name="payment_api_opt[fail_page]" id="payment_api_opt[fail_page]" style="">
                <option selected="selected" disabled="disabled" value="">Select Fail / Cancel Page</option>
                <?php
                $selected_page =  $options['fail_page'];
                $pages = get_pages();
                foreach ( $pages as $page ) {
                    $option = '<option value="' . $page->ID . '" ';
                    $option .= ( $page->ID == $selected_page ) ? 'selected="selected"' : '';
                    $option .= '>';
                    $option .= $page->post_title;
                    $option .= '</option>';
                    echo $option;
                }
                ?>
            </select>
            <p class="description">User will be redirected here if transaction fails or get canceled.</p>
        </fieldset>
        <?php
    }
    public function IpnPageFieldRender(){
        $options = get_option( 'payment_api_opt' );
        ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Select IPN Page</span></legend>
            <select class="select " name="payment_api_opt[ipn_page]" id="payment_api_opt[ipn_page]" style="">
                <option selected="selected" disabled="disabled" value="">Select IPN Page</option>
                <?php
                $selected_page =  $options['ipn_page'];
                $pages = get_pages();
                foreach ( $pages as $page ) {
                    $option = '<option value="' . $page->ID . '" ';
                    $option .= ( $page->ID == $selected_page ) ? 'selected="selected"' : '';
                    $option .= '>';
                    $option .= $page->post_title;
                    $option .= '</option>';
                    echo $option;
                }
                ?>
            </select>
            <p class="description">Copy this URL and set as "IPN URL" in your Merchant Panel.</p>
        </fieldset>
        <?php
    }

    /**
     * Payment settings
     **/
    public function paymentSettings(){
        require_once DUMHALL_PATH .'/member-settings/payments/payment-options.php';
    }
    public function member_search_sction(){


        //echo "test" .$department;

        // this is required to terminate immediately and return a proper response
        $department = '';
        $session = '';
        if(isset($_POST['department'])){
            $department = sanitize_text_field($_POST['department']);
        }
        if(isset($_POST['session'])){
            $session = sanitize_text_field($_POST['session']);
        }
        $args = array(
            'meta_query' => array(
                'relation' => 'AND', // Could be OR, default is AND
                array(
                    'key'     => 'department',
                    'value'   => $department,
                    'compare' => '=='
                ),
                array(
                    'key'     => 'session',
                    'value'   => $session,
                    'compare' => '=='
                )
            )
        );
        $users = get_users( $args );

        foreach ( $users as $user ) {
            //var_dump($user->user_email);
               // $session = $user->session;
                if ( $user->first_name && $user->last_name ) {
                    $user_fullname = ' (' . $user->first_name . ' ' . $user->last_name . ')';
                } else {
                    $user_fullname = '';
                }
                echo '<option value="' . esc_html( $user->user_email ) . '">' . esc_html( $user->user_email ) . esc_html( $user_fullname) .'</option>';

        };
        wp_die();

    }

    public function du_member_screen_option() {

        $option = 'per_page';
        $args   = [
            'label'   => 'users',
            'default' => 5,
            'option'  => 'users_per_page'
        ];

        add_screen_option( $option, $args );

        $this->memberlistTable = new MemberTableLists();
    }
}