<div class="event-wrapper">
    	<?php
			$eventData = new WP_Query(
				[
					'post_type'        => 'events',
					'posts_per_page'   => 1,
					'orderby'          => 'post_date',
					'order'            => 'DESC',
					'post_status'      => 'publish'
				]
			);
			?>
			<?php if ($eventData->have_posts()): while ($eventData->have_posts()) : $eventData->the_post(); 
		?>
	<div class="event-inner">
		<div class="img-wrapper">
		    <?php if(has_post_thumbnail()): ?>
				<?php the_post_thumbnail('full', ['class'=>'img-fluid w-100 mb-3', 'alt'=> 'event-thumb']); ?>
			<?php endif; ?>
			
			<span>Event Going To <?php echo get_field('event_deadline');?></span>
		</div>
		<div class="content">
		    <?php if(!empty($settings->heading)) : ?>
    		<span class="upcoming-text"><?php echo $settings->heading; ?></span>
    		<?php else: ?>
    			<span class="upcoming-text">
    				Upcoming Event
    			</span>
    	    <?php endif;?>
		
			<!--<div class="time-remain">-->
			<!--	<span class="day time-schedule">-->
			<!--		Days-->
			<!--		<span class="count">00</span>-->
			<!--	</span>-->
			<!--	<span class="hour time-schedule">-->
			<!--		Hr-->
			<!--		<span class="count">00</span>-->
			<!--	</span>-->
			<!--	<span class="minutes time-schedule">-->
			<!--		Min-->
			<!--		<span class="count">00</span>-->
			<!--	</span>-->
			<!--	<span class="seconds time-schedule">-->
			<!--		Sec-->
			<!--		<span class="count">00</span>-->
			<!--	</span>-->
			<!--	<span>Remaining</span>-->
			<!--</div>-->
		    <h2><a href="<?php the_permalink(); ?>" class="text-white"><?php the_title(); ?></a></h2>
			<p><?php the_excerpt(); ?></p>
			<div class="join-us">
				<a href="<?php the_permalink(); ?>">See Details</a>
			</div>
		</div>
	</div>
	<?php endwhile;  wp_reset_query(); endif; ?>
</div>