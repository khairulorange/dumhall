<?php
//if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Plugin Name: BB Events
 * Description: Orange products list for beaver builder.
 * Version: 1.0.0
 * Author: orangetoolz
 * Author URI: https://orangetoolz.com
 */

/*
 * define module directory and url
 */
define('BBEVENT_DIR', plugin_dir_path(__FILE__));
define('BBEVENT_URL', plugins_url('/', __FILE__));





/*
 * register related posts module for beaver builder
 */
function event_lists_module()
{
	if (class_exists('FLBuilder')) {
		require_once 'event-lists-module/EventListsModule.php';
	}
}
add_action('init', 'event_lists_module');

function event_enqueue_scripts_module(){
	wp_enqueue_style('event-style', plugins_url('/event-lists-module/css/event-styles.css', __FILE__));
}
add_action('wp_enqueue_scripts','event_enqueue_scripts_module');

