<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'dumhall.local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '1yW+c)FceqGR^Np~7[X{hHzD&o4Mt2Ky322,gD+ ?Z[iwC_lRTd3Ev+^Q:*oo;L5' );
define( 'SECURE_AUTH_KEY',  'Fb.5@pzd}nBx6aRvN!x21b)h<3Q|2{Exksv9BJ2xofjC@yY{ASOJ3i >z+me>5{(' );
define( 'LOGGED_IN_KEY',    'hH%voOHJ9.-Bmcq L%iOI oYiplAV[EkKvXWok$5~{vX6[C`JC^mLE;k+l/;`_qr' );
define( 'NONCE_KEY',        'DAai4!v^9@/5S~]nFvuCzG7j7x4`A~cj&~4x7I?SA6Nk8m[0Axcr:[!Yp:v[NyG9' );
define( 'AUTH_SALT',        'GdX#A*Br}3UkT&>q9y3=`4ncsYQsG58g8iyTDUmaF[vU*#*Vmz^JIG`;YV4dCD{G' );
define( 'SECURE_AUTH_SALT', '(C$=C{j 2niE[9i5|fMgp}h5&|p`S|d~[(OZy64|Rek122]s$sWquni]2DC8zXC]' );
define( 'LOGGED_IN_SALT',   'Zy=*Ktiz>C!:ZS<lnQ8JIv)dg.%*bU=PT$&O6w{=d1d]JpTeFnPo|4F|TPY:/_c0' );
define( 'NONCE_SALT',       'G)WZ#.<0PJM2j^KT4[9Ed(}C3Rb(Aw:[H&N1D`,ZMAQTWg)Ae-mnM n*Jj,FK*VK' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
